Парсинг выражений реализован с помощью precedence climbing algorithm.
Парсинг текста реализован с помощью chain of responsibility.
Соответствует Sun Microsystems code style.
Использован log4j-2.11.
Версия Java - 8
