package com.epam.informationhandling.parser;

import com.epam.informationhandling.text.AbstractTextComponent;
import com.epam.informationhandling.text.ExpressionTextComponent;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses lexeme to binary expressions.
 */
public class ToExpressionParser extends AbstractParser {

    /**
     * Regex for binary expressions.
     */
    private static final String EXP_REGEX = "^[\\d|&^~><()]+$";

    /**
     * Default constructor.
     */
    public ToExpressionParser() {
        setPattern(Pattern.compile(EXP_REGEX));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTextComponent parse(final String textComponent) {
        Matcher matcher = getPattern().matcher(textComponent);

        if (matcher.find()) {
            return new ExpressionTextComponent(textComponent);

        } else {
            return parseNext(textComponent);
        }

    }
}
