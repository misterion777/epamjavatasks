package com.epam.informationhandling.parser;

import com.epam.informationhandling.text.AbstractTextComponent;
import com.epam.informationhandling.text.Character;
import com.epam.informationhandling.text.Word;

/**
 * Parses words in text.
 */
public class WordParser extends AbstractParser {

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTextComponent parse(final String textComponent) {

        AbstractTextComponent word = new Word();

        for (int i = 0; i < textComponent.length(); i++) {
            word.add(new Character("" + textComponent.charAt(i)));
        }

        LOGGER.info("Word {} parsed!", textComponent);
        return word;
    }
}
