package com.epam.informationhandling.expressionhandling;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains all possible token values.
 */
public enum Token {
    /**
     * Bitwise not operation.
     */
    NOT("~"),
    /**
     * Bitwise or operation.
     */
    OR("|"),
    /**
     * Bitwise and operation.
     */
    AND("&"),
    /**
     * Bitwise left shift operation.
     */
    LEFT_SHIFT("<<"),
    /**
     * Bitwise right shift operation.
     */
    RIGHT_SHIFT(">>"),
    /**
     * Bitwise xor operation.
     */
    XOR("^"),
    /**
     * Open bracket.
     */
    OPEN_BRACKET("("),
    /**
     * Close bracket.
     */
    CLOSE_BRACKET(")"),
    /**
     * Variable.
     */
    VARIABLE("\\d+"),
    /**
     * End of expression marker.
     */
    END("$");

    /**
     * Token string value.
     */
    private final String tokenString;

    /**
     * Lookup for static obtaining of token object.
     */
    private static final Map<String, Token> LOOKUP = new HashMap<>();

    static {
        for (Token token : Token.values()) {
            LOOKUP.put(token.getTokenString(), token);
        }
    }

    /**
     * Constructor for token object.
     *
     * @param value string token value.
     */
    Token(final String value) {
        tokenString = value;
    }

    /**
     * Getter for token value.
     *
     * @return token string value.
     */
    public String getTokenString() {
        return tokenString;
    }

    /**
     * Checks whether current token is a binary operator.
     *
     * @return boolean that indicates whether current token is a
     * binary operator.
     */
    public Boolean isBinaryOperator() {
        switch (this) {
            case OR:
            case AND:
            case XOR:
            case LEFT_SHIFT:
            case RIGHT_SHIFT:
                return true;
            default:
                return false;
        }
    }

    /**
     * Returns precedence of a token in case it's a operator.
     *
     * @return precedence of a operator.
     */
    public int getPrecedence() {
        final int three = 3;
        final int four = 4;
        final int five = 5;
        switch (this) {
            case NOT:
                return 1;
            case LEFT_SHIFT:
            case RIGHT_SHIFT:
                return 2;
            case AND:
                return three;
            case XOR:
                return four;
            case OR:
                return five;
            default:
                return 0;
        }
    }

    /**
     * Obtains token object by string.
     *
     * @param tokenString token string value.
     * @return token object.
     */
    public static Token getToken(String tokenString) {
        if (tokenString.matches("\\d+")) {
            tokenString = "\\d+";
        }
        return LOOKUP.get(tokenString);
    }
}
