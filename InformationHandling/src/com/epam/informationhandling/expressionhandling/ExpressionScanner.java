package com.epam.informationhandling.expressionhandling;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Class for traversing through expression string.
 */
public class ExpressionScanner {

    /**
     * Root logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Constant that specifies the end of the expression.
     */
    private static final String END = "$";
    /**
     * Pattern for matching tokens.
     */
    private static final String SCAN_PATTERN = "[()^~&|$]|[<>]{1,2}|\\d+";

    /**
     * Compiled regext pattern.
     */
    private static Pattern pattern = Pattern.compile(SCAN_PATTERN);

    /**
     * Scanner class that scans the expression string.
     */
    private Scanner expressionScanner;
    /**
     * Current string value of scanner.
     */
    private String nextTokenString;
    /**
     * Indicates whether old string value was consumed or not.
     */
    private Boolean isConsumed;

    /**
     * Default constructor.
     *
     * @param newExpression unparsed expression.
     */
    public ExpressionScanner(final String newExpression) {
        expressionScanner = new Scanner(newExpression + END);
        expressionScanner.useDelimiter("");

        isConsumed = true;
    }

    /**
     * Sets next string token.
     */
    private void next() {
        if (isConsumed) {
            if (expressionScanner.hasNext(pattern)) {
                nextTokenString = expressionScanner.findInLine(pattern);
            }
            isConsumed = false;
        }
    }

    /**
     * Returns next token object.
     *
     * @return token object.
     */
    public Token nextToken() {
        next();
        return Token.getToken(nextTokenString);
    }

    /**
     * Returns next string token value.
     *
     * @return string token value.
     */
    public String nextTokenValue() {
        next();
        return nextTokenString;
    }

    /**
     * Consumes next token, and moves forward to another.
     */
    public void consume() {
        isConsumed = true;
    }

    /**
     * Checks whether expected token corresponds to actual one.
     *
     * @param token expected token
     * @throws ExpressionException is thrown if expected token doesn't
     *                             correspond to actual one.
     */
    public void expect(final Token token) throws ExpressionException {
        if (nextToken() == token) {
            consume();
        } else {
            LOGGER.error("Expression parsing: Unexpected first token!");
            throw new ExpressionException("Unexpected token");
        }
    }
}
