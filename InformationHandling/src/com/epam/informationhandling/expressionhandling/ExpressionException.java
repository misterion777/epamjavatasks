package com.epam.informationhandling.expressionhandling;

/**
 * Exception that occurs during expression parsing.
 */
public class ExpressionException extends RuntimeException {

    /**
     * Default exception constructor with message
     * @param message exception message.
     */
    public ExpressionException(final String message){
        super(message);
    }

}
