/**
 * Package containing logic for expression parsing and calculating.
 */
package com.epam.informationhandling.expressionhandling;
