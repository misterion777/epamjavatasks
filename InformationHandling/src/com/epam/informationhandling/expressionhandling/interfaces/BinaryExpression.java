package com.epam.informationhandling.expressionhandling.interfaces;

/**
 * Interface for calculating binary expressions.
 */
@FunctionalInterface
public interface BinaryExpression {
    /**
     * Calculates expression with two operands.
     * @param left first operand
     * @param right second operand
     * @return expression result
     */
    int interpret(int left, int right);
}
