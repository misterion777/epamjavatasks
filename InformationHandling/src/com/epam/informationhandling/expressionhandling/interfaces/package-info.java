/**
 * Package containing functional interfaces for expression operators.
 */
package com.epam.informationhandling.expressionhandling.interfaces;
