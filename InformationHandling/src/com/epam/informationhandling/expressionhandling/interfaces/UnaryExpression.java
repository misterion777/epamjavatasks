package com.epam.informationhandling.expressionhandling.interfaces;

/**
 * Interface for calculating unary expressions.
 */
@FunctionalInterface
public interface UnaryExpression {
    /**
     * Calculates expression with single operand.
     * @param operand single operand
     * @return expression result
     */
    int interpret(int operand);
}
