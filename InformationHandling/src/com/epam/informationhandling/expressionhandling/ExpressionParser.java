package com.epam.informationhandling.expressionhandling;

import com.epam.informationhandling.expressionhandling.expression.BinaryOperator;
import com.epam.informationhandling.expressionhandling.expression.Expression;
import com.epam.informationhandling.expressionhandling.expression.Operand;
import com.epam.informationhandling.expressionhandling.expression.OperatorFactory;
import com.epam.informationhandling.expressionhandling.expression.UnaryOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Parser of expressions.
 */
public class ExpressionParser {

    /**
     * Root logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Expression scanner object.
     */
    private ExpressionScanner scanner;

    /**
     * Default constructor.
     *
     * @param expression unparsed string expression.
     */
    public ExpressionParser(final String expression) {
        scanner = new ExpressionScanner(expression);
    }

    /**
     * Parses the expression passed to constructor.
     *
     * @return tree-like expression object.
     */
    public Expression parse() {
        Expression expression = parseGroup(0);
        scanner.expect(Token.END);
        LOGGER.info("Expression parsed!");
        return expression;
    }

    /**
     * Parses group according to precedence.
     *
     * @param precedence priority of operators.
     * @return tree-like expression object.
     */
    private Expression parseGroup(final int precedence) {
        Expression expression = parseNext();
        while (scanner.nextToken().isBinaryOperator()
                && scanner.nextToken().getPrecedence() >= precedence) {
            BinaryOperator operator = OperatorFactory.
                    getBinaryOperator(scanner.nextTokenValue());
            scanner.consume();
            Expression nextExpression = parseGroup(operator.getPrecedence());
            operator.addChildren(expression, nextExpression);
            expression = operator;
        }

        return expression;
    }

    /**
     * Parses single token like unary operators or brackets.
     *
     * @return tree-like expression object.
     */
    private Expression parseNext() {
        if (scanner.nextToken() == Token.NOT) {
            UnaryOperator operator = OperatorFactory
                    .getUnaryOperator(scanner.nextTokenValue());
            scanner.consume();
            Expression expression = parseGroup(operator.getPrecedence());
            operator.addChild(expression);
            return operator;
        } else if (scanner.nextToken() == Token.OPEN_BRACKET) {
            scanner.consume();
            Expression t = parseGroup(0);
            scanner.expect(Token.CLOSE_BRACKET);
            return t;
        } else if (scanner.nextToken() == Token.VARIABLE) {
            Expression expression = new Operand(scanner.nextTokenValue());
            scanner.consume();
            return expression;
        } else {
            LOGGER.error("Expression parsing: Unexpected first token!");
            throw new ExpressionException("Unexpected first token");
        }
    }
}
