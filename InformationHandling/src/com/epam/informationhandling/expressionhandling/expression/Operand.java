package com.epam.informationhandling.expressionhandling.expression;


/**
 * Operand in expression tree.
 */
public class Operand extends Expression {
    /**
     * Integer value of operand.
     */
    private int value;

    /**
     * Default constructor.
     * @param token string value
     */
    public Operand(final String token) {
        super(token);

        value = Integer.parseInt(token);
    }

    /**
     * Getter for value.
     * @return integer value of operand
     */
    public int getValue() {
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Operand operand = (Operand) o;
        return value == operand.value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + value;
        result = prime * result + ((getTokenString() == null) ? 0
                : getTokenString().hashCode());
        return result;
    }
}
