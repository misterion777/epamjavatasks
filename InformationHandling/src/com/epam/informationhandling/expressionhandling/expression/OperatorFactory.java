package com.epam.informationhandling.expressionhandling.expression;

/**
 * Factory for creating operators objects.
 */
public final class OperatorFactory {

    /**
     * Private default constructor.
     */
    private OperatorFactory() {
    }

    /**
     * Creates binary operator by string token.
     *
     * @param tokenString string token.
     * @return binary operator object.
     */
    public static BinaryOperator getBinaryOperator(final String tokenString) {
        final int three = 3;
        final int four = 4;
        final int five = 5;
        switch (tokenString) {
            case "<<":
                return new BinaryOperator(2, tokenString,
                        (x, y) -> (x << y));
            case ">>":
                return new BinaryOperator(2, tokenString,
                        (x, y) -> (x >> y));
            case "&":
                return new BinaryOperator(three, tokenString,
                        (x, y) -> (x & y));
            case "^":
                return new BinaryOperator(four, tokenString,
                        (x, y) -> (x ^ y));
            case "|":
                return new BinaryOperator(five, tokenString,
                        (x, y) -> (x | y));
            default:
                return null;
        }
    }

    /**
     * Creates unary operator by string token.
     *
     * @param tokenString string token.
     * @return binary operator object.
     */

    public static UnaryOperator getUnaryOperator(final String tokenString) {
        if (tokenString.equals("~")) {
            return new UnaryOperator(1, tokenString, x -> (~x));
        }
        return null;
    }
}
