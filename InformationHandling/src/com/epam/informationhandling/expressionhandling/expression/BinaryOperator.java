package com.epam.informationhandling.expressionhandling.expression;

import com.epam.informationhandling.expressionhandling.interfaces.BinaryExpression;

import java.util.Objects;

/**
 * Binary operator that contains two children.
 */
public class BinaryOperator extends AbstractOperator {

    /**
     * Expression that corresponds to operator.
     */
    private BinaryExpression expression;

    /**
     * Left child in an expression tree.
     */
    private Expression leftChild;
    /**
     * Right child in an expression tree.
     */
    private Expression rightChild;

    /**
     * Default constructor.
     *
     * @param precedence    precedence of the operator.
     * @param tokenString   string value of the operator.
     * @param newExpression expression that corresponds to operator.
     */
    public BinaryOperator(final int precedence,
                          final String tokenString,
                          final BinaryExpression newExpression) {
        super(precedence, tokenString);
        this.expression = newExpression;

    }


    /**
     * {@inheritDoc}
     */
    public int interpret() {
        int left;
        int right;
        if (leftChild instanceof AbstractOperator) {
            left = ((AbstractOperator) leftChild).interpret();

        } else {
            left = ((Operand) leftChild).getValue();
        }

        if (rightChild instanceof AbstractOperator) {
            right = ((AbstractOperator) rightChild).interpret();
        } else {
            right = ((Operand) rightChild).getValue();
        }

        return expression.interpret(left, right);
    }


    /**
     * Add two children to this operator.
     *
     * @param leftExpression  left child in a tree.
     * @param rightExpression right child in a tree.
     */
    public void addChildren(final Expression leftExpression,
                            final Expression rightExpression) {
        this.leftChild = leftExpression;
        this.rightChild = rightExpression;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        BinaryOperator that = (BinaryOperator) o;

        return Objects.equals(leftChild, that.leftChild)
                && Objects.equals(rightChild, that.rightChild);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + super.hashCode();
        result = prime * result + ((leftChild == null) ? 0
                : leftChild.hashCode());
        result = prime * result + ((rightChild == null) ? 0
                : rightChild.hashCode());
        result = prime * result + ((expression == null) ? 0
                : expression.hashCode());

        return result;
    }
}
