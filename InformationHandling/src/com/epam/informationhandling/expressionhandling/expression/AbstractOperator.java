package com.epam.informationhandling.expressionhandling.expression;

/**
 * Abstract operator in expression.
 */
public abstract class AbstractOperator extends Expression {

    /**
     * Priority of order of interpretation in expression.
     */
    private int precedence;

    /**
     * Default constructor.
     *
     * @param newPrecedence precedence of current operator.
     * @param tokenString   string value of current operator.
     */
    public AbstractOperator(final int newPrecedence, final String tokenString) {
        super(tokenString);
        this.precedence = newPrecedence;
    }

    /**
     * Getter for precedence.
     *
     * @return precedence
     */
    public int getPrecedence() {
        return precedence;
    }

    /**
     * Calculates the result of operation.
     *
     * @return result of operation.
     */
    public abstract int interpret();

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AbstractOperator that = (AbstractOperator) o;

        return (precedence == that.precedence
                && getTokenString().equals(that.getTokenString()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + precedence;
        result = prime * result
            + ((getTokenString() == null) ? 0 : getTokenString().hashCode());
        return result;
    }
}
