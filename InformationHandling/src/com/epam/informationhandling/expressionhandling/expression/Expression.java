package com.epam.informationhandling.expressionhandling.expression;


/**
 * Basic expression class.
 */
public class Expression {

    /**
     * String value of a expression in tree.
     */
    private String tokenString;

    /**
     * Default constructor.
     *
     * @param value string value
     */
    public Expression(final String value) {
        tokenString = value;
    }

    /**
     * Getter for token value.
     *
     * @return string value of an expression.
     */
    public String getTokenString() {
        return tokenString;
    }

    /**
     * Setter for a token string.
     *
     * @param newTokenString token string.
     */
    public void setTokenString(final String newTokenString) {
        this.tokenString = newTokenString;
    }
}
