package com.epam.informationhandling.expressionhandling.expression;

import com.epam.informationhandling.expressionhandling.interfaces.UnaryExpression;

import java.util.Objects;

/**
 * Unary operator in expression tree.
 */
public class UnaryOperator extends AbstractOperator {

    /**
     * Expression that corresponds to operator.
     */
    private UnaryExpression expression;

    /**
     * Single child for current operator.
     */
    private Expression child;

    /**
     * Default constructor.
     *
     * @param precedence      precedence for operator
     * @param tokenString     string value
     * @param unaryExpression Expression that corresponds to operator.
     */
    public UnaryOperator(final int precedence,
                         final String tokenString,
                         final UnaryExpression unaryExpression) {
        super(precedence, tokenString);
        this.expression = unaryExpression;
    }

    /**
     * Add single child to operator.
     *
     * @param newChild expression child.
     */
    public void addChild(final Expression newChild) {
        this.child = newChild;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int interpret() {

        int left;

        if (child instanceof AbstractOperator) {
            left = ((AbstractOperator) child).interpret();

        } else {
            left = ((Operand) child).getValue();
        }

        return expression.interpret(left);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        UnaryOperator that = (UnaryOperator) o;
        return Objects.equals(expression, that.expression)
                && Objects.equals(child, that.child);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + super.hashCode();
        result = prime * result + ((child == null) ? 0 : child.hashCode());
        result = prime * result + ((expression == null) ? 0
                : expression.hashCode());

        return result;
    }
}
