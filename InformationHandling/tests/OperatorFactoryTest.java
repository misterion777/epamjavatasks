import com.epam.informationhandling.expressionhandling.ExpressionParser;
import com.epam.informationhandling.expressionhandling.expression.AbstractOperator;
import com.epam.informationhandling.expressionhandling.expression.BinaryOperator;
import com.epam.informationhandling.expressionhandling.expression.Expression;
import com.epam.informationhandling.expressionhandling.expression.Operand;
import com.epam.informationhandling.expressionhandling.expression.OperatorFactory;
import com.epam.informationhandling.expressionhandling.expression.UnaryOperator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@Test
public class OperatorFactoryTest {

    @DataProvider(name = "binaryOperatorData")
    public Object[][] binaryOperatorData(){
        return new Object[][] {
                {"&", new BinaryOperator(3, "&", (x, y) -> (x & y))},
                {"<<", new BinaryOperator(2, "<<", (x, y) -> (x << y))},
                {"|", new BinaryOperator(5, "|", (x, y) -> (x | y))},
                {"^", new BinaryOperator(4, "^", (x, y) -> (x ^ y))},
        };
    }

    @Test(dataProvider = "binaryOperatorData")
    public void testGetBinaryOperator(String inputData, BinaryOperator expectedResult){
        BinaryOperator result = OperatorFactory.getBinaryOperator(inputData);
        Assert.assertEquals(result,expectedResult);
    }

}
