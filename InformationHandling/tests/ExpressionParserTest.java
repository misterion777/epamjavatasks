import com.epam.informationhandling.expressionhandling.ExpressionParser;
import com.epam.informationhandling.expressionhandling.expression.AbstractOperator;
import com.epam.informationhandling.expressionhandling.expression.BinaryOperator;
import com.epam.informationhandling.expressionhandling.expression.Expression;
import com.epam.informationhandling.expressionhandling.expression.Operand;
import com.epam.informationhandling.expressionhandling.expression.OperatorFactory;
import com.epam.informationhandling.expressionhandling.expression.UnaryOperator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@Test
public class ExpressionParserTest {

    private AbstractOperator operator;


    @DataProvider(name = "expressionData")
    public Object[][] expressionData(){
        UnaryOperator not = OperatorFactory.getUnaryOperator("~");
        BinaryOperator and1 = OperatorFactory.getBinaryOperator("&");
        BinaryOperator and2 = OperatorFactory.getBinaryOperator("&");
        BinaryOperator or = OperatorFactory.getBinaryOperator("|");
        and2.addChildren(new Operand("3"), new Operand("4"));
        or.addChildren(new Operand("9"), and2);
        and1.addChildren(new Operand("6"), or);
        not.addChild(and1);
        operator = not;

        return new Object[][] {
                {"~6&9|(3&4)", operator}
        };
    }

    @Test(dataProvider = "expressionData")
    public void testParse(String inputData, Expression expectedResult){
        ExpressionParser parser = new ExpressionParser(inputData);
        Expression exp = parser.parse();

        Assert.assertEquals(exp,expectedResult);
    }

    @Test(dependsOnMethods = "testParse")
    public void testInterpret(){
        int expectedResult = -1;
        int result = operator.interpret();

        Assert.assertEquals(result, expectedResult);
    }
}
