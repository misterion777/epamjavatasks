import com.epam.informationhandling.parser.AbstractParser;
import com.epam.informationhandling.parser.LexemeParser;
import com.epam.informationhandling.parser.ParagraphParser;
import com.epam.informationhandling.parser.SentenceParser;
import com.epam.informationhandling.parser.TextParser;
import com.epam.informationhandling.parser.WordParser;
import com.epam.informationhandling.text.AbstractTextComponent;
import com.epam.informationhandling.text.Character;
import com.epam.informationhandling.text.Lexeme;
import com.epam.informationhandling.text.Paragraph;
import com.epam.informationhandling.text.PunctuationMark;
import com.epam.informationhandling.text.Sentence;
import com.epam.informationhandling.text.Text;
import com.epam.informationhandling.text.Word;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@Test
public class TextComponentParsersTest {

    private AbstractTextComponent currentComponent;
    private AbstractParser currentParser;

    @BeforeClass
    public void initialize() {
        currentComponent = new Word();
        currentParser = new WordParser();
    }

    @DataProvider(name = "wordData")
    public Object[][] wordData(){

        currentComponent.add(new Character("f"));
        currentComponent.add(new Character("o"));
        currentComponent.add(new Character("o"));

        return new Object[][] {
                {"foo", currentComponent}
        };
    }

    @DataProvider(name = "lexemeData")
    public Object[][] lexemeData(){
        PunctuationMark mark = new PunctuationMark();
        mark.add(new Character(","));

        Lexeme lexeme = new Lexeme();
        lexeme.add(currentComponent);
        lexeme.add(mark);

        currentComponent = lexeme;

        return new Object[][] {
                {"foo,", currentComponent}
        };
    }

    @DataProvider(name = "sentenceData")
    public Object[][] sentenceData(){

        Word word1 = new Word();
        word1.add(new Character("B"));
        word1.add(new Character("a"));
        word1.add(new Character("z"));

        PunctuationMark mark1 = new PunctuationMark();
        mark1.add(new Character(","));

        Lexeme lexeme1 = new Lexeme();
        lexeme1.add(word1);
        lexeme1.add(mark1);

        Word word2 = new Word();
        word2.add(new Character("b"));
        word2.add(new Character("a"));
        word2.add(new Character("r"));

        PunctuationMark mark2 = new PunctuationMark();
        mark2.add(new Character("."));

        Lexeme lexeme2 = new Lexeme();
        lexeme2.add(word2);
        lexeme2.add(mark2);

        Sentence sentence = new Sentence();
        sentence.add(lexeme1);
        sentence.add(currentComponent);
        sentence.add(lexeme2);

        currentComponent = sentence;

        return new Object[][] {
                {"Baz, foo, bar.", currentComponent}
        };
    }

    @DataProvider(name = "paragraphData")
    public Object[][] paragraphData(){
        Paragraph paragraph = new Paragraph();
        paragraph.add(currentComponent);
        paragraph.add(currentComponent);
        paragraph.add(currentComponent);

        currentComponent = paragraph;

        return new Object[][] {
                {"Baz, foo, bar. Baz, foo, bar. Baz, foo, bar.",
                        currentComponent}
        };
    }

    @DataProvider(name = "textData")
    public Object[][] textData(){
        Text text = new Text();
        text.add(currentComponent);
        text.add(currentComponent);
        text.add(currentComponent);

        currentComponent = text;

        return new Object[][] {
                {"    Baz, foo, bar. Baz, foo, bar. Baz, foo, bar.\n" +
                 "    Baz, foo, bar. Baz, foo, bar. Baz, foo, bar.\n" +
                 "    Baz, foo, bar. Baz, foo, bar. Baz, foo, bar.",
                 currentComponent}
        };
    }





    @Test(dataProvider = "wordData")
    public void testWordParser(String inputData, Word expectedResult){
        Word word = (Word) currentParser.parse(inputData);
        Assert.assertEquals(expectedResult, word, expectedResult.getText());
    }


    @Test(dataProvider = "lexemeData", dependsOnMethods = {"testWordParser"})
    public void testLexemeParser(String inputData, Lexeme expectedResult){
        LexemeParser parser = new LexemeParser();
        parser.linkWith(currentParser);
        currentParser = parser;

        Lexeme lexeme = (Lexeme) parser.parse(inputData);
        Assert.assertEquals(expectedResult, lexeme, expectedResult.getText());
    }

    @Test(dataProvider = "sentenceData", dependsOnMethods = {
            "testLexemeParser", "testWordParser"})
    public void testSentenceParser(String inputData, Sentence expectedResult){
        SentenceParser parser = new SentenceParser();
        parser.linkWith(currentParser);
        currentParser = parser;

        Sentence sentence = (Sentence) parser.parse(inputData);
        Assert.assertEquals(expectedResult, sentence, expectedResult.getText());
    }

    @Test(dataProvider = "paragraphData", dependsOnMethods = {
            "testLexemeParser", "testWordParser", "testSentenceParser"})
    public void testParagraphParser(String inputData, Paragraph expectedResult){
        ParagraphParser parser = new ParagraphParser();
        parser.linkWith(currentParser);
        currentParser = parser;

        Paragraph paragraph = (Paragraph) parser.parse(inputData);
        Assert.assertEquals(expectedResult, paragraph,
                expectedResult.getText());
    }

    @Test(dataProvider = "textData", dependsOnMethods = {
            "testLexemeParser", "testWordParser", "testSentenceParser",
            "testParagraphParser"})
    public void testTextParser(String inputData, Text expectedResult){
        TextParser parser = new TextParser();
        parser.linkWith(currentParser);
        currentParser = parser;

        Text text = (Text) parser.parse(inputData);

        Assert.assertEquals(text, expectedResult,
                expectedResult.getText());
    }

    @Test(dependsOnMethods = {
            "testLexemeParser", "testWordParser", "testSentenceParser",
            "testParagraphParser", "testTextParser"})
    public void testGetText(){
        String expectedResult = "    Baz, foo, bar. Baz, foo, bar. Baz, foo, bar. \n" +
                "    Baz, foo, bar. Baz, foo, bar. Baz, foo, bar. \n" +
                "    Baz, foo, bar. Baz, foo, bar. Baz, foo, bar. \n";

        Assert.assertEquals(currentComponent.getText(), expectedResult);
    }
}
