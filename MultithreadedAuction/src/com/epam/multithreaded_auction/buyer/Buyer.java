package com.epam.multithreaded_auction.buyer;

import com.epam.multithreaded_auction.auction.Auction;
import com.epam.multithreaded_auction.bid.Bid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Callable class that represents buyer on auction.
 */
public class Buyer implements Callable<Boolean> {
    /**
     * Root logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Name of a buyer.
     */
    private String name;

    /**
     * Amount of money that buyer has.
     */
    private float balance;
    /**
     * List of in which this buyer is interested.
     */
    private List<String> interestingBidNames;

    /**
     * Default constructor for a buyer class.
     * @param buyerName name of a buyer.
     * @param buyerBalance current balance that buyer has.
     * @param bids list of bids in which buyer is interested.
     */
    public Buyer(final String buyerName,
                 final float buyerBalance, final List<String> bids) {
        name = buyerName;
        balance = buyerBalance;
        interestingBidNames = bids;
    }

    /**
     * Method representing actions of a buyer on auction.
     * Buyer will increase price of a current bid anytime except:
     * he is not interested in current bid, he has already the highest offer or
     * he has no more money for increase.
     * @return boolean value that indicates whether this buyer is interested in
     * continuation of increasing price for current bid or not.
     */
    @Override
    public Boolean call() {

        Auction auction =  Auction.getAuctionObject();
        Bid currentBid = auction.getCurrentBid();
        boolean isInterested;

        if (interestingBidNames.contains(currentBid.toString())) {

            if (currentBid.getLatestBuyer() == this) {
                LOGGER.info("{} has already highest price offer on {})",
                        name, currentBid);
                isInterested = true;
            } else if (balance < currentBid.getNextPrice()) {
                LOGGER.info("{} has not enough money for {}", name, currentBid);
                isInterested = false;
            } else {
                LOGGER.info("{} increased cost for {} to {}",
                        name, currentBid, currentBid.getNextPrice());
                currentBid.increasePrice(this);
                isInterested = true;
            }

        } else {
            LOGGER.info("{} is not interested in {}", name, currentBid);
            isInterested = false;
        }

        return isInterested;
    }

    /**
     * Decreases current balance after winning the bid.
     */
    public void decreaseBalance() {
        this.balance -= Auction.getAuctionObject().getCurrentBid()
                .getCurrentPrice();
    }

    /**
     * {@inheritDoc}
     */
    public String toString() {
        return name;
    }

    /**
     * Getter for name of a buyer.
     * @return name of a buyer.
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for name of a buyer.
     * @param newName new name of a buyer.
     */
    public void setName(final String newName) {
        this.name = newName;
    }

    /**
     * Getter for current balance.
     * @return current balance of a buyer.
     */
    public float getBalance() {
        return balance;
    }

    /**
     * Getter for list of bids.
     * @return list of bids, buyer is interested in.
     */
    public List<String> getInterestingBidNames() {
        return interestingBidNames;
    }

    /**
     * Setter for list of bids.
     * @param newInterestingBids new list of bids, buyer is interested in.
     */
    public void setInterestingBidNames(final List<String> newInterestingBids) {
        this.interestingBidNames = newInterestingBids;
    }

    /**
     * Setter for current balance.
     * @param newBalance new balance of a buyer.
     */
    public void setBalance(final float newBalance) {
        this.balance = newBalance;

    }
}
