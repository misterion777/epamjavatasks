package com.epam.multithreaded_auction.creator;

import com.epam.multithreaded_auction.auction.Auction;
import com.epam.multithreaded_auction.parser.InputParser;
import com.epam.multithreaded_auction.validator.InputValidator;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.util.stream.Stream;

/**
 * Single auction object initializer.
 */
public class AuctionInitializer {

    /**
     * Initializes single auction object.
     * @param bidsFilePath path to file containing info for all bids that
     *                     are being sold on auction.
     * @param buyersFilePath path to file containing info for all buyers that
     *                       participating in auction.
     */
    public void initAuctionData(final String bidsFilePath,
                                final String buyersFilePath) {
        InputParser parser = new InputParser();
        InputValidator validator = new InputValidator();
        AuctionObjectsCreator creator = new AuctionObjectsCreator();
        try {
            Stream<String> bidsStream = parser.parseBid(bidsFilePath);
            bidsStream = validator.validateBids(bidsStream);

            Auction.getAuctionObject().setBids(creator.createBids(bidsStream));

            Stream<String> buyersStream = parser.parseBuyers(buyersFilePath);
            buyersStream = validator.validateBuyers(buyersStream);

            Auction.getAuctionObject()
                    .setBuyers(creator.createBuyers(buyersStream));

        } catch (IOException e) {
            LogManager.getLogger().error(e.getMessage());
        }
    }
}
