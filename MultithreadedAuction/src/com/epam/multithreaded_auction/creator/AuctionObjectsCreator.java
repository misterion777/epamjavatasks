package com.epam.multithreaded_auction.creator;

import com.epam.multithreaded_auction.bid.Bid;
import com.epam.multithreaded_auction.buyer.Buyer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Class for creating objects necessary for auction to be held.
 */
class AuctionObjectsCreator {

    /**
     * Creates list of buyers that are participating in auction.
     * @param buyersStream parsed stream containing
     *                     string representing buyers objects
     * @return list of buyer objects that participate in auction.
     */
    List<Buyer> createBuyers(final Stream<String> buyersStream) {
        List<Buyer> personList = new ArrayList<>();
        buyersStream.forEach(line -> personList.add(createBuyer(line)));
        return personList;
    }


    /**
     * Creates single buyer from string.
     * @param line string representing buyer object
     * @return buyer object
     */
    private Buyer createBuyer(final String line) {
        String[] splitted = line.split(" ");
        List<String> bids = new ArrayList<>(Arrays.asList(splitted)
                .subList(2, splitted.length));
        return new Buyer(splitted[0], Float.parseFloat(splitted[1]), bids);
    }

    /**
     * Creates list of bids that are being sold in auction.
     * @param bidsStream parsed stream containing
     *                     string representing bids objects
     * @return list of bids objects that are being sold in auction.
     */
     List<Bid> createBids(final Stream<String> bidsStream) {
        List<Bid> bidList = new ArrayList<>();
        bidsStream.forEach(line -> {
            Bid bid = createBid(line);
            if (!bidList.contains(bid)) {
                bidList.add(bid);
            }
        });
        return  bidList;
    }

    /**
     * Creates single bid from string.
     * @param line string representing bid object
     * @return bid object
     */
    private Bid createBid(final String line) {
        String[] splitted = line.split(" ");
        return new Bid(splitted[0], Float.parseFloat(splitted[1]),
                Integer.parseInt(splitted[2]));
    }
}
