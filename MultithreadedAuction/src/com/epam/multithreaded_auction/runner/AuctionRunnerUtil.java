package com.epam.multithreaded_auction.runner;

import com.epam.multithreaded_auction.auction.Auction;
import com.epam.multithreaded_auction.bid.Bid;
import com.epam.multithreaded_auction.buyer.Buyer;
import com.epam.multithreaded_auction.creator.AuctionInitializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Class that behaves as auctioneer: initializes auction data,
 * controls buyers (threads), determines winners of bids.
 */
public final class AuctionRunnerUtil {

    /**
     * Path to input data for bids objects.
     */
    private static final String BIDS_INPUT_FILE = "./data/bids1.txt";
    /**
     * Path to input data for buyers objects.
     */
    private static final String BUYERS_INPUT_FILE = "./data/buyers1.txt";
    /**
     * Root logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Private default constructor.
     */
    private AuctionRunnerUtil() {

    }

    /**
     * Performs auction process until all bids are closed.
     * @param args input arguments
     */
    public static void main(final String[] args) {

        initAuction();

        while (Auction.getAuctionObject().getCurrentBid() != null) {

            sellCurrentBid();

            getSellResult();

            Auction.getAuctionObject().nextBid();
        }
    }

    /**
     * Auction initialization.
     */
    private static void initAuction() {
        AuctionInitializer init = new AuctionInitializer();
        init.initAuctionData(BIDS_INPUT_FILE, BUYERS_INPUT_FILE);
    }

    /**
     * Performs current bid selling.
     */
    private static void sellCurrentBid() {
        Bid currentBid = Auction.getAuctionObject().getCurrentBid();
        List<Buyer> buyers = Auction.getAuctionObject().getBuyers();

        LOGGER.info("Current bid is {}", currentBid);

        for (int i = 0; i < currentBid.getTimeout(); i++) {

            LOGGER.info("Round: {}", i);
            int interestedBuyersAmount = 0;
            ExecutorService executor
                    = Executors.newFixedThreadPool(buyers.size());
            try {
                List<Future<Boolean>> futures = executor.invokeAll(buyers);
                executor.shutdown();

                for (Future<Boolean> future : futures) {
                    if (future.get()) {
                        interestedBuyersAmount += 1;
                    }
                }

                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.error(e.getMessage());
            }

            final int minBuyersAmount = 2;
            if (interestedBuyersAmount < minBuyersAmount) {
                break;
            }
        }

    }

    /**
     * Determines result of the trade.
     */
    private static void getSellResult() {
        Bid currentBid = Auction.getAuctionObject().getCurrentBid();

        if (currentBid.getLatestBuyer() == null) {
            LOGGER.info("{} is uninterested. Bid is closed.", currentBid);
        } else {
            LOGGER.info("{} is sold! Winner is: {}", currentBid,
                    currentBid.getLatestBuyer());

            currentBid.getLatestBuyer().decreaseBalance();
            LOGGER.info("Starting prise was: {}. Buyout price is: {}.",
                    currentBid.getStartPrice(), currentBid.getCurrentPrice());
        }

        final int dashAmount = 50;
        LOGGER.info(new String(new char[dashAmount]).replace("\0", "-"));
    }
}
