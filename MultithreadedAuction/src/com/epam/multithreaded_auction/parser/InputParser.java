package com.epam.multithreaded_auction.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Class for parcing input data files.
 */
public class InputParser {

    /**
     * Root logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();
    /**
     * Regex for valid data for bid objects.
     */
    private static final String BID_REGEX = "^\\w+ \\d*[.,]?\\d+ \\d+$";
    /**
     * Regex for valid data for buyers objects.
     */
    private static final String BUYER_REGEX
            = "^\\w+ \\d*[.,]?\\d+ (\\w+( |$))+";

    /**
     * Parses file and creates valid stream for creating buyers objects.
     * @param filePath path to file with input data
     * @return stream of strings used to create buyers objects
     * @throws IOException thrown if there is no such file
     */
    public Stream<String> parseBuyers(final String filePath)
            throws IOException {

        Stream<String> stream = Files.lines(Paths.get(filePath));
        return stream.filter(line -> {
            if (!line.matches(BUYER_REGEX)) {
                LOGGER.error("{} is not valid line!", line);
            }
            return line.matches(BUYER_REGEX);
        });
    }

    /**
     * Parses file and creates valid stream for creating bids objects.
     * @param filePath path to file with input data
     * @return stream of strings used to create bids objects
     * @throws IOException thrown if there is no such file
     */
    public Stream<String> parseBid(final String filePath) throws IOException {

        Stream<String> stream = Files.lines(Paths.get(filePath));
        return stream.filter(line -> {
            if (!line.matches(BID_REGEX)) {
                LOGGER.error("{} is not valid line!", line);
            }
            return line.matches(BID_REGEX);
        });
    }
}
