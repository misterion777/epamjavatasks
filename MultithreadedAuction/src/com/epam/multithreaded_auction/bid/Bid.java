package com.epam.multithreaded_auction.bid;

import com.epam.multithreaded_auction.buyer.Buyer;

import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Bid class representing bid on auction.
 */
public class Bid {
    /**
     * Percentage to which bid price increases by buyers.
     */
    private static final float INCREASE_PERCENTAGE = 0.1f;

    /**
     * Locker for thread safety.
     */
    private final transient ReentrantLock lock = new ReentrantLock();
    /**
     * Name of the bid.
     */
    private final transient String name;
    /**
     * Starting price of the bid.
     */
    private final transient float startPrice;
    /**
     * Current price of the bid.
     */
    private transient float currentPrice;
    /**
     * Number of rounds during which this bid is sold on auction.
     */
    private int timeout;

    /**
     * Object of buyer that is ready to pay the largest amount of money
     * for this bid.
     */
    private Buyer latestBuyer;

    /**
     * Constructor for bid.
     * @param bidName name of this bid.
     * @param price starting price for bid.
     * @param bidTimeout number of rounds this bid lasts.
     */
    public Bid(final String bidName, final float price, final int bidTimeout) {
        name = bidName;
        startPrice = price;
        currentPrice = startPrice;
        timeout = bidTimeout;
    }

    /**
     * Increases current cost and saves buyer that did that.
     * @param buyer buyer that increases current cost.
     */
    public void increasePrice(final Buyer buyer) {
        lock.lock();
        currentPrice = getNextPrice();
        latestBuyer = buyer;
        lock.unlock();
    }

    /**
     * Returns price after next increase.
     * @return price that this bid will cost after price increase
     */
    public float getNextPrice() {
        return currentPrice * (1 + INCREASE_PERCENTAGE);
    }

    /**
     * Getter for current price of this bid.
     * @return current price.
     */
    public float getCurrentPrice() {
        return currentPrice;
    }

    /**
     * Getter for staring price of this bid.
     * @return starting price of a bid.
     */
    public float getStartPrice() {
        return startPrice;
    }


    /**
     * Getter for bid timeout.
     * @return bid timeout.
     */
    public int getTimeout() {
        return timeout;
    }

    /**
     * Setter for bid timeout.
     * @param newTimeout new timeout value.
     */
    public void setTimeout(final int newTimeout) {
        this.timeout = newTimeout;
    }

    /**
     * Getter for latest buyer.
     * @return latest buyer object.
     */
    public Buyer getLatestBuyer() {
        return latestBuyer;
    }

    /**
     * Setter for latest buyer.
     * @param newLatestBuyer latest buyer that did something with this bid.
     */
    public void setLatestBuyer(final Buyer newLatestBuyer) {
        this.latestBuyer = newLatestBuyer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Bid bid = (Bid) obj;
        return Objects.equals(name, bid.name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return name;
    }
}
