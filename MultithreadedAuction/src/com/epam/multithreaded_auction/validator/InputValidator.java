package com.epam.multithreaded_auction.validator;

import com.epam.multithreaded_auction.auction.Auction;
import com.epam.multithreaded_auction.bid.Bid;

import java.util.stream.Stream;
import java.util.List;

/**
 * Validates input string streams so real objects can be created.
 */
public class InputValidator {

    /**
     * Validate input bid string stream.
     * @param bidsStream input string stream for creating bid objects.
     * @return valid string stream.
     */
    public Stream<String> validateBids(final Stream<String> bidsStream) {

        return bidsStream.filter(line -> {
            String[] splitted = line.split(" ");
            return splitted[1].charAt(0) != '0' || splitted[2].charAt(0) != '0';
        });
    }

    /**
     * Validate input buyer string stream.
     * @param buyersStream input string stream for creating buyer objects.
     * @return valid string stream.
     */
    public Stream<String> validateBuyers(final Stream<String> buyersStream) {
        return buyersStream.filter(line -> {
            final int firstBidIndex = 2;
            String[] splitted = line.split(" ");
            boolean correctBidNames = true;
            List<Bid> auctionBids = Auction.getAuctionObject().getBids();

            if (splitted.length - firstBidIndex > auctionBids.size()) {
                correctBidNames = false;
            } else {
                for (int i = firstBidIndex; i < splitted.length; i++) {
                    if (!splitted[i].equals(auctionBids.get(i - firstBidIndex)
                            .toString())) {
                        correctBidNames = false;
                        break;
                    }
                }
            }
            return splitted[1].charAt(0) != '0' || !correctBidNames;
        });
    }
}
