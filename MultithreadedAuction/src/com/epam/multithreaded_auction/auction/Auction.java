package com.epam.multithreaded_auction.auction;

import com.epam.multithreaded_auction.bid.Bid;
import com.epam.multithreaded_auction.buyer.Buyer;

import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Thread-safe auctionObject, containing list of all buyers and bids.
 */
public final class Auction {

    /**
     * Singleton auctionObject object.
     */
    private static Auction auctionObject;
    /**
     * Locker for thread safety.
     */
    private static ReentrantLock lock = new ReentrantLock();

    /**
     * List of all available auctionObject bids.
     */
    private List<Bid> bids = new ArrayList<>();
    /**
     * List of participants in the auctionObject.
     */
    private List<Buyer> buyers = new ArrayList<>();
    /**
     * Index of current bid in list of all bids.
     */
    private transient int currentBidIndex;

    /**
     * Auction object can be obtained only by
     * {@link Auction#getAuctionObject()} function.
     */
    private Auction() {
        // Private default constructor
    }

    /**
     * Thread-safe singleton.
     * @return Auction object
     */
    public static Auction getAuctionObject() {
        lock.lock();
        if (auctionObject == null) {
            auctionObject = new Auction();
        }
        lock.unlock();
        return auctionObject;
    }

    /**
     * Returns last bid object from a list of bids.
     * @return bid object that is selling right now.
     */
    public Bid getCurrentBid() {
        lock.lock();
        Bid currentBid = null;
        if (currentBidIndex < bids.size()) {
            currentBid = bids.get(currentBidIndex);
        }
        lock.unlock();
        return currentBid;
    }


    /**
     * Moves to next bid in a list of bids.
     */
    public void nextBid() {
        currentBidIndex++;
    }

    /**
     * Getter for list of bids.
     * @return list of bids objects.
     */
    public List<Bid> getBids() {
        return bids;
    }

    /**
     * Setter for list of bids.
     * @param newBids list of all available bids.
     */
    public void setBids(final List<Bid> newBids) {
        this.bids = newBids;
    }

    /**
     * Getter for list of all participants of auctionObject.
     * @return list of buyers.
     */
    public List<Buyer> getBuyers() {
        return buyers;
    }

    /**
     * Setter for list of all participants of auctionObject.
     * @param newBuyers list of buyers.
     */
    public void setBuyers(final List<Buyer> newBuyers) {
        this.buyers = newBuyers;
    }
}
