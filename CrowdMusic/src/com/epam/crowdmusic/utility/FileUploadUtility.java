package com.epam.crowdmusic.utility;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import java.nio.file.StandardOpenOption;

/**
 * Utility for handling uploading files.
 */
public final class FileUploadUtility {

    /**
     * Path to folder where uploaded files are stored.
     */
    public static final String FULL_PATH =
        "/home/misterion/Development/Java/EPAM/epamjavatasks/CrowdMusicUploads";

    /**
     * Max threshold size for disk file item.
     */
    private static final int MAX_MEMORY_SIZE = 1024 * 1024 * 100;
    /**
     * Max upload request size.
     */
    private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 1000;


    /**
     * Delete file from file system.
     * @param filePath file to be deleted.
     * @return true, if successful.
     */
    public static boolean deleteFile(String filePath){
        File file = new File(FULL_PATH + filePath);
        return file.delete();
    }

    /**
     * Saves bytes to file by filepath
     * @param data raw data.
     * @param path file path
     * @param fileName file name
     * @throws IOException can occur when writing data to file.
     */
    public static void saveBytes(byte[] data, String path,
            String fileName)
            throws IOException {

        path = FULL_PATH + path;
        File directory = new File(path);
        if (!directory.exists()){
            Files.createDirectories(directory.toPath());
        }

        Path file = Paths.get(path + "/" + fileName);
        Files.write(file, data, StandardOpenOption.CREATE);
    }


    /**
     * Gets multipart file items from http request.
     * @param request http request.
     * @return list of multipart file items.
     * @throws FileUploadException can occur if uploaded files do not match
     * conditions of max size.
     */
    public static List<FileItem> getFileItems(HttpServletRequest request) throws FileUploadException {
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(MAX_MEMORY_SIZE);
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setSizeMax(MAX_REQUEST_SIZE);

        return upload.parseRequest(request);
    }


}
