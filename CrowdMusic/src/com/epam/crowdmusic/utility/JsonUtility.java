package com.epam.crowdmusic.utility;

import com.epam.crowdmusic.dto.Genre;
import com.epam.crowdmusic.dto.Project;
import com.epam.crowdmusic.dto.Reward;
import com.epam.crowdmusic.dto.User;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Utility that provides some functionality for working with JSON.
 */
public final class JsonUtility {

    /**
     * Gets list of integers from a json representation of array.
     *
     * @param jsonString json representation of array.
     * @return list of integers.
     */
    public static List<Integer> getIntList(String jsonString) {
        List<Integer> ids = new ArrayList<>();
        JsonArray data = new Gson().fromJson(jsonString, JsonArray.class);
        for (JsonElement element : data) {
            ids.add(element.getAsInt());
        }

        return ids;
    }


    /**
     * Builds reward object from json string.
     *
     * @param jsonString json representation of reward object.
     * @return
     */
    public static Reward getRewardFromJson(String jsonString) {
        JsonObject element = new Gson().fromJson(jsonString, JsonObject.class);
        Reward reward = new Reward();
        reward.setCost(element.get("cost").getAsFloat());
        reward.setName(element.get("name").getAsString());
        reward.setDescription(element.get("description").getAsString());
        return reward;
    }

    /**
     * Changes existing user fields by parsing them from json.
     *
     * @param user       existing user.
     * @param jsonString json representation of user fields.
     * @throws IllegalAccessException occurs while trying to access private
     *                                fields
     * @throws NoSuchFieldException   occurs if such field doesn't exist in
     *                                user object.
     */
    public static void changeUserObjectByJson(User user, String jsonString)
            throws IllegalAccessException, NoSuchFieldException {
        JsonObject obj = new Gson().fromJson(jsonString,
                JsonObject.class);

        for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {

            Field field = User.class.getDeclaredField(entry.getKey());

            field.setAccessible(true);
            field.set(user, entry.getValue().getAsString());
        }
    }

    /**
     * Gets project from json string.
     * @param jsonString json representation of project
     * @return new project object.
     */
    public static Project getProjectFromJson(String jsonString) {

        JsonObject data = new Gson().fromJson(jsonString, JsonObject
                .class);
        String name = data.get("name").getAsString();
        String description = data.get("description").getAsString();
        List<Genre> genreList = getGenresList(data.get("genres"));
        Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setGenres(genreList);
        project.setCreationDate(LocalDate.now());
        return project;
    }

    /**
     * Gets list of genres from a json array.
     * @param data json array containing genres info.
     * @return list of genre objects.
     */
    public static List<Genre> getGenresList(JsonElement data) {
        List<Genre> genreList = new ArrayList<>();
        JsonArray array = data.getAsJsonArray();
        for (JsonElement element : array) {
            JsonObject obj = element.getAsJsonObject();
            Genre genre = new Genre();
            genre.setName(obj.get("name").getAsString());
            genre.setId(obj.get("id").getAsInt());
            Genre parentGenre = new Genre();
            parentGenre.setId(obj.get("parentGenre").getAsJsonObject()
                    .get("id").getAsInt());
            genre.setParentGenre(parentGenre);
            genreList.add(genre);
        }
        return genreList;
    }
}
