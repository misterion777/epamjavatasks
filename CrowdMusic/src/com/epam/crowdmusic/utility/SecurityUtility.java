package com.epam.crowdmusic.utility;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

/**
 * Utility for handling security.
 */
public final class SecurityUtility  {

    /**
     * Generates MD5 hash value from string.
     * @param string input string
     * @return hash value of input string.
     */
    public static String md5(String string) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("md5");
            digest.reset();
            digest.update(string.getBytes());
            byte hash[] = digest.digest();
            Formatter formatter = new Formatter();
            for (byte hashByte : hash) {
                formatter.format("%02X", hashByte);
            }
            String md5summ = formatter.toString();
            formatter.close();
            return md5summ;
        } catch(NoSuchAlgorithmException e) {
            return null;
        }
    }
}
