package com.epam.crowdmusic.utility;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.TemporalAmount;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.List;

/**
 * Utility for parsing date info.
 */
public final class DateUtility {

    /**
     * Regex for splitting digits and time measures.
     */
    private static final String TIMECODE_REGEX = "\\d+|\\w";

    /**
     * Gets DateTime from timecode got from view.
     * @param timeFromNow timecode in format '\d+\w'
     * @return localdatetime object in future.
     */
    public static LocalDateTime getDateTimeFromNow(String timeFromNow){
        LocalDateTime now = LocalDateTime.now();
        parseTimeCode(timeFromNow);

        return now.plus(parseTimeCode(timeFromNow));
    }

    /**
     * Parses time code to create TemporalAmount
     * @param timeCode timeCode got from view.
     * @return Temporal amount to be added to current time.
     */
    private static TemporalAmount parseTimeCode(String timeCode){

        Matcher matcher = Pattern.compile(TIMECODE_REGEX).matcher(timeCode);

        List<String> chunks = new ArrayList<>();
        while(matcher.find()){
            chunks.add(matcher.group());
        }

        int amount = Integer.parseInt(chunks.get(0));
        switch (chunks.get(1)){
            case "h":
                return Duration.ofHours(amount);
            case "d":
                return Period.ofDays(amount);
            case "m":
                return Period.ofMonths(amount);
            case "y":
                return Period.ofYears(amount);
        }
        throw new IllegalArgumentException("Something strange happened");
    }
}
