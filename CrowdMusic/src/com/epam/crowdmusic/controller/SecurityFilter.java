package com.epam.crowdmusic.controller;

import com.epam.crowdmusic.action.Action;
import com.epam.crowdmusic.dto.Role;
import com.epam.crowdmusic.dto.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;


/**
 * Filter that checks whether user has permission for specified action.
 */
public class SecurityFilter implements Filter {
    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger(
            SecurityFilter.class);

    /**
     *{@inheritDoc}
    */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse
            servletResponse, FilterChain filterChain) throws IOException,
            ServletException {
        if (servletRequest instanceof HttpServletRequest && servletResponse
                instanceof
                HttpServletResponse) {
            HttpServletRequest httpRequest = (HttpServletRequest)
                    servletRequest;

            Action action = (Action) httpRequest.getAttribute
                    ("action");

            String uri = httpRequest.getRequestURI();
            String actionName = uri.substring(uri.lastIndexOf('/') + 1,
                    uri.length());

            Set<Role> allowedRoles = action.getAllowedRoles();
            if (allowedRoles == null) {
                servletRequest.getRequestDispatcher("/app/" + actionName)
                        .forward(servletRequest, servletResponse);
            } else {
                HttpSession session = httpRequest.getSession();
                User user = (User) session.getAttribute("user");
                if (user != null && allowedRoles.contains(user.getRole())) {
                    servletRequest.getRequestDispatcher("/app/" + actionName)
                            .forward(servletRequest, servletResponse);
                } else {
                    if (ControllerServlet.isAjax(httpRequest)){
                        String json;
                        if (user == null)
                            json = "{\"error\":\"You must first sign in!\"}";
                        else
                            json = "{\"error\":\""+ user.getRole()+" cannot " +
                                    "do this action!\"}";
                        servletResponse.setContentType("application/json");
                        servletResponse.setCharacterEncoding("UTF-8");
                        try {
                            servletResponse.getWriter().write(json);
                        } catch (IOException e) {
                            servletRequest.setAttribute("error", e.getMessage());
                            servletRequest.getServletContext().getRequestDispatcher
                                    ("/WEB-INF/jsp/error.jsp").forward(servletRequest, servletResponse);
                        }

                    }else {
                        servletRequest.setAttribute("error", "You are not " +
                                "permitted for this action!");
                        servletRequest.getRequestDispatcher("/WEB-INF/jsp/error" +
                                ".jsp").forward(servletRequest, servletResponse);
                    }

                }
            }

        } else {
            LOGGER.error("It is impossible to use HTTP filter");
            servletRequest.setAttribute("error", "It is impossible to use HTTP filter");
            servletRequest.getServletContext().getRequestDispatcher
                    ("/WEB-INF/jsp/error.jsp").forward(servletRequest, servletResponse);
        }


    }
}
