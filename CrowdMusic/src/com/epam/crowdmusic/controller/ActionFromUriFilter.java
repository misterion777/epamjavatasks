package com.epam.crowdmusic.controller;

import com.epam.crowdmusic.action.Action;
import com.epam.crowdmusic.action.factory.ActionFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Servlet filter that gets action name from url and set action object.
 */
public class ActionFromUriFilter implements Filter {

    /**
     * {@inheritDoc}
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;

            String uri = httpRequest.getRequestURI();
            String actionName = uri.substring(uri.lastIndexOf('/') + 1,
                    uri.length());

            Action command = ActionFactory.getAction(actionName);

            request.setAttribute("action", command);
            request.getRequestDispatcher("/security/" + actionName).forward
                    (request, response);
        }
    }

}
