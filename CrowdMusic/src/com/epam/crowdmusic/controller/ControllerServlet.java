package com.epam.crowdmusic.controller;

import com.epam.crowdmusic.action.Action;
import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.dao.pool.ConnectionPool;
import com.epam.crowdmusic.dao.pool.ConnectionPoolException;
import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Main controller servlet that gets all requests and handles them.
 */
public class ControllerServlet extends HttpServlet {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger(
            ControllerServlet.class);

    /**
     * Database driver class.
     */
    private static final String DB_DRIVER_CLASS = "com.mysql.jdbc.Driver";
    /**
     * Database url.
     */
    private static final String DB_URL =
            "jdbc:mysql://localhost:3306/CrowdMusicDB?useUnicode=true" +
                    "&characterEncoding=UTF-8";
    /**
     * Database user.
     */
    private static final String DB_USER = "root";

    /**
     * Database password.
     */
    private static final String DB_PASSWORD = "deathwing2105";

    /**
     * Connection pool start size.
     */
    private static final int DB_POOL_START_SIZE = 10;

    /**
     * Connection pool maximum size.
     */
    private static final int DB_POOL_MAX_SIZE = 10000;

    /**
     * Connection pool connection timeout.
     */
    private static final int DB_POOL_CHECK_CONNECTION_TIMEOUT = 0;


    /**
     * {@inheritDoc}
     */
    public void init() {
        try {
            ConnectionPool.getInstance().init(DB_DRIVER_CLASS, DB_URL,
                    DB_USER, DB_PASSWORD, DB_POOL_START_SIZE,
                    DB_POOL_MAX_SIZE, DB_POOL_CHECK_CONNECTION_TIMEOUT);
        } catch (ConnectionPoolException e) {
            LOGGER.error("It is impossible to initialize application", e);
            destroy();
        }
    }

    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        processRequest(req, resp);
    }

    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        processRequest(req, resp);
    }

    /**
     * Gets the request, process it and sends reponse.
     * @param request http request from client
     * @param response http response to client
     */
    private void processRequest(HttpServletRequest request,
            HttpServletResponse response) {

        Action command = (Action) request.getAttribute("action");
        Forward forward = command.execute(request);

        try {
            if (forward.isOutputStream()) {
                sendStream(forward, response);

            } else if (isAjax(request)) {
                sendJson(forward, response);

            } else {

                if (forward.isRedirect()) {
                    response.sendRedirect(forward.getForwardPage());
                } else {
                    getServletContext().getRequestDispatcher(forward
                            .getForwardPage()).forward(request, response);
                }
            }
        } catch (ServletException | IOException e) {
            LOGGER.error("ControllerServletError", e);

        }
    }

    /**
     * Checks if request is ajax request.
     * @param request http request from client.
     * @return boolean value that indicates whether request is ajax.
     */
    static boolean isAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader
                ("X-Requested-With"));
    }

    /**
     * Sends output stream to client.
     * @param forward forward data containing file path.
     * @param response http response.
     * @throws IOException thrown in case error during file copying.
     */
    private void sendStream(Forward forward, HttpServletResponse response)
            throws IOException {
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "filename=\"" +
                forward.getAttribute("filename") + "\"");
        File srcFile = new File((String) forward.getAttribute("path"));
        FileUtils.copyFile(srcFile, response.getOutputStream());

    }

    /**
     * Sends JSON data to client.
     * @param forward forward object with data.
     * @param response http response.
     * @throws IOException thrown in case error during writing.
     */
    private void sendJson(Forward forward, HttpServletResponse response)
            throws IOException {
        String json = new Gson().toJson(forward.getAttributes());
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }

}
