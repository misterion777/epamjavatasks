package com.epam.crowdmusic.dao.pool;

public class ConnectionPoolException extends Exception {


    public ConnectionPoolException() {
    }

    public ConnectionPoolException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectionPoolException(String message) {
        super(message);
    }

    public ConnectionPoolException(Throwable cause) {
        super(cause);
    }
}
