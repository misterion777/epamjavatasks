package com.epam.crowdmusic.dao.pool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Pool for managing database connections
 */
final public class ConnectionPool {
    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Database url.
     */
    private String url;
    /**
     * Database user.
     */
    private String user;
    /**
     * Database password.
     */
    private String password;
    /**
     * Maximum connections amount
     */
    private int maxSize;
    /**
     * Connection timeout.
     */
    private int checkConnectionTimeout;

    /**
     * Lock object for thread synchronization.
     */
    private ReentrantLock lock = new ReentrantLock();

    /**
     * Thread-safe queue of connections that are free.
     */
    private BlockingQueue<PooledConnection> freeConnections = new
            LinkedBlockingQueue<>();

    /**
     * Thread-safe set of connections that are used.
     */
    private Set<PooledConnection> usedConnections = new
            ConcurrentSkipListSet<>();

    /**
     * Connection pool singleton instance.
     */
    private static ConnectionPool instance = new ConnectionPool();


    /**
     * Returns instance of connection pool.
     * @return single connection pool instance.
     */
    public static ConnectionPool getInstance() {
        return instance;
    }

    /**
     * Private default constructor.
     */
    private ConnectionPool() {
    }

    /**
     * Gets connection from pool.
     * @return free connection
     * @throws ConnectionPoolException thrown if there is some database
     * exception
     */
    public Connection getConnection() throws ConnectionPoolException {
        lock.lock();
        PooledConnection connection = null;
        try {
            while (connection == null) {
                if (!freeConnections.isEmpty()) {
                    connection = freeConnections.take();
                    if (!connection.isValid(checkConnectionTimeout)) {
                        connection.getConnection().close();
                        connection = null;
                    }
                } else if (usedConnections.size() < maxSize) {
                    connection = createConnection();
                } else {
                    LOGGER.error("The limit of number of database connections"
                            + " is exceeded");
                    throw new SQLException();
                }
            }
            usedConnections.add(connection);
            LOGGER.debug(
                    String.format("Connection was received from pool. " +
                                    "Current pool size: %d used connections;" +
                                    " %d free connection", usedConnections.size(),
                            freeConnections.size()));
        } catch (InterruptedException | SQLException e) {
            LOGGER.error("It is impossible to connect to a database", e);
            throw new ConnectionPoolException(e);
        } finally {
            lock.unlock();
        }
        return connection;
    }

    /**
     * Puts connection to free connection and remove from userd connections.
     * @param connection pooled connection that is being freed.
     * @throws SQLException thrown if there is sql error
     */
    void freeConnection(PooledConnection connection) throws
            SQLException {
        lock.lock();
        try {
            if (connection.isValid(checkConnectionTimeout)) {
                connection.clearWarnings();
                connection.setAutoCommit(true);
                usedConnections.remove(connection);
                freeConnections.put(connection);
                LOGGER.debug(
                        String.format("Connection was returned into pool. " +
                                        "Current pool size: %d used " +
                                        "connections;" +
                                        " %d free connection",
                                usedConnections.size(),
                                freeConnections.size()));
            }
        } catch (SQLException | InterruptedException e) {
            LOGGER.warn("It is impossible to return database " +
                    "connection into pool", e);
            connection.getConnection().close();
        }finally {
            lock.unlock();
        }
    }

    /**
     * Inits connection pool.
     * @param driverClass database driver class
     * @param url database url
     * @param user database user
     * @param password database password
     * @param startSize starting size of free connections
     * @param maxSize maximum connections amount
     * @param checkConnectionTimeout connections timeout
     * @throws ConnectionPoolException thrown if its impossible to init pool.
     */
    public void init(String driverClass, String url, String
            user, String password, int startSize, int maxSize, int
            checkConnectionTimeout) throws ConnectionPoolException {
        lock.lock();
        try {
            destroy();
            Class.forName(driverClass);
            this.url = url;
            this.user = user;
            this.password = password;
            this.maxSize = maxSize;
            this.checkConnectionTimeout = checkConnectionTimeout;
            for (int counter = 0; counter < startSize; counter++) {
                freeConnections.put(createConnection());
            }
        } catch (ClassNotFoundException | SQLException | InterruptedException
                e) {
            LOGGER.fatal("It is impossible to initialize connection pool", e);
            throw new ConnectionPoolException(e);
        }
        finally {
            lock.unlock();
        }
    }


    /**
     * Creates new pooled connection
     * @return new pooled connection with all parameters.
     * @throws SQLException
     */
    private PooledConnection createConnection() throws SQLException {
        return new PooledConnection(DriverManager.getConnection(url, user,
                password));
    }

    /**
     * Called on finalize.
     * @throws SQLException thrown if its impossible to close the connections.
     */
    private void destroy() throws SQLException {
        lock.lock();
        try{
            usedConnections.addAll(freeConnections);
            freeConnections.clear();
            for (PooledConnection connection : usedConnections) {
                connection.getConnection().close();
            }
            usedConnections.clear();
        }
        finally {
            lock.unlock();
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void finalize() throws Throwable {
        destroy();
    }
}
