package com.epam.crowdmusic.dao;
import com.epam.crowdmusic.dto.BannedUser;

/**
 * Interface for access Banned User entity data.
 */
public interface BannedUserDao extends Dao<BannedUser> {

    /**
     * Finds banned user entity by user id that is stored in this entity
     * @param userId user id that is stored in banned user
     * @return banned user entity
     * @throws DaoException thrown if error occurs while accessing database
     */
    BannedUser findByUser (int userId) throws DaoException;

}
