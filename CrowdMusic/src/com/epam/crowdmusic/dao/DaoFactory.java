package com.epam.crowdmusic.dao;


/**
 * Provides interface for creating dao objects.
 */
public interface DaoFactory {

    /**
     * Creates Genre Dao object.
     * @return Genre dao object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    GenreDao getGenreDao() throws DaoException;

    /**
     * Creates User Dao object.
     * @return User dao object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    UserDao getUserDao() throws DaoException;

    /**
     * Creates Project Dao object.
     * @return Project dao object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    ProjectDao getProjectDao() throws DaoException;

    /**
     * Creates Project Dao object.
     * @return Project dao object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    RewardDao getRewardDao() throws DaoException;

    /**
     * Creates Track Dao object.
     * @return Track dao object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    TrackDao getTrackDao() throws DaoException;

    /**
     * Creates BannedUser Dao object.
     * @return BannedUser dao object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    BannedUserDao getBannedUserDao() throws DaoException;

    /**
     * Creates ManyToMany Dao object.
     * @return ManyToMany dao object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    ManyToManyDao getManyToManyDao(String type) throws
            DaoException;
}
