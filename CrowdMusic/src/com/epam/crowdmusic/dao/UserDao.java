package com.epam.crowdmusic.dao;

import com.epam.crowdmusic.dto.User;

import java.util.List;

/**
 * Interface for access User entity data.
 */
public interface UserDao extends Dao<User> {

    /**
     * Adds anotherUserId to linked_user_id column to row with currentUserId id.
     * @param currentUserId id of user to which will be added anotherUserId.
     * @param anotherUserId id that will be added to current user.
     * @return true, if successful.
     * @throws DaoException thrown if error occurs while accessing database.
     */
    boolean linkUser(int currentUserId, int anotherUserId) throws DaoException;

    /**
     * Finds user object by its name.
     * @param name user name.
     * @return user object matching condtition.
     * @throws DaoException thrown if error occurs while accessing database.
     */
    User findByName(String name) throws DaoException;

    /**
     * Finds user by its name and password.
     * @param name user name.
     * @param password hashed user password.
     * @return user object matching condtion.
     * @throws DaoException thrown if error occurs while accessing database.
     */
    User findByNameAndPassword(String name, String password) throws
            DaoException;

    /**
     * Finds list of users that bought specific reward.
     * @param rewardId reward that were bought by users.
     * @return list of users matching condition.
     * @throws DaoException thrown if error occurs while accessing database.
     */
    List<User> findByReward(int rewardId) throws DaoException;

    /**
     * Finds list of users that are authors on specific project.
     * @param projectId project owned by users.
     * @return list of users matching condition.
     * @throws DaoException thrown if error occurs while accessing database.
     */
    List<User> findByProject(int projectId) throws DaoException;

}
