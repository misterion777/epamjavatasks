package com.epam.crowdmusic.dao;

import com.epam.crowdmusic.dto.Reward;

import java.util.List;

/**
 * Interface for access Reward entity data.
 */
public interface RewardDao extends Dao<Reward> {

    /**
     * Finds list of rewards by user that has bought all these rewards.
     * @param userId user id that has all returned rewards
     * @return list of rewards matching condtions.
     * @throws DaoException thrown if error occurs while accessing database.
     */
    List<Reward> findByUser (int userId) throws DaoException;

    /**
     * Finds list of rewards by project that has created all these rewards.
     * @param projectId project id that has created all returned rewards.
     * @return list of rewards matching condtitions.
     * @throws DaoException thrown if error occurs while accessing database.
     */
    List<Reward> findByProject (int projectId) throws DaoException;
}
