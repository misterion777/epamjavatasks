package com.epam.crowdmusic.dao;

import com.epam.crowdmusic.dto.Project;

import java.util.List;

/**
 * Interface for access Project entity data.
 */
public interface ProjectDao extends Dao<Project> {

    /**
     * Get project pages amount for pagination pages by genre id.
     * @param genreId id of genre whick is being searched
     * @return total page amount
     * @throws DaoException thrown if error occurs while accessing database.
     */
    int getPageAmount(int genreId) throws DaoException;

    /**
     * Get total project pages amount for pagination pages.
     * @return total page amount.
     * @throws DaoException thrown if error occurs while accessing database.
     */
    int getPageAmount() throws DaoException;

    /**
     * Finds list of project on specific page.
     * @param pageNumber page number.
     * @return list of project
     * @throws DaoException thrown if error occurs while accessing database.
     */
    List<Project> findAll(int pageNumber) throws DaoException;

    /**
     * Finds list of projects by genre id
     * @param genreId genre which has all these projects.
     * @param pageNumber page number where to find project.
     * @return list of projects matching conditions.
     * @throws DaoException thrown if error occurs while accessing database.
     */
    List<Project> findByGenre(int genreId, int pageNumber) throws DaoException;

    /**
     * Finds list of projects by user id.
     * @param userId user which has all these projects.
     * @return list of projects matching conditions.
     * @throws DaoException thrown if error occurs while accessing database.
     */
    List<Project> findByUser(int userId) throws DaoException;

    /**
     * Finds project by its name.
     * @param name project name.
     * @return project matching conditions
     * @throws DaoException thrown if error occurs while accessing database.
     */
    Project findByName(String name) throws DaoException;


}
