package com.epam.crowdmusic.dao;


/**
 *
 * Provides basic interface for many to many database connections.
 */
public interface ManyToManyDao {

    /**
     * Creates row with two entities.
     * @param entityId1 first entity id
     * @param entityId2 second entity id
     * @return true, if successful
     * @throws DaoException thrown if error occurs while accessing database
     */
    boolean create(int entityId1, int entityId2) throws DaoException;

    /**
     * Deletes row by first entity
     * @param entityId first entity id
     * @return true, if successful
     * @throws DaoException thrown if error occurs while accessing database
     */
    boolean delete(int entityId) throws DaoException;

    /**
     * Delets row by both entities
     * @param entityId1 first entity id
     * @param entityId2 second entity id
     * @return true, if successful
     * @throws DaoException thrown if error occurs while accessing database
     */
    boolean delete(int entityId1, int entityId2) throws DaoException;

}
