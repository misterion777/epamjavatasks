package com.epam.crowdmusic.dao;

import com.epam.crowdmusic.dto.Track;

import java.util.List;

/**
 * Interface for access Track entity data.
 */
public interface TrackDao extends Dao<Track> {

    /**
     * Finds list of track by project that has all these tracks.
     *
     * @param projectId project id that has all returned tracks.
     * @return list of tracks matching conditions.
     * @throws DaoException thrown if error occurs while accessing database.
     */
    List<Track> findByProject(int projectId) throws DaoException;
}
