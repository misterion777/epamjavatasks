package com.epam.crowdmusic.dao.mysql;

import com.epam.crowdmusic.dao.BannedUserDao;
import com.epam.crowdmusic.dao.DaoFactory;
import com.epam.crowdmusic.dao.GenreDao;
import com.epam.crowdmusic.dao.ManyToManyDao;
import com.epam.crowdmusic.dao.ProjectDao;
import com.epam.crowdmusic.dao.RewardDao;
import com.epam.crowdmusic.dao.TrackDao;
import com.epam.crowdmusic.dao.UserDao;
import com.epam.crowdmusic.dao.pool.ConnectionPool;
import com.epam.crowdmusic.dao.DaoException;
import com.epam.crowdmusic.dao.pool.ConnectionPoolException;

/**
 * Mysql dao factory implementation.
 */
public class MySqlDaoFactory  implements DaoFactory {

    /**
     * {@inheritDoc}
     */
    @Override
    public GenreDao getGenreDao() throws DaoException {
        MySqlAbstractDao dao = new MySqlGenreDao();
        setDaoConnection(dao);
        return (GenreDao) dao;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserDao getUserDao() throws DaoException {
        MySqlAbstractDao dao = new MySqlUserDao();
        setDaoConnection(dao);
        return (UserDao) dao;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjectDao getProjectDao() throws DaoException {
        MySqlAbstractDao dao = new MySqlProjectDao();
        setDaoConnection(dao);
        return (ProjectDao) dao;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RewardDao getRewardDao() throws DaoException {
        MySqlAbstractDao dao = new MySqlRewardDao();
        setDaoConnection(dao);
        return (RewardDao) dao;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TrackDao getTrackDao() throws DaoException {
        MySqlAbstractDao dao = new MySqlTrackDao();
        setDaoConnection(dao);
        return (TrackDao) dao;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BannedUserDao getBannedUserDao() throws DaoException {
        MySqlAbstractDao dao = new MySqlBannedUserDao();
        setDaoConnection(dao);
        return (BannedUserDao) dao;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ManyToManyDao getManyToManyDao(String type) throws
            DaoException {

        MySqlAbstractManyToManyDao dao;
        switch (type){
            case "userHasProject":
                dao = new MySqlUserHasProjectDao();
                break;
            case "userHasReward":
                dao = new MySqlUserHasRewardDao();
                break;
            case "projectHasGenre":
                dao = new MySqlProjectHasGenreDao();
                break;
            default:
                throw new DaoException("No such many to many dao!");
        }

        setDaoConnection(dao);
        return dao;
    }

    /**
     * Sets dao connection to connection from connection pool.
     * @param dao newly created dao object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    private void setDaoConnection (MySqlAbstractDao dao) throws DaoException {
        try {
            dao.setConnection(ConnectionPool.getInstance().getConnection());
        } catch (ConnectionPoolException e) {
            throw new DaoException(e);
        }
    }

}
