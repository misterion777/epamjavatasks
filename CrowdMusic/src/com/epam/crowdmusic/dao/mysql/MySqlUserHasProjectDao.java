package com.epam.crowdmusic.dao.mysql;

/**
 * Mysql implementation for user has project dao.
 */
public class MySqlUserHasProjectDao extends MySqlAbstractManyToManyDao {

    /**
     * Query for insert.
     */
    private static final String SQL_INSERT =
            "INSERT INTO `user_has_project` (`user_id`, `project_id`) " +
                    "VALUES (?,?)";

    /**
     * Query for delete by project.
     */
    private static final String SQL_DELETE_BY_PROJECT =
            "DELETE FROM `user_has_project` WHERE `project_id`=?";

    /**
     * Query for delete.
     */
    private static final String SQL_DELETE =
            "DELETE FROM `user_has_project` WHERE `user_id`=? AND " +
                    "`project_id`=?";

    /**
     * Default constructor setting queries in base class.
     */
    MySqlUserHasProjectDao() {
        super(SQL_INSERT, SQL_DELETE_BY_PROJECT, SQL_DELETE);
    }

}
