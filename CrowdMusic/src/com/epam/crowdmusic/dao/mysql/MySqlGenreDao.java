package com.epam.crowdmusic.dao.mysql;

import com.epam.crowdmusic.dao.GenreDao;
import com.epam.crowdmusic.dto.Genre;
import com.epam.crowdmusic.dao.DaoException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Mysql implementation for genre dao.
 */
public class MySqlGenreDao extends MySqlAbstractDao implements GenreDao {

    /**
     * Mysql select query.
     */
    private static final String SQL_SELECT_ALL =
            "SELECT * FROM `genre`";


    /**
     * Mysql select genres by project query.
     */
    private static final String SQL_FIND_GENRES_BY_PROJECT =
            "SELECT `genre`.* " +
                    "FROM `genre` " +
                    "JOIN `project_has_genre` ON `genre_id`=`id` " +
                    "WHERE `project_id`=?";



    @Override
    public List<Genre> findAll() throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_SELECT_ALL)) {

            ResultSet resultSet = statement.executeQuery();

            List<Genre> genres = new ArrayList<>();
            while (resultSet.next()) {
                genres.add(buildGenre(resultSet));
            }
            return genres;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<Genre> findByProject(int projectId) throws DaoException {

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_FIND_GENRES_BY_PROJECT)) {
            statement.setInt(1, projectId);
            ResultSet resultSet = statement.executeQuery();

            List<Genre> genres = new ArrayList<>();
            while (resultSet.next()) {
                genres.add(buildGenre(resultSet));
            }
            return genres;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Builds genre object from result set.
     * @param rs result of a query.
     * @return new genre object.
     * @throws SQLException thrown if error occurs while reading result set.
     */
    private Genre buildGenre(ResultSet rs) throws SQLException {
        Genre genre = new Genre();
        genre.setId(rs.getInt("id"));
        genre.setName(rs.getString("name"));

        Genre parentGenre = new Genre();
        parentGenre.setId(rs.getInt("parent_genre_id"));
        genre.setParentGenre(parentGenre);

        return genre;
    }



    @Override
    public Genre findById(int id) throws DaoException {
        throw new NotImplementedException();
    }

    @Override
    public boolean delete(int id) throws DaoException {
        throw new NotImplementedException();
    }


    @Override
    public boolean create(Genre entity) throws DaoException {
        throw new NotImplementedException();
    }

    @Override
    public boolean update(Genre entity) throws DaoException {
        throw new NotImplementedException();
    }
}
