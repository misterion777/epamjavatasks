package com.epam.crowdmusic.dao.mysql;

import com.epam.crowdmusic.dao.BannedUserDao;
import com.epam.crowdmusic.dto.BannedUser;
import com.epam.crowdmusic.dto.User;
import com.epam.crowdmusic.dao.DaoException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Mysql dao implementation for banned user dto.
 */
public class MySqlBannedUserDao extends MySqlAbstractDao implements
        BannedUserDao {

    /**
     * Mysql query for insert.
     */
    private static final String SQL_INSERT =
            "INSERT INTO `banned_user` (`user_id`, `expiration`,`reason`)" +
                    "VALUES (?,?,?)";

    /**
     * Mysql query for selection by user.
     */
    private static final String SQL_SELECT_BY_USER =
            "SELECT * FROM `banned_user` WHERE `user_id` = ?";

    /**
     * Mysql query for delete.
     */
    private static final String SQL_DELETE =
            "DELETE FROM `banned_user` WHERE `id`=?";


    /**
     * {@inheritDoc}
     */
    @Override
    public BannedUser findByUser(int userId) throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_SELECT_BY_USER)) {
            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return buildBannedUser(resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Builds banned user object from result set.
     * @param rs result of a query.
     * @return new banned user object.
     * @throws SQLException thrown if error occurs while reading result set.
     */
    private BannedUser buildBannedUser(ResultSet rs) throws SQLException {
        BannedUser bannedUser = new BannedUser();
        bannedUser.setId(rs.getInt("id"));
        bannedUser.setBanReason(rs.getString("reason"));
        bannedUser.setExpirationDateTime(rs.getObject("expiration",
                LocalDateTime.class));
        User user = new User();
        user.setId(rs.getInt("user_id"));
        bannedUser.setUser(user);
        return bannedUser;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<BannedUser> findAll() throws DaoException {
        throw new NotImplementedException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BannedUser findById(int id) throws DaoException {
        throw new NotImplementedException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean create(BannedUser entity) throws DaoException {
        ResultSet resultSet;
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, entity.getUser().getId());
            statement.setObject(2, entity.getExpirationDateTime());
            statement.setString(3, entity.getBanReason());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                entity.setId(resultSet.getInt(1));
                return true;
            }
            return false;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean update(BannedUser entity) throws DaoException {
        throw new NotImplementedException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean delete(int id) throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_DELETE)) {

            statement.setInt(1, id);
            int update = statement.executeUpdate();

            return update > 0;

        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }
}
