package com.epam.crowdmusic.dao.mysql;

/**
 * Mysql implementation for user has reward dao.
 */
public class MySqlUserHasRewardDao extends MySqlAbstractManyToManyDao{

    /**
     * Query for insert.
     */
    private static final String SQL_INSERT =
            "INSERT INTO `user_has_reward` (`user_id`, `reward_id`) " +
                    "VALUES (?, ?)";

    /**
     * Query for delete by reward.
     */
    private static final String SQL_DELETE_BY_REWARD =
            "DELETE FROM `user_has_reward` WHERE `reward_id`=?";

    /**
     * Query for delete.
     */
    private static final String SQL_DELETE =
            "DELETE FROM `user_has_reward` WHERE `user_id` = ? AND " +
                    "`reward_id`=?";

    /**
     * Default constructor setting queries in base class.
     */
    MySqlUserHasRewardDao() {
        super(SQL_INSERT, SQL_DELETE_BY_REWARD, SQL_DELETE);
    }

}
