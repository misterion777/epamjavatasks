package com.epam.crowdmusic.dao.mysql;

import com.epam.crowdmusic.dao.TrackDao;
import com.epam.crowdmusic.dto.Track;
import com.epam.crowdmusic.dao.DaoException;
import com.mysql.jdbc.exceptions
        .jdbc4.MySQLIntegrityConstraintViolationException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Mysql implementation for track dao.
 */
public class MySqlTrackDao extends MySqlAbstractDao implements TrackDao {

    /**
     * Query for insert.
     */
    private static final String SQL_INSERT =
            "INSERT INTO `track` (`project_id`, `name`,`path`) " +
                    "VALUES (?, ?, ?)";

    /**
     * Query for selection by project id.
     */
    private static final String SQL_SELECT_BY_PROJECT_ID =
            "SELECT * FROM `track` WHERE `project_id` = ?";

    /**
     * Query for selection by track id.
     */
    private static final String SQL_SELECT_BY_TRACK_ID =
            "SELECT * FROM `track` WHERE `id` = ?";


    /**
     * Query for delete.
     */
    private static final String SQL_DELETE =
            "DELETE FROM `track` WHERE `id`=?";

    @Override
    public boolean create(Track entity) throws DaoException {
        ResultSet resultSet;
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, entity.getProject().getId());
            statement.setString(2, entity.getName());
            statement.setString(3, entity.getPath());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                entity.setId(resultSet.getInt(1));
                return true;
            }
            return false;
        } catch (MySQLIntegrityConstraintViolationException e) {
            throw new DaoException("Username '"
                    + entity.getName() + "' " + "already exists!");
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<Track> findByProject(int projectId) throws DaoException {

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_SELECT_BY_PROJECT_ID)) {
            statement.setInt(1, projectId);
            ResultSet resultSet = statement.executeQuery();

            List<Track> tracks = new ArrayList<>();
            while (resultSet.next()) {
                tracks.add(buildTrack(resultSet));
            }
            return tracks;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Builds track object from result set.
     * @param rs result of a query.
     * @return new track object.
     * @throws SQLException thrown if error occurs while reading result set.
     */
    private Track buildTrack(ResultSet rs) throws SQLException {
        Track track = new Track();
        track.setPath(rs.getString("path"));
        track.setName(rs.getString("name"));
        track.setId(rs.getInt("id"));
        return track;
    }

    @Override
    public List<Track> findAll() throws DaoException {
        return null;
    }

    @Override
    public Track findById(int id) throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_SELECT_BY_TRACK_ID)) {
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return buildTrack(resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public boolean delete(int id) throws DaoException {

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_DELETE)) {

            statement.setInt(1, id);
            int update = statement.executeUpdate();

            return update > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }



    @Override
    public boolean update(Track entity) throws DaoException {
        throw new NotImplementedException();
    }
}
