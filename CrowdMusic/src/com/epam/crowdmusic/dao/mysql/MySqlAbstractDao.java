package com.epam.crowdmusic.dao.mysql;

import java.sql.Connection;

/**
 * Abstract mysql dao implementation.
 */
public abstract class MySqlAbstractDao {

    /**
     * Mysql Connection object.
     */
    private Connection connection;

    /**
     * Gets connection object.
     * @return mysql connection object.
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Sets connection object.
     * @param connection mysql connection object.
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }
    
}
