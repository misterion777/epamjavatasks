package com.epam.crowdmusic.dao.mysql;

import com.epam.crowdmusic.dao.UserDao;
import com.epam.crowdmusic.dto.Role;
import com.epam.crowdmusic.dto.User;
import com.epam.crowdmusic.dao.DaoException;
import com.mysql.jdbc.exceptions
        .jdbc4.MySQLIntegrityConstraintViolationException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Mysql implementation for user dao.
 */
public class MySqlUserDao extends MySqlAbstractDao implements UserDao {

    /**
     * Query for selection by name and password.
     */
    private static final String SQL_FIND_USER_BY_NAME_AND_PASSWORD =
            "SELECT * FROM " +
                    "`user` WHERE " +
                    "`username` = ? AND " +
                    "`password` = ?";

    /**
     * Query for selecting by name.
     */
    private static final String SQL_FIND_USER_BY_NAME =
            "SELECT * FROM " +
                    "`user` WHERE " +
                    "`username` = ?";


    /**
     * Query for selecting by id.
     */
    private static final String SQL_FIND_USER_BY_ID =
            "SELECT * FROM `user` WHERE `id`= ?";


    /**
     * Query for selecting by project id.
     */
    private static final String SQL_FIND_USERS_IN_PROJECT =
            "SELECT `user`.* " +
                    "FROM `user` " +
                    "JOIN `user_has_project` ON `user_id`=`id` " +
                    "WHERE `project_id`=?";


    /**
     * Query for selecting by reward id.
     */
    private static final String SQL_FIND_USERS_IN_REWARD =
            "SELECT `user`.* " +
                    "FROM `user` " +
                    "JOIN `user_has_reward` ON `user_id`=`id` " +
                    "WHERE `reward_id`=?";


    /**
     * Query for insert.
     */
    private static final String SQL_INSERT =
            "INSERT INTO `user` (`username`, `email`,`password`, `role`, " +
                    "`bio`, `location`) " +
                    "VALUES (?, ?, ?, ?, ?, ?)";


    /**
     * Query for update.
     */
    private static final String SQL_UPDATE =
            "UPDATE `user` SET `email`=?, `username`=?, `bio`=?, " +
                    "`location`=?, `social`=?, " +
                    "`picture_path`=? " +
                    "WHERE `id`=?";


    /**
     * Query for updating linked user.
     */
    private static final String SQL_UPDATE_LINK =
            "UPDATE `user` SET `linked_user_id`=? " +
                    "WHERE `id`=?";

    /**
     * Query for selecting all.
     */
    private static final String SQL_SELECT_ALL =
            "SELECT * FROM `user` ";



    @Override
    public boolean create(User entity) throws DaoException {

        ResultSet resultSet;

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, entity.getName());
            statement.setString(2, entity.getEmail());
            statement.setString(3, entity.getPassword());
            statement.setString(4, entity.getRole().toString());
            statement.setString(5, entity.getBio());
            statement.setString(6, entity.getLocation());

            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                entity.setId(resultSet.getInt(1));
                return true;
            };
            return false;
        } catch (MySQLIntegrityConstraintViolationException e) {
            throw new DaoException("Username '"
                    + entity.getName() + "' " + "already exists!");
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }


    @Override
    public List<User> findAll() throws DaoException {

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_SELECT_ALL)) {

            ResultSet resultSet = statement.executeQuery();
            List<User> users = new ArrayList<>();
            while (resultSet.next()) {
                users.add(buildUser(resultSet));
            }
            return users;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public User findById(int id) throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_FIND_USER_BY_ID)) {
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return buildUser(resultSet);
            }
            throw new DaoException("Cannot find user!");
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public boolean delete(int id) throws DaoException {
        return false;
    }


    @Override
    public boolean update(User entity) throws DaoException {

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_UPDATE)) {
            statement.setString(1, entity.getEmail());
            statement.setString(2, entity.getName());
            statement.setString(3, entity.getBio());
            statement.setString(4, entity.getLocation());
            statement.setString(5, entity.getSocial());
            statement.setString(6, entity.getPicturePath());
            statement.setInt(7, entity.getId());

            int update = statement.executeUpdate();

            return update > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        }

    }

    @Override
    public boolean linkUser(int currentUserId, int anotherUserId) throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_UPDATE_LINK)) {

            statement.setInt(1, anotherUserId);
            statement.setInt(2, currentUserId);

            int update = statement.executeUpdate();
            return update > 0;
            //logger.error("There is no autoincremented index after trying to
            // add record into table `users`");
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public User findByName(String username) throws DaoException {

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_FIND_USER_BY_NAME)) {
            statement.setString(1, username);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return buildUser(resultSet);
            }
            throw new DaoException("User " + username + " doesn't exist!");
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }


    @Override
    public User findByNameAndPassword(String email, String password) throws
            DaoException {

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_FIND_USER_BY_NAME_AND_PASSWORD)) {
            statement.setString(1, email);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return buildUser(resultSet);
            }
            throw new DaoException("Invalid email or password!");
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }


    @Override
    public List<User> findByReward(int rewardId) throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_FIND_USERS_IN_REWARD)) {
            statement.setInt(1, rewardId);
            ResultSet resultSet = statement.executeQuery();
            List<User> users = new ArrayList<>();
            while (resultSet.next()) {
                users.add(buildUser(resultSet));
            }
            return users;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<User> findByProject(int projectId)
            throws DaoException {

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_FIND_USERS_IN_PROJECT)) {
            statement.setInt(1, projectId);
            ResultSet resultSet = statement.executeQuery();

            List<User> users = new ArrayList<>();
            while (resultSet.next()) {
                users.add(buildUser(resultSet));
            }
            return users;
        } catch (SQLException e) {
            throw new DaoException(e);
        }

    }

    /**
     * Builds user object from result set.
     * @param rs result of a query.
     * @return new user object.
     * @throws SQLException thrown if error occurs while reading result set.
     */
    private User buildUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setName(rs.getString("username"));
        user.setId(rs.getInt("id"));
        user.setEmail(rs.getString("email"));
        user.setPassword(rs.getString("password"));
        user.setRole(Role.valueOf(rs.getString("role")));
        user.setCreditCardNumber(rs.getString("credit_card_number"));
        user.setLocation(rs.getString("location"));
        user.setBio(rs.getString("bio"));
        user.setPicturePath(rs.getString("picture_path"));
        user.setSocial(rs.getString("social"));

        int linked_user_id = rs.getInt("linked_user_id");
        User linkedUser = null;
        if (linked_user_id != 0){
            linkedUser = new User();
            linkedUser.setId(linked_user_id);
        }
        user.setLinkedUser(linkedUser);
        return user;
    }

}
