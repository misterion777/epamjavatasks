package com.epam.crowdmusic.dao.mysql;

import com.epam.crowdmusic.action.reward.BuyRewardAction;
import com.epam.crowdmusic.dao.ManyToManyDao;
import com.epam.crowdmusic.dao.DaoException;
import com.epam.crowdmusic.dao.pool.ConnectionPoolException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Basic implementation of many to many dao.
 */
public abstract class MySqlAbstractManyToManyDao extends MySqlAbstractDao
        implements
        ManyToManyDao {


    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger(MySqlAbstractManyToManyDao.class);

    /**
     * Mysql query for insert.
     */
    private String sqlInsert;

    /**
     * Mysql query for delete by one entity.
     */
    private String sqlDeleteByEntity;

    /**
     * Mysql query for delete.
     */
    private String sqlDelete;


    /**
     * Constructor that sets all queries needed for interface implementation.
     * @param sqlInsert query for insert.
     * @param sqlDeleteByEntity query for delete by one entity.
     * @param sqlDelete query for delete.
     */
    MySqlAbstractManyToManyDao(String sqlInsert, String sqlDeleteByEntity,
            String sqlDelete) {
        this.sqlInsert = sqlInsert;
        this.sqlDeleteByEntity = sqlDeleteByEntity;
        this.sqlDelete = sqlDelete;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean create(int entityId1, int entityId2) throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (sqlInsert)) {
            statement.setInt(1, entityId1);
            statement.setInt(2, entityId2);
            int update = statement.executeUpdate();
            return update > 0;
        } catch (com.mysql.jdbc.exceptions
                .jdbc4.MySQLIntegrityConstraintViolationException e) {
            throw new DaoException("You've already got this!");
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean delete(int entityId) throws DaoException {

        try (PreparedStatement statement = getConnection().prepareStatement
                (sqlDeleteByEntity)) {
            statement.setInt(1, entityId);
            int update = statement.executeUpdate();
            return update > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean delete(int entityId1, int entityId2) throws
            DaoException {

        try (PreparedStatement statement = getConnection().prepareStatement
                (sqlDelete)) {
            statement.setInt(1, entityId1);
            statement.setInt(2, entityId2);
            int update = statement.executeUpdate();
            return update > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }


}
