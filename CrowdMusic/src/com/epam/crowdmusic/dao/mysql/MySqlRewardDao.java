package com.epam.crowdmusic.dao.mysql;

import com.epam.crowdmusic.dao.RewardDao;
import com.epam.crowdmusic.dto.Project;
import com.epam.crowdmusic.dto.Reward;

import com.epam.crowdmusic.dao.DaoException;
import com.mysql.jdbc.exceptions
        .jdbc4.MySQLIntegrityConstraintViolationException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Mysql implementation for Reward Dao.
 */
public class MySqlRewardDao extends MySqlAbstractDao implements RewardDao {

    /**
     * Query for insert.
     */
    private static final String SQL_INSERT =
            "INSERT INTO `reward` " +
                    "(`name`, `description`,`project_id`,`cost`) " +
                    "VALUES (?, ?, ?, ?)";

    /**
     * Query for selection by project id.
     */
    private static final String SQL_SELECT_BY_PROJECT_ID =
            "SELECT * FROM `reward` WHERE `project_id` = ?";


    /**
     * Query for selection by user id.
     */
    private static final String SQL_SELECT_BY_USER_ID =
            "SELECT `reward`.* FROM `reward`" +
                    "JOIN `user_has_reward` u ON reward.id = u.reward_id " +
                    "WHERE u.`user_id` = ?";

    /**
     * Query for update.
     */
    private static final String SQL_UPDATE =
            "UPDATE `reward` SET `name`=?, `description`=?, `cost`=?" +
                    "WHERE `id`=?";


    @Override
    public boolean create(Reward entity) throws DaoException {
        ResultSet resultSet;

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, entity.getName());
            statement.setString(2, entity.getDescription());
            statement.setObject(3, entity.getProject().getId());
            statement.setFloat(4, entity.getCost());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();

            if (resultSet.next()) {
                entity.setId(resultSet.getInt(1));
                return true;
            }
            return false;
        } catch (MySQLIntegrityConstraintViolationException e) {
            throw new DaoException("Reward " +
                    "with name '"
                    + entity.getName() + "' " + "already exists!");
        } catch (SQLException e) {
            throw new DaoException(e);
        }

    }

    @Override
    public boolean update(Reward entity) throws DaoException {

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_UPDATE)) {

            statement.setString(1, entity.getName());
            statement.setString(2, entity.getDescription());
            statement.setFloat(3, entity.getCost());
            statement.setInt(4, entity.getId());

            int update = statement.executeUpdate();

            return update > 0;

        } catch (SQLException e) {
            throw new DaoException(e);
        }

    }

    @Override
    public List<Reward> findByUser(int userId) throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_SELECT_BY_USER_ID)) {
            statement.setInt(1, userId);

            ResultSet resultSet = statement.executeQuery();

            List<Reward> rewards = new ArrayList<>();
            while (resultSet.next()) {
                rewards.add(buildReward(resultSet));
            }
            return rewards;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }


    @Override
    public List<Reward> findByProject(int projectId) throws DaoException {

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_SELECT_BY_PROJECT_ID)) {
            statement.setInt(1, projectId);
            ResultSet resultSet = statement.executeQuery();

            List<Reward> rewards = new ArrayList<>();
            while (resultSet.next()) {
                rewards.add(buildReward(resultSet));
            }
            return rewards;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Builds reward object from result set.
     * @param rs result of a query.
     * @return new reward object.
     * @throws SQLException thrown if error occurs while reading result set.
     */
    private Reward buildReward(ResultSet rs) throws SQLException {
        Reward reward = new Reward();
        reward.setName(rs.getString("name"));
        reward.setDescription(rs.getString("description"));
        reward.setCost(rs.getFloat("cost"));
        reward.setId(rs.getInt("id"));
        Project project = new Project();
        project.setId(rs.getInt("project_id"));
        reward.setProject(project);
        return reward;
    }


    @Override
    public List<Reward> findAll() throws DaoException {
        throw new NotImplementedException();
    }

    @Override
    public Reward findById(int id) throws DaoException {
        throw new NotImplementedException();
    }

    @Override
    public boolean delete(int id) throws DaoException {
        throw new NotImplementedException();
    }

}
