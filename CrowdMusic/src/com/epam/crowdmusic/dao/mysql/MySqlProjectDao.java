package com.epam.crowdmusic.dao.mysql;

import com.epam.crowdmusic.dao.ProjectDao;
import com.epam.crowdmusic.dto.Project;

import com.epam.crowdmusic.dao.DaoException;
import com.mysql.jdbc.exceptions
        .jdbc4.MySQLIntegrityConstraintViolationException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Mysql implementation of project dao.
 */
public class MySqlProjectDao extends MySqlAbstractDao implements ProjectDao {

    /**
     * Mysql query for selecting projects by user.
     */
    private static final String SQL_FIND_PROJECTS_IN_USER =
            "SELECT `project`.* " +
                    "FROM `project` " +
                    "JOIN `user_has_project` ON `project_id`=`id` " +
                    "WHERE `user_id`=?";

    /**
     * Mysql query for inserting project.
     */
    private static final String SQL_INSERT =
            "INSERT INTO `project` " +
                    "(`name`, `description`,`creation_time`,`picture_path`) " +
                    "VALUES (?, ?, ?, ?)";


    /**
     * Mysql query for selecting project by name.
     */
    private static final String SQL_FIND_PROJECT_BY_NAME =
            "SELECT * FROM " +
                    "`project` WHERE " +
                    "`name` = ?";

    /**
     * Query for selecting project by id.
     */
    private static final String SQL_FIND_PROJECT_BY_ID =
            "SELECT * FROM " +
                    "`project` WHERE " +
                    "`id` = ?";

    /**
     * Query for deleting project.
     */
    private static final String SQL_DELETE =
            "DELETE FROM `project` WHERE `id`=?";

    /**
     * Query for selecting projects.
     */
    private static final String SQL_SELECT_ALL =
            "SELECT * FROM `project` ";

    /**
     * Query for updating project.
     */
    private static final String SQL_UPDATE =
            "UPDATE `project` SET `name`=?, `description`=?, `picture_path`=? "+
                    "WHERE `id`=?";

    /**
     * Project amount per page in pagination.
     */
    private static final int PROJECT_PER_PAGE = 12;

    /**
     * Query for selecting specific range of projects.
     */
    private static final String SQL_SELECT_PART =
            "SELECT * FROM `project` LIMIT ? OFFSET ?";

    /**
     * Query for selecting total page amount.
     */
    private static final String SQL_SELECT_PAGES_AMOUNT =
            "SELECT CEIL(COUNT(*)/?) FROM `project` ";

    /**
     * Subquery that selects id of genre and all its parent genres.
     */
    private static final String SQL_RECURSIVE_SUBQUERY =
                "SELECT * FROM ( " +
                    "SELECT `id` FROM ( " +
                    "SELECT * FROM `genre` " +
                    "ORDER BY `parent_genre_id`, `id`) genres_sorted " +
                    "JOIN (SELECT @pv := ?) init " +
                    "WHERE find_in_set(`parent_genre_id`, @pv)" +
                    "AND length(@pv := concat(@pv, ',', id))" +
                ") AS subquery";

    /**
     * Query that select total pages amount within specific genre.
     */
    private static final String SQL_SELECT_PAGES_AMOUNT_WITH_GENRES =
                SQL_SELECT_PAGES_AMOUNT +
                "JOIN `project_has_genre` ON `project`.`id` = `project_id` " +
                "WHERE `genre_id` IN (" +
                    SQL_RECURSIVE_SUBQUERY +
                ") OR `genre_id` = ?";


    /**
     * Query that select all project by specific genre.
     */
    private static final String SQL_FIND_PROJECTS_BY_GENRE =
            "SELECT DISTINCT `project`.* FROM `project` \n" +
            "JOIN `project_has_genre` ON `project`.`id` = `project_id`\n" +
            "WHERE `genre_id` IN ( " +
                SQL_RECURSIVE_SUBQUERY +
            ") OR `genre_id` = ? LIMIT ? OFFSET ?";



    @Override
    public boolean create(Project entity) throws DaoException {
        ResultSet resultSet;

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, entity.getName());
            statement.setString(2, entity.getDescription());
            statement.setObject(3, entity.getCreationDate());
            statement.setString(4, entity.getPicturePath());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();

            if (resultSet.next()) {
                entity.setId(resultSet.getInt(1));
                return true;
            }
            //logger.error("There is no autoincremented index after trying to
            // add record into table `users`");
            return false;
        } catch (MySQLIntegrityConstraintViolationException e) {
            throw new DaoException("Project " +
                    "with name '"
                    + entity.getName() + "' " + "already exists!");
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public int getPageAmount() throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_SELECT_PAGES_AMOUNT)) {

            statement.setInt(1, PROJECT_PER_PAGE);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
            throw new DaoException("Internal error");
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public int getPageAmount(int genreId) throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_SELECT_PAGES_AMOUNT_WITH_GENRES)) {

            statement.setInt(1, PROJECT_PER_PAGE);
            statement.setInt(2, genreId);
            statement.setInt(3, genreId);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
            throw new DaoException("Internal error");
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public Project findByName(String name) throws DaoException {

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_FIND_PROJECT_BY_NAME)) {
            statement.setString(1, name);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return buildBasicProject(resultSet);
            }
            throw new DaoException("Project " + name + " doesn't exist!");
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<Project> findByGenre(int genreId, int pageNumber) throws DaoException {

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_FIND_PROJECTS_BY_GENRE)) {
            statement.setInt(1, genreId);
            statement.setInt(2, genreId);
            statement.setInt(3, PROJECT_PER_PAGE);
            statement.setInt(4, PROJECT_PER_PAGE * (pageNumber - 1));

            ResultSet resultSet = statement.executeQuery();
            List<Project> projects = new ArrayList<>();
            while (resultSet.next()) {
                projects.add(buildBasicProject(resultSet));
            }
            return projects;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<Project> findByUser(int userId) throws DaoException {

        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_FIND_PROJECTS_IN_USER)) {

            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();

            List<Project> projects = new ArrayList<>();
            while (resultSet.next()) {
                projects.add(buildBasicProject(resultSet));
            }
            return projects;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Builds banned user object from result set.
     * @param rs result of a query.
     * @return new banned user object.
     * @throws SQLException thrown if error occurs while reading result set.
     */
    private Project buildBasicProject(ResultSet rs) throws
            SQLException {
        Project project = new Project();
        project.setId(rs.getInt("id"));
        project.setName(rs.getString("name"));
        project.setDescription(rs.getString("description"));
        project.setCreationDate(rs.getObject("creation_time", LocalDate.class));
        project.setPicturePath(rs.getString("picture_path"));
        return project;
    }


    @Override
    public boolean delete(int id) throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_DELETE)) {

            statement.setInt(1, id);
            int update = statement.executeUpdate();

            return update > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }


    @Override
    public List<Project> findAll() throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_SELECT_ALL)) {

            ResultSet resultSet = statement.executeQuery();
            List<Project> projects = new ArrayList<>();
            while (resultSet.next()) {
                projects.add(buildBasicProject(resultSet));
            }
            return projects;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<Project> findAll(int pageNumber) throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_SELECT_PART)) {
            statement.setInt(1, PROJECT_PER_PAGE);
            statement.setInt(2, PROJECT_PER_PAGE * (pageNumber - 1));

            ResultSet resultSet = statement.executeQuery();
            List<Project> projects = new ArrayList<>();
            while (resultSet.next()) {
                projects.add(buildBasicProject(resultSet));
            }
            return projects;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }


    @Override
    public Project findById(int id) throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_FIND_PROJECT_BY_ID)) {
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return buildBasicProject(resultSet);
            }
            throw new DaoException("Cannot find project!");
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }



    @Override
    public boolean update(Project entity) throws DaoException {
        try (PreparedStatement statement = getConnection().prepareStatement
                (SQL_UPDATE)) {

            statement.setString(1, entity.getName());
            statement.setString(2, entity.getDescription());
            statement.setString(3, entity.getPicturePath());
            statement.setInt(4, entity.getId());
            
            int update = statement.executeUpdate();

            return update > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        }

    }
}
