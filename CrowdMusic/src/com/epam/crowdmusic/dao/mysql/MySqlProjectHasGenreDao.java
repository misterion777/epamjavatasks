package com.epam.crowdmusic.dao.mysql;


/**
 * Mysql implementation of ProjectHasGenre dao.
 */
public class MySqlProjectHasGenreDao extends MySqlAbstractManyToManyDao {

    /**
     * Query for insert.
     */
    private static final String SQL_INSERT =
            "INSERT INTO `project_has_genre` (`project_id`, `genre_id`) " +
                    "VALUES (?,?)";


    /**
     * Query for delete by one entity.
     */
    private static final String SQL_DELETE_BY_PROJECT =
            "DELETE FROM `project_has_genre` WHERE `project_id`=?";

    /**
     * Query for delete by both entities.
     */
    private static final String SQL_DELETE =
            "DELETE FROM `project_has_genre` WHERE `project_id`=? " +
            "AND `genre_id` = ?";

    /**
     * Default constructor setting queries in base class.
     */
    MySqlProjectHasGenreDao() {
        super(SQL_INSERT, SQL_DELETE_BY_PROJECT, SQL_DELETE);
    }


}
