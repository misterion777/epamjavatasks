package com.epam.crowdmusic.dao;

import com.epam.crowdmusic.dto.Entity;

import java.util.List;


/**
 * Provides basic interface to communicate with database. Note that
 * implementation may not provides implementation for some methods.
 *
 * @param <T> the data transfer object
 */
public interface Dao<T extends Entity> {

    /**
     * Get all DTO's
     * @return list of DTO object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    List<T> findAll() throws DaoException;

    /**
     * Get DTO by its id in table.
     * @param id id of DTO.
     * @return DTO object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    T findById(int id) throws DaoException;

    /**
     * Creates row in entity table.
     * @param entity DTO object ready for insertion in database.
     * @return true, if successful
     * @throws DaoException thrown if error occurs while accessing database
     */
    boolean create(T entity) throws DaoException;

    /**
     * Updates row in entity table.
     * @param entity changed DTO object ready for update.
     * @return true, if successful
     * @throws DaoException thrown if error occurs while accessing database
     */
    boolean update(T entity) throws DaoException;

    /**
     * Deletes row from entity table by its id.
     * @param id id of an row.
     * @return true, if successful
     * @throws DaoException thrown if error occurs while accessing database
     */
    boolean delete(int id) throws DaoException;

}
