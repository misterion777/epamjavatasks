package com.epam.crowdmusic.dao;

import com.epam.crowdmusic.dto.Genre;

import java.util.List;


/**
 * Interface for access Genre entity data.
 */
public interface GenreDao extends Dao<Genre> {

    /**
     * Finds list of all genres that are part of some project.
     * @param projectId project id to find in.
     * @return list of genre entities containing in project
     * @throws DaoException thrown if error occurs while accessing database
     */
    List<Genre> findByProject(int projectId) throws DaoException;
}
