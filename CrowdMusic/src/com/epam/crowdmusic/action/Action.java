package com.epam.crowdmusic.action;

import com.epam.crowdmusic.dto.Role;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

/**
 * Base action interface. Must be implemented in case of creating new possible
 * action for current web-system.
 */
public interface Action {

    /**
     * Gets set of roles that have permission to do particular action.
     * @return set of allowed roles of current action.
     */
    Set<Role> getAllowedRoles();

    /**
     * Executes any possible action.
     * @param request http request containing some parameters needed for
     *                executing current action.
     * @return forward object containing response object or messages.
     */
    Forward execute(HttpServletRequest request);
}
