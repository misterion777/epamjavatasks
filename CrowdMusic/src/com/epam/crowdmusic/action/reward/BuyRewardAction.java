package com.epam.crowdmusic.action.reward;

import com.epam.crowdmusic.action.AuthorizedAction;
import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.dto.Role;
import com.epam.crowdmusic.dto.User;
import com.epam.crowdmusic.service.ServiceException;
import com.epam.crowdmusic.service.UserHasRewardService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class BuyRewardAction extends AuthorizedAction {


    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger(BuyRewardAction.class);

    /**
     * Default constructor setting fan as an allowed role for this action.
     */
    public BuyRewardAction(){
        getAllowedRoles().add(Role.FAN);
    }

    /**
     * Gets current user and reward id and connects them so user "own" the
     * reward.
     * @param request request containing reward id, and session with current
     *                user object.
     * @return forward object that redirect to current user page.
     */
    @Override
    public Forward execute(HttpServletRequest request) {

        Forward forward = new Forward();
        User user = (User)request.getSession().getAttribute("user");
        int rewardId = Integer.parseInt(request.getParameter("rewardId"));

        UserHasRewardService userHasRewardService = new UserHasRewardService();
        try {
            userHasRewardService.create(user.getId(),rewardId);
            forward.setRedirect(true);
            forward.setForwardPage("/user/" + user.getName());

            LOGGER.info("User " + user.getId() + " now owns reward " +
                    rewardId);
        } catch (ServiceException e) {
            LOGGER.error(e);
            forward.addAttribute("error", e.getMessage());

        }
        return forward;
    }
}
