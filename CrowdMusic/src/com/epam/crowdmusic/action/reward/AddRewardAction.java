package com.epam.crowdmusic.action.reward;

import com.epam.crowdmusic.action.AuthorizedAction;
import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.dto.Project;
import com.epam.crowdmusic.dto.Reward;
import com.epam.crowdmusic.dto.Role;
import com.epam.crowdmusic.service.RewardService;
import com.epam.crowdmusic.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;


/**
 * Action that occurs when new reward is added to project.
 */
public class AddRewardAction extends AuthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger(AddRewardAction.class);

    /**
     * Default constructor setting artist as an allowed role for this action.
     */
    public AddRewardAction(){
        getAllowedRoles().add(Role.ARTIST);
    }

    /**
     * Gets request with needed parameters and creates new reward.
     * @param request request with name, cost and description parameters.
     * @return forward object that redirect to current project page.
     */
    @Override
    public Forward execute(HttpServletRequest request) {

        String name = request.getParameter("name");
        Float cost = Float.parseFloat(request.getParameter("cost"));
        String description = request.getParameter("description");

        Forward forward = new Forward();
        Reward reward = new Reward();
        reward.setName(name);
        reward.setCost(cost);
        reward.setDescription(description);
        Project project = (Project)request.getSession().getAttribute("project");
        reward.setProject(project);


        RewardService rewardService = new RewardService();
        try {
            rewardService.create(reward);
            forward.setRedirect(true);
            forward.setForwardPage("/project/" + project.getName());
            LOGGER.info("New reward " + reward.getId() + " is created for " +
                    "project " + project.getId());
        } catch (ServiceException e) {
            LOGGER.error(e);
            forward.addAttribute("error", e.getMessage());
            forward.setRedirect(true);
            forward.setForwardPage("/WEB-INF/jsp/error.jsp");
        }
        return forward;
    }
}
