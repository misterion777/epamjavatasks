package com.epam.crowdmusic.action.search;

import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.action.UnauthorizedAction;
import com.epam.crowdmusic.dto.Entity;
import com.epam.crowdmusic.service.ProjectService;
import com.epam.crowdmusic.service.ServiceException;
import com.epam.crowdmusic.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Action that occurs when user requests for search start.
 */
public class GetSearchDataAction extends UnauthorizedAction {
    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (GetSearchDataAction.class);


    /**
     * Returns data needed for search.
     * @param request client request
     * @return forward object with list containing all projects and all users.
     */
    @Override
    public Forward execute(HttpServletRequest request) {

        Forward forward = new Forward();

        ProjectService projectService = new ProjectService();
        UserService userService = new UserService();


        List<Entity> entities = new ArrayList<>();
        try {
            entities.addAll(projectService.findAll());
            entities.addAll(userService.findAll());
            forward.addAttribute("searchData",entities);

        } catch (ServiceException e) {
            LOGGER.error(e);
            request.getSession().setAttribute("error", e.getMessage());
            forward.setRedirect(true);
            forward.setForwardPage("/error");

        }

        return forward;
    }
}
