package com.epam.crowdmusic.action.search;

import com.epam.crowdmusic.action.AuthorizedAction;
import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.dto.Role;
import com.epam.crowdmusic.service.ServiceException;
import com.epam.crowdmusic.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Action that occurs when its needed for view to get all users that have
 * ARTIST role.
 */
public class GetArtistsAction extends AuthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (GetArtistsAction.class);

    /**
     * Default constructor setting artist as an allowed role for this action.
     */
    public GetArtistsAction() {
        getAllowedRoles().add(Role.ARTIST);
    }

    /**
     * Returns list of artists to view.
     * @param request client request.
     * @return forward object with list of user objects.
     */
    @Override
    public Forward execute(HttpServletRequest request) {
        Forward forward = new Forward();

        UserService userService = new UserService();

        try {
            forward.addAttribute("artists", userService.findArtists());

        } catch (ServiceException e) {
            LOGGER.error(e);
            request.getSession().setAttribute("error", e.getMessage());
            forward.setRedirect(true);
            forward.setForwardPage("/error");

        }

        return forward;
    }
}
