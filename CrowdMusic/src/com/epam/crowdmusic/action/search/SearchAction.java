package com.epam.crowdmusic.action.search;

import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.action.UnauthorizedAction;
import com.epam.crowdmusic.dto.Project;
import com.epam.crowdmusic.dto.User;
import com.epam.crowdmusic.dao.DaoException;
import com.epam.crowdmusic.service.ProjectService;
import com.epam.crowdmusic.service.ServiceException;
import com.epam.crowdmusic.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Action that occurs when user search for specific entity.
 */
public class SearchAction extends UnauthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (SearchAction.class);

    /**
     * Get search parameters and redirects to destination page.
     *
     * @param request request with entity id and entity type.
     * @return forward object that redirects to certain page.
     */
    @Override
    public Forward execute(HttpServletRequest request) {

        Forward forward = new Forward();

        try {
            int id = Integer.parseInt(request.getParameter("id"));
            String objectType = request.getParameter("type");
            forward = getForwardByObjectType(objectType, id);

        } catch (ServiceException | NumberFormatException e) {
            LOGGER.error("Search was unsuccessful.");
            forward.addAttribute("error", "Search " +
                    "returned nothing. Try again.");
        }

        return forward;
    }

    /**
     * Creates forward with redirect to specific entity.
     * @param objectType string representation of entity type.
     * @param id entity id.
     * @return forward object with redirect.
     * @throws DaoException
     */
    private Forward getForwardByObjectType(String objectType, int id) throws
            ServiceException {
        Forward forward = new Forward();
        switch (objectType) {
            case "project":
                Project project = new ProjectService().findById(id);
                forward.setForwardPage("/project/" + project.getName());
                return forward;
            case "user":
                User user = new UserService().findById(id);
                forward.setForwardPage("/user/" + user.getName());
                return forward;
        }
        throw new IllegalArgumentException();
    }
}
