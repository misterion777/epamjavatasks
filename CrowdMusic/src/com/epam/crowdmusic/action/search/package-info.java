/**
 * Package containing logic connected to search action.
 */
package com.epam.crowdmusic.action.search;
