package com.epam.crowdmusic.action.project;

import com.epam.crowdmusic.action.AuthorizedAction;
import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.dto.Project;
import com.epam.crowdmusic.dto.Role;
import com.epam.crowdmusic.dto.User;
import com.epam.crowdmusic.service.ProjectService;
import com.epam.crowdmusic.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Action that occurs when project is deleted.
 */
public class DeleteProjectAction extends AuthorizedAction {
    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (DeleteProjectAction.class);


    /**
     * Default constructor setting artist as an allowed role for this action.
     */
    public DeleteProjectAction() {
        getAllowedRoles().add(Role.ARTIST);
    }


    /**
     * Deletes project from db.
     *
     * @param request client request.
     * @return forward object that redirects to user page.
     */
    @Override
    public Forward execute(HttpServletRequest request) {

        Forward forward = new Forward();

        HttpSession session = request.getSession();
        Project project = (Project) session.getAttribute("project");

        ProjectService projectService = new ProjectService();
        try {
            projectService.delete(project.getId());
            session.removeAttribute("project");

            User user = (User) session.getAttribute("user");
            forward.setForwardPage("/user/" + user.getName());
            forward.setRedirect(true);

        } catch (ServiceException e) {
            LOGGER.error(e);
            forward.addAttribute("error", e.getMessage());
        }
        return forward;
    }
}
