package com.epam.crowdmusic.action.project;

import com.epam.crowdmusic.action.AuthorizedAction;
import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.dto.Project;
import com.epam.crowdmusic.dto.Role;
import com.epam.crowdmusic.service.ProjectService;
import com.epam.crowdmusic.service.ServiceException;
import com.epam.crowdmusic.service.TrackService;
import com.epam.crowdmusic.service.UserHasProjectService;
import com.epam.crowdmusic.utility.FileUploadUtility;
import com.epam.crowdmusic.utility.JsonUtility;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Action that occurs when project is edited.
 */
public class EditProjectAction extends AuthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (EditProjectAction.class);

    /**
     * Default constructor setting artist as an allowed role for this action.
     */
    public EditProjectAction() {
        getAllowedRoles().add(Role.ARTIST);
    }


    /**
     * Gets multipart request with files (possibly) and updates project in db.
     * @param request multipart request with image, audios and all other data.
     * @return forward object that redirects to updated user page.
     */
    @Override
    public Forward execute(HttpServletRequest request) {
        HttpSession session = request.getSession();

        ProjectService projectService = new ProjectService();
        TrackService trackService = new TrackService();
        UserHasProjectService userHasProjectService = new
                UserHasProjectService();

        Forward forward = new Forward();
        try {
            Project project = (Project) session.getAttribute("project");

            List<FileItem> fileList = FileUploadUtility.getFileItems(request);

            project = changeProjectByFileList(project, fileList);

            for (FileItem file : fileList) {
                if (file.getContentType() != null && file.getContentType()
                        .contains("audio")) {
                    trackService.create(CreateProjectAction.createTrack
                            (file, project));
                }
                if (file.getContentType() != null && file.getContentType()
                        .contains("image")) {
                    String imagePath = "/images/" + project.getName();

                    FileUploadUtility.saveBytes(file.get(), imagePath, file
                            .getName());

                    project.setPicturePath(imagePath + "/" + file.getName());
                }
            }

            List<Integer> deletedTracksIds = JsonUtility.getIntList(
                    fileList.get(3).getString());
            trackService.deleteTracks(deletedTracksIds);

            List<Integer> artistsId = JsonUtility.getIntList(
                    fileList.get(4).getString());
            userHasProjectService.updateProjectArtists(project, artistsId);

            projectService.update(project);

            request.setAttribute("project", project);

            forward.setForwardPage("/project/" + project.getName());
            forward.setRedirect(true);


        } catch (ServiceException | IOException | FileUploadException e) {
            LOGGER.error(e);
            forward.addAttribute("error", e.getMessage());
        }

        return forward;

    }

    /**
     * Change current project object by getting all data from list of files.
     * @param project current project object.
     * @param fileList files list got from multipart request.
     * @return changed project object.
     */
    private Project changeProjectByFileList(Project project, List<FileItem>
            fileList){
        project.setName(fileList.get(0).getString());
        project.setDescription(fileList.get(1).getString());

        JsonObject data = new Gson().fromJson(fileList.get(2).getString()
                , JsonObject.class);
        project.setGenres(JsonUtility.getGenresList(data.get("genres")));

        return project;
    }
}
