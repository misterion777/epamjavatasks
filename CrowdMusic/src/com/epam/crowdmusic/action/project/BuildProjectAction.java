package com.epam.crowdmusic.action.project;

import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.action.UnauthorizedAction;
import com.epam.crowdmusic.dto.Project;
import com.epam.crowdmusic.dto.Reward;
import com.epam.crowdmusic.dto.Track;
import com.epam.crowdmusic.service.ProjectService;
import com.epam.crowdmusic.service.RewardService;
import com.epam.crowdmusic.service.ServiceException;
import com.epam.crowdmusic.service.TrackService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Action that occurs when project page is loaded.
 */
public class BuildProjectAction extends UnauthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (BuildProjectAction.class);


    /**
     * Get request with project name parameter and return all info about that
     * project.
     *
     * @param request request with projectname parameter.
     * @return forward object with project object, rewards and tracks.
     */
    @Override
    public Forward execute(HttpServletRequest request) {

        String name = (String) request.getAttribute("projectname");
        Forward forward = new Forward();

        try {
            ProjectService projectService = new ProjectService();
            TrackService trackService = new TrackService();
            RewardService rewardService = new RewardService();

            Project project = projectService.findByName(name);
            List<Track> tracks = trackService.findByProject(project.getId());
            List<Reward> rewards = rewardService.findByProject(project.getId());

            request.setAttribute("rewards", rewards);
            request.setAttribute("tracks", tracks);
            request.setAttribute("project", project);
            forward.setForwardPage("/WEB-INF/jsp/project.jsp");
        } catch (ServiceException e) {
            LOGGER.error(e);
            request.setAttribute("error", e.getMessage());
            forward.setForwardPage("/WEB-INF/jsp/error.jsp");
        }
        return forward;
    }
}
