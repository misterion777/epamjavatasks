package com.epam.crowdmusic.action.project;

import com.epam.crowdmusic.action.AuthorizedAction;
import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.dto.Project;
import com.epam.crowdmusic.dto.Reward;
import com.epam.crowdmusic.dto.Role;
import com.epam.crowdmusic.dto.Track;
import com.epam.crowdmusic.dto.User;
import com.epam.crowdmusic.service.ProjectService;
import com.epam.crowdmusic.service.RewardService;
import com.epam.crowdmusic.service.ServiceException;
import com.epam.crowdmusic.service.TrackService;
import com.epam.crowdmusic.utility.FileUploadUtility;
import com.epam.crowdmusic.utility.JsonUtility;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


/**
 * Action that occurs when artist user creates new project.
 */
public class CreateProjectAction extends AuthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (CreateProjectAction.class);


    /**
     * Default constructor setting artist as an allowed role for this action.
     */
    public CreateProjectAction() {
        getAllowedRoles().add(Role.ARTIST);
    }

    /**
     * Gets multipart request and sets project object to session.
     *
     * @param request multipart request with audio and image files, and all
     *                data needed for project creation.
     * @return forward object that redirects to newly created project page.
     */
    @Override
    public Forward execute(HttpServletRequest request) {

        Forward forward = new Forward();
        try {
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("user");

            List<FileItem> fileList = FileUploadUtility.getFileItems(request);

            Project project = createProjectByFileList(fileList);
            project.getUsers().add(user);

            ProjectService projectService = new ProjectService();
            TrackService trackService = new TrackService();
            RewardService rewardService = new RewardService();

            if (projectService.create(project)) {
                for (FileItem file : fileList) {
                    if (file.getContentType() != null && file.getContentType
                            ().contains("audio")) {
                        trackService.create(createTrack(file, project));
                    }
                    if (file.getFieldName().equals("rewardData")) {
                        Reward reward = JsonUtility.getRewardFromJson(
                                file.getString());
                        reward.setProject(project);
                        rewardService.create(reward);
                    }
                }

                request.setAttribute("project", project);
                request.getSession().setAttribute("project", project);
                forward.setForwardPage("/project/" + project.getName());
                forward.setRedirect(true);
            } else {
                LOGGER.error("Cannot create new project");
                forward.addAttribute("error", "Cannot create new project");
            }


        } catch (ServiceException | IOException | FileUploadException e) {
            forward.addAttribute("error", e.getMessage());
            LOGGER.error(e);
        }

        return forward;
    }


    /**
     * Saves audio on server and creates track object for current project from
     * multipart file item.
     *
     * @param file    multipart file object.
     * @param project newly created project.
     * @return track object that is ready to be inserted in db.
     * @throws IOException thrown in case of error during audio file saving.
     */
    static Track createTrack(FileItem file, Project project) throws
            IOException {
        String path = "/audio/" + project.getName();
        FileUploadUtility.saveBytes(file.get(), path, file.getName());

        Track track = new Track();

        track.setName(FilenameUtils.removeExtension(file
                .getName()));

        track.setPath(path + "/" + file.getName());
        track.setProject(project);

        return track;
    }

    /**
     * Create project object from uploaded files.
     *
     * @param fileList list of uploaded multipart files.
     * @return project object that is ready to be inserted in db.
     * @throws IOException thrown in case of error during project image file
     *                     saving.
     */
    private Project createProjectByFileList(List<FileItem> fileList) throws
            IOException {
        Project project = JsonUtility.getProjectFromJson(fileList.get(0)
                .getString());

        FileItem image = fileList.get(1);
        String imagePath = "/images/" + project.getName();

        FileUploadUtility.saveBytes(image.get(), imagePath, image.getName());

        project.setPicturePath(imagePath + "/" + image.getName());
        return project;
    }

}

