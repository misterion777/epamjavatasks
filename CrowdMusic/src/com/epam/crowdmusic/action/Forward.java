package com.epam.crowdmusic.action;

import java.util.HashMap;
import java.util.Map;

/**
 * Class that contains response objects set by actions.
 */
public class Forward {

    /**
     * Map of response attributes.
     */
    private Map<String, Object> attributes = new HashMap<>();
    /**
     * Marks whether controller should redirect to another page.
     */
    private boolean redirect;
    /**
     * Marks whether forward attributes are downloadable by client.
     */
    private boolean isOutputStream;

    /**
     * Default empty constructor.
     */
    public Forward(){
    }

    /**
     * Constructor that sets to attributes page to forward.
     * @param forwardPage page that controller will forward to.
     */
    public Forward(String forwardPage){
        attributes.put("forward", forwardPage);
    }


    public Map<String, Object> getAttributes() {
        return attributes;
    }

    /**
     * Setter for attributes.
     * @param attributes map of attributes.
     */
    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    /**
     * Adds one attribute to forward attributes.
     * @param key string key
     * @param value any object
     */
    public void addAttribute(String key, Object value){
        attributes.put(key,value);
    }

    /**
     * Gets attribute by key.
     * @param key string key.
     * @return object stored by that key.
     */
    public Object getAttribute(String key){
        return attributes.get(key);
    }

    /**
     * Sets forward page.
     * @param forwardPage page that controller will forward to.
     */
    public void setForwardPage(String forwardPage){
        attributes.put("forward", forwardPage);
    }

    /**
     * Gets forward page.
     * @return page that controller will forward to.
     */
    public String getForwardPage(){
        return (String)attributes.get("forward");
    }

    /**
     * Check if current forward is redirectable.
     * @return
     */
    public boolean isRedirect() {
        return redirect;
    }

    /**
     * Set current redirect value.
     * @param redirect if true controller will redirect to
     * {@link #getForwardPage()}
     */
    public void setRedirect(boolean redirect) {
        this.redirect = redirect;
    }

    /**
     * Checks if current forward is an output stream
     * @return
     */
    public boolean isOutputStream() {
        return isOutputStream;
    }

    /**
     * Sets current output stream mark.
     * @param outputStream if true controller will send forward attributes as
     *                     output stream to client.
     */
    public void setOutputStream(boolean outputStream) {
        isOutputStream = outputStream;
    }
}
