package com.epam.crowdmusic.action;

public class ActionException extends Exception {

    public ActionException() {
    }

    public ActionException(String message, Throwable cause) {
        super(message, cause);
    }

    public ActionException(String message) {
        super(message);
    }

    public ActionException(Throwable cause) {
        super(cause);
    }
}
