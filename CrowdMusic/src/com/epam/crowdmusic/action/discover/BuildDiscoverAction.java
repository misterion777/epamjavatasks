package com.epam.crowdmusic.action.discover;

import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.action.UnauthorizedAction;
import com.epam.crowdmusic.dto.Genre;
import com.epam.crowdmusic.dto.Project;
import com.epam.crowdmusic.service.GenreService;
import com.epam.crowdmusic.service.ProjectService;
import com.epam.crowdmusic.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Action that occurs when discover page loads.
 */
public class BuildDiscoverAction extends UnauthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (BuildDiscoverAction.class);

    /**
     * Sends all genres and projects to view.
     * @param request request from client.
     * @return forward object, containing genres, projects objects.
     */
    @Override
    public Forward execute(HttpServletRequest request) {

        Forward forward = new Forward();
        ProjectService projectService = new ProjectService();
        GenreService genreService = new GenreService();

        try {
            List<Genre> genres = genreService.findAll();
            List<Project> projects = projectService.findAll(0,1);
            int pageAmount = projectService.getPageAmount(0);
            forward.addAttribute("pageAmount",pageAmount);
            forward.addAttribute("genres",genres);
            forward.addAttribute("projects", projects);

        } catch (ServiceException e) {
            LOGGER.error(e);
            request.getSession().setAttribute("error", e.getMessage());
            forward.setForwardPage("/error");
        }

        return forward;
    }
}
