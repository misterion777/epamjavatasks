package com.epam.crowdmusic.action.discover;

import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.action.UnauthorizedAction;
import com.epam.crowdmusic.dto.Project;
import com.epam.crowdmusic.service.ProjectService;
import com.epam.crowdmusic.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * Action that occurs when discover page loads new projects by pages and genres.
 */
public class GetProjectsAction extends UnauthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (GetProjectsAction.class);


    /**
     * Gets page and genre from request and returns projects that match
     * conditions.
     *
     * @param request request with page number and genre id parameters.
     * @return list of projects that match conditions.
     */
    @Override
    public Forward execute(HttpServletRequest request) {
        int pageNumber = Integer.parseInt(request.getParameter("page"));
        int genreId = Integer.parseInt(request.getParameter("genreId"));

        Forward forward = new Forward();
        ProjectService projectService = new ProjectService();

        try {
            List<Project> projects = projectService.findAll(genreId,
                    pageNumber);

            if (Boolean.parseBoolean(request.getParameter("updatePageAmount")
            )) {
                int pageAmount = projectService.getPageAmount(genreId);
                forward.addAttribute("pageAmount", pageAmount);
            }

            forward.addAttribute("projects", projects);

        } catch (ServiceException e) {
            LOGGER.error(e);
            request.getSession().setAttribute("error", e.getMessage());
            forward.setForwardPage("/error");
        }

        return forward;
    }
}
