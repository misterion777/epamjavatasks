/**
 * Package containing logic for actions called from discover page.
 */
package com.epam.crowdmusic.action.discover;
