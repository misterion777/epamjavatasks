package com.epam.crowdmusic.action.stream;

import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.action.UnauthorizedAction;
import com.epam.crowdmusic.utility.FileUploadUtility;
import org.apache.commons.io.FilenameUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Action that occurs when user requests for file in upload folder.
 */
public class OutputStreamAction extends UnauthorizedAction {

    /**
     * Returns forward object with path to file to be downloaded.
     * @param request client request.
     * @return forward object with path to file to be downloaded.
     */
    @Override
    public Forward execute(HttpServletRequest request) {

        Forward forward = new Forward();
        String path = (String) request.getAttribute("path");

        forward.setOutputStream(true);
        forward.addAttribute("path", FileUploadUtility.FULL_PATH + path);
        forward.addAttribute("filename",FilenameUtils.getName(path));
        return forward;
    }
}
