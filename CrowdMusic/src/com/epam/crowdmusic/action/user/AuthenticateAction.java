package com.epam.crowdmusic.action.user;

import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.action.UnauthorizedAction;
import com.epam.crowdmusic.dto.User;
import com.epam.crowdmusic.service.ServiceException;
import com.epam.crowdmusic.service.UserService;
import com.epam.crowdmusic.utility.SecurityUtility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Action that occurs when user tries to authenticate into the system.
 */
public class AuthenticateAction extends UnauthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (AuthenticateAction.class);

    /**
     * Gets login parameters and tries to create new user in session.
     *
     * @param request request containing name and password
     * @return forward object that redirects to user page.
     */
    @Override
    public Forward execute(HttpServletRequest request) {
        String name = request.getParameter("name");
        String psw = SecurityUtility.md5(request.getParameter("psw"));
        Forward forward = new Forward();

        try {
            UserService userService = new UserService();
            User user = userService.findByNameAndPassword(name, psw);

            HttpSession session = request.getSession();

            session.setAttribute("user", user);
            forward.setRedirect(true);
            forward.setForwardPage("/user/" + user.getName());
            LOGGER.info(user.getId() + " successfully entered the system from"
                    + " " + request.getRemoteAddr());

        } catch (ServiceException e) {
            LOGGER.error(e);
            forward.addAttribute("error", e.getMessage());
        }
        return forward;
    }
}
