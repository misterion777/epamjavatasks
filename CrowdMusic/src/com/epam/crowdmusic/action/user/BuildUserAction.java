package com.epam.crowdmusic.action.user;

import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.action.UnauthorizedAction;
import com.epam.crowdmusic.dto.User;
import com.epam.crowdmusic.service.ServiceException;
import com.epam.crowdmusic.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;


/**
 * Action that occurs when user page is opened.
 */
public class BuildUserAction extends UnauthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (BuildUserAction.class);

    @Override
    public Forward execute(HttpServletRequest request) {

        String username = (String) request.getAttribute("username");
        Forward forward = new Forward();

        try {
            UserService userService = new UserService();
            User user = userService.findUserByName(username);

            request.setAttribute("user", user);
            forward.setForwardPage("/WEB-INF/jsp/user.jsp");

        } catch (ServiceException e) {
            LOGGER.error(e);
            request.setAttribute("error", e.getMessage());
            forward.setForwardPage("/WEB-INF/jsp/error.jsp");
        }

        return forward;
    }
}
