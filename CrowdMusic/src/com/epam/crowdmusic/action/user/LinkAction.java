package com.epam.crowdmusic.action.user;

import com.epam.crowdmusic.action.ActionException;
import com.epam.crowdmusic.action.AuthorizedAction;
import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.dto.Role;
import com.epam.crowdmusic.dto.User;
import com.epam.crowdmusic.service.ServiceException;
import com.epam.crowdmusic.service.UserService;
import com.epam.crowdmusic.utility.SecurityUtility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;


/**
 * Action that occurs when user tries to link two accounts.
 */
public class LinkAction extends AuthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (LinkAction.class);

    /**
     * Default constructor setting all roles as an allowed roles for this
     * action.
     */
    public LinkAction() {
        getAllowedRoles().addAll(Arrays.asList(Role.values()));
    }

    /**
     * Gets link parameters and links two accounts.
     * @param request request containing name and password for account to
     *                link with.
     * @return forward object that redirects to current user page.
     */
    @Override
    public Forward execute(HttpServletRequest request) {

        HttpSession session = request.getSession();
        String name = request.getParameter("linkName");
        String psw = SecurityUtility.md5(request.getParameter("linkPsw"));
        Forward forward = new Forward();

        try {
            UserService userService = new UserService();
            User anotherUser = userService.findByNameAndPassword(name, psw);
            User currentUser = (User)session.getAttribute("user");

            if (currentUser.getRole() == anotherUser.getRole()){
                throw new ActionException("You cannot link two " +
                    currentUser.getRole().toString().toLowerCase() +"accounts!");
            }

            userService.linkUsers(currentUser.getId(), anotherUser.getId());

            forward.setRedirect(true);
            forward.setForwardPage("/user/" + currentUser.getName());
            LOGGER.info("User " + currentUser.getId() + " has successfully " +
                    "linked to user " + anotherUser.getId());

        } catch (ServiceException | ActionException e) {
            LOGGER.error(e);
            forward.addAttribute("error", e.getMessage());
        }
        return forward;
    }
}
