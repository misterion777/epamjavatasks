package com.epam.crowdmusic.action.user;

import com.epam.crowdmusic.action.AuthorizedAction;
import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.dto.Role;
import com.epam.crowdmusic.dto.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;


/**
 * Action that occurs when user switched to another already linked account.
 */
public class SwitchAction extends AuthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (SwitchAction.class);

    /**
     * Default constructor setting all roles as an allowed roles for this
     * action.
     */
    public SwitchAction() {
        getAllowedRoles().addAll(Arrays.asList(Role.values()));
    }

    /**
     * Gets linked user from session and switches session object to linked user.
     *
     * @param request client request.
     * @return forward object that redirects to linked user page.
     */
    @Override
    public Forward execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        User oldUser = (User) session.getAttribute("user");
        User newUser = oldUser.getLinkedUser();

        Forward forward = new Forward();
        newUser.setLinkedUser(oldUser);
        session.setAttribute("user", newUser);
        forward.setForwardPage("/user/" + newUser.getName());
        LOGGER.info("User " + oldUser.getId() + "successfully switched to"
                + " user " + newUser.getId());
        return forward;
    }
}
