package com.epam.crowdmusic.action.user;

import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.action.UnauthorizedAction;
import com.epam.crowdmusic.dto.Role;
import com.epam.crowdmusic.dto.User;
import com.epam.crowdmusic.service.ServiceException;
import com.epam.crowdmusic.service.UserService;
import com.epam.crowdmusic.utility.SecurityUtility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * Action that occurs on user registration
 */
public class RegisterAction extends UnauthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (RegisterAction.class);

    /**
     * Gets basic registration data and saves user object in db
     * @param request request containing user role, username, email, password.
     * @return forward object that redirect to newly created user page.
     */
    @Override
    public Forward execute(HttpServletRequest request) {

        String role = request.getParameter("role");
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        String psw = SecurityUtility.md5(request.getParameter("psw"));

        User user = new User();
        user.setRole(Role.valueOf(role.toUpperCase()));
        user.setName(username);
        user.setEmail(email);
        user.setPassword(psw);
        user.setBio("No bio yet.");
        user.setLocation("Unknown place, unknown galaxy");

        Forward forward = new Forward();

        try {
            UserService service = new UserService();

            if (service.create(user)) {
                HttpSession session = request.getSession();
                session.setAttribute("user", user);
                forward.setForwardPage("/user/" + user.getName());
                forward.setRedirect(true);
                LOGGER.info("New user " + user.getId() + "was successfully " +
                        "created.");
            } else {
                forward.addAttribute("error", "Cannot create new user");
            }
         } catch (ServiceException e) {
            LOGGER.error(e);
            forward.addAttribute("error", e.getMessage());
        }

        return forward;
    }
}
