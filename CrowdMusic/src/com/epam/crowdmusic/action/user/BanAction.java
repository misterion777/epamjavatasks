package com.epam.crowdmusic.action.user;

import com.epam.crowdmusic.action.AuthorizedAction;
import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.dto.BannedUser;
import com.epam.crowdmusic.dto.Role;
import com.epam.crowdmusic.dto.User;
import com.epam.crowdmusic.service.BannedUserService;
import com.epam.crowdmusic.service.ServiceException;
import com.epam.crowdmusic.utility.DateUtility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Action that occurs when admin bans some specific user.
 */
public class BanAction extends AuthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (BanAction.class);

    /**
     * Default constructor setting admin as an allowed role for this action.
     */
    public BanAction(){
        getAllowedRoles().add(Role.ADMIN);
    }

    /**
     * Gets ban parameters and bans the user.
     * @param request request containing banned user id, ban time, and reason
     *               of ban.
     * @return forward objects that returns operation result.
     */
    @Override
    public Forward execute(HttpServletRequest request) {

        Forward forward = new Forward();

        int userId = Integer.parseInt(request.getParameter("userId"));
        String time = request.getParameter("banTime");
        String reason = request.getParameter("reason");

        BannedUser bannedUser = new BannedUser();
        bannedUser.setBanReason(reason);
        bannedUser.setExpirationDateTime(DateUtility.getDateTimeFromNow(time));
        User user = new User();
        user.setId(userId);
        bannedUser.setUser(user);

        BannedUserService bannedUserService = new BannedUserService();
        try {
            bannedUserService.create(bannedUser);
            forward.addAttribute("success", "User successfully banned!");
            LOGGER.info("User " + userId + "is banned till" +
                    bannedUser.getExpirationDateTime() +
                    "because: " + bannedUser.getBanReason());
        } catch (ServiceException e) {
            LOGGER.error(e);
            request.getSession().setAttribute("error", e);
            forward.setForwardPage("/error");
        }

        return forward;
    }
}
