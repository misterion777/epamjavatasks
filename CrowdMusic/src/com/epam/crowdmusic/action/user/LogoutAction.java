package com.epam.crowdmusic.action.user;

import com.epam.crowdmusic.action.AuthorizedAction;
import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.dto.Role;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;


/**
 * Action that occurs when user tries to logout.
 */
public class LogoutAction extends AuthorizedAction {

    /**
     * Default constructor setting all roles as an allowed roles for this
     * action.
     */
    public LogoutAction() {
        getAllowedRoles().addAll(Arrays.asList(Role.values()));
    }

    /**
     * Destroys user object from session.
     *
     * @param request client request.
     * @return forward object that redirects to main page.
     */
    @Override
    public Forward execute(HttpServletRequest request) {
        request.getSession().removeAttribute("user");
        Forward forward = new Forward("/");
        forward.setRedirect(true);
        return forward;
    }
}
