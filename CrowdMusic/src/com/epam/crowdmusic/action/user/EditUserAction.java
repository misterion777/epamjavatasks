package com.epam.crowdmusic.action.user;

import com.epam.crowdmusic.action.AuthorizedAction;
import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.dto.Role;
import com.epam.crowdmusic.dto.User;
import com.epam.crowdmusic.service.ServiceException;
import com.epam.crowdmusic.service.UserService;
import com.epam.crowdmusic.utility.FileUploadUtility;
import com.epam.crowdmusic.utility.JsonUtility;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


/**
 * Action that occurs when trying to edit user page.
 */
public class EditUserAction extends AuthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger
            (EditUserAction.class);

    /**
     * Default constructor setting all roles as an allowed roles for this
     * action.
     */
    public EditUserAction() {
        getAllowedRoles().addAll(Arrays.asList(Role.values()));
    }

    /**
     * Gets edit parameters and redirects to newly updated user page.
     *
     * @param request request containing multipart data: image file, and
     *                other user data.
     * @return forward object that redirects to updated user page.
     */
    @Override
    public Forward execute(HttpServletRequest request) {

        Forward forward = new Forward();

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        try {
            List<FileItem> fileList = FileUploadUtility.getFileItems(request);

            for (FileItem file : fileList) {
                if (file.getContentType() != null && file.getContentType()
                        .equals("image/jpeg")) {
                    String imagePath = "/images/" + user.getName();
                    FileUploadUtility.saveBytes(file.get(), imagePath, file
                            .getName());
                    user.setPicturePath(imagePath + "/" + file.getName());
                } else {
                    JsonUtility.changeUserObjectByJson(user, file.getString());
                }
            }

            UserService service = new UserService();

            if (service.update(user)) {
                session.setAttribute("user", user);
                forward.setForwardPage("/user/" + user.getName());
                LOGGER.info("User " + user.getId() + "has successfully edited"
                        + " the page!");
            } else {
                forward.addAttribute("error", "Cannot update user!");
            }

        } catch (FileUploadException | IOException | NoSuchFieldException |
                IllegalAccessException | ServiceException e) {
            LOGGER.error(e);
            forward.addAttribute("error", e.getMessage());
        }
        return forward;
    }
}
