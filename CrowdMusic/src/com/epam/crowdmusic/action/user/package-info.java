/**
 * Package containing all logic for actions connected to user entity.
 */
package com.epam.crowdmusic.action.user;
