package com.epam.crowdmusic.action;

import com.epam.crowdmusic.dto.Role;

import java.util.HashSet;
import java.util.Set;


/**
 * Abstract action that can be done only by authorized users.
 */
public abstract class AuthorizedAction implements Action {
    /**
     * Set of roles of users that can do such action.
     */
    private Set<Role> allowedRoles = new HashSet<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Role> getAllowedRoles() {
        return allowedRoles;
    }
}
