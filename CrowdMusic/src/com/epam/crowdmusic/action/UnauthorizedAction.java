package com.epam.crowdmusic.action;

import com.epam.crowdmusic.dto.Role;

import java.util.Set;

/**
 * Abstract action that can be done by unauthorized users.
 */
public abstract class UnauthorizedAction implements Action {

    /**
     * All users can make this action, so returns null.
     * @return null object
     */
    @Override
    public Set<Role> getAllowedRoles() {
        return null;
    }

}
