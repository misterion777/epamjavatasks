package com.epam.crowdmusic.action.factory;


import com.epam.crowdmusic.action.Action;

/**
 * Functional interface containing function for creating object that
 * implements action interface
 */
@FunctionalInterface
public interface ConcreteActionFactory {
    /**
     * Creates object that implements Action interface.
     * @return Action object.
     */
    Action createAction();
}
