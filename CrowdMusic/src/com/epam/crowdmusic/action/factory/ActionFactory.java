package com.epam.crowdmusic.action.factory;

import com.epam.crowdmusic.action.Action;
import com.epam.crowdmusic.action.discover.BuildDiscoverAction;
import com.epam.crowdmusic.action.discover.GetProjectsAction;
import com.epam.crowdmusic.action.genre.GetGenresAction;
import com.epam.crowdmusic.action.project.BuildProjectAction;
import com.epam.crowdmusic.action.project.DeleteProjectAction;
import com.epam.crowdmusic.action.project.EditProjectAction;
import com.epam.crowdmusic.action.reward.AddRewardAction;
import com.epam.crowdmusic.action.reward.BuyRewardAction;
import com.epam.crowdmusic.action.search.GetArtistsAction;
import com.epam.crowdmusic.action.search.GetSearchDataAction;
import com.epam.crowdmusic.action.search.SearchAction;
import com.epam.crowdmusic.action.stream.OutputStreamAction;

import com.epam.crowdmusic.action.project.CreateProjectAction;
import com.epam.crowdmusic.action.user.AuthenticateAction;
import com.epam.crowdmusic.action.user.BanAction;
import com.epam.crowdmusic.action.user.BuildUserAction;
import com.epam.crowdmusic.action.user.EditUserAction;
import com.epam.crowdmusic.action.user.LinkAction;
import com.epam.crowdmusic.action.user.LogoutAction;
import com.epam.crowdmusic.action.user.RegisterAction;
import com.epam.crowdmusic.action.user.SwitchAction;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Factory that creates action object from string url.
 */
public class ActionFactory {

    /**
     * Static map for mapping string values and action classes.
     */
    private static final Map<String, ConcreteActionFactory> FACTORY_MAP =
            new ConcurrentHashMap<>();
    static {
        FACTORY_MAP.put("authenticate", AuthenticateAction::new);
        FACTORY_MAP.put("logout", LogoutAction::new);
        FACTORY_MAP.put("register", RegisterAction::new);
        FACTORY_MAP.put("builduser", BuildUserAction::new);
        FACTORY_MAP.put("createproject", CreateProjectAction::new);
        FACTORY_MAP.put("buildproject", BuildProjectAction::new);
        FACTORY_MAP.put("deleteproject", DeleteProjectAction::new);
        FACTORY_MAP.put("builddiscover", BuildDiscoverAction::new);
        FACTORY_MAP.put("getgenres", GetGenresAction::new);
        FACTORY_MAP.put("outputstream", OutputStreamAction::new);
        FACTORY_MAP.put("addreward", AddRewardAction::new);
        FACTORY_MAP.put("buyreward", BuyRewardAction::new);
        FACTORY_MAP.put("edituser", EditUserAction::new);
        FACTORY_MAP.put("getprojects", GetProjectsAction::new);
        FACTORY_MAP.put("editproject", EditProjectAction::new);
        FACTORY_MAP.put("getsearchdata", GetSearchDataAction::new);
        FACTORY_MAP.put("search", SearchAction::new);
        FACTORY_MAP.put("getartists", GetArtistsAction::new);
        FACTORY_MAP.put("link", LinkAction::new);
        FACTORY_MAP.put("switch", SwitchAction::new);
        FACTORY_MAP.put("ban", BanAction::new);
    }


    /**
     * Creates action object from string name got from url.
     * @param actionName string representation of action.
     * @return action object.
     */
    public static Action getAction(String actionName) {
        ConcreteActionFactory factory = FACTORY_MAP.get(actionName);
        if (factory == null){
            throw new IllegalArgumentException();
        }
        return factory.createAction();
    }
}
