package com.epam.crowdmusic.action.genre;

import com.epam.crowdmusic.action.Forward;
import com.epam.crowdmusic.action.UnauthorizedAction;
import com.epam.crowdmusic.dto.Genre;
import com.epam.crowdmusic.service.GenreService;
import com.epam.crowdmusic.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * Action that occurs when its needed for view to get all genres.
 */
public class GetGenresAction extends UnauthorizedAction {

    /**
     * Event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger(GetGenresAction
            .class);


    /**
     * Returns all genres to view.
     *
     * @param request client request.
     * @return forward object with list of genres.
     */
    @Override
    public Forward execute(HttpServletRequest request) {

        Forward forward = new Forward();
        GenreService genreService = new GenreService();
        try {

            List<Genre> genres = genreService.findAll();
            forward.addAttribute("genres", genres);

        } catch (ServiceException e) {
            LOGGER.error(e);
            forward.addAttribute("error", e.getMessage());
            forward.setRedirect(true);
            forward.setForwardPage("/WEB-INF/jsp/error.jsp");
        }

        return forward;
    }
}
