/**
 * Package containing logic for actions connected with genre entity.
 */
package com.epam.crowdmusic.action.genre;
