package com.epam.crowdmusic.service;

import com.epam.crowdmusic.dao.GenreDao;
import com.epam.crowdmusic.dao.ManyToManyDao;
import com.epam.crowdmusic.dao.ProjectDao;
import com.epam.crowdmusic.dto.Genre;
import com.epam.crowdmusic.dto.Project;
import com.epam.crowdmusic.dao.DaoException;

import java.util.List;

/**
 * Provides facade for accessing info about Project entity.
 */
public class ProjectService extends AbstractService {

    /**
     * Updates row in project table
     * @param project project entity
     * @return operation result
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public boolean update(Project project) throws ServiceException {
        try {
            ProjectDao projectDao = daoFactory.getProjectDao();
            GenreDao genreDao = daoFactory.getGenreDao();
            ManyToManyDao projectHasGenreDao = daoFactory.getManyToManyDao
                    ("projectHasGenre");


            List<Genre> currentGenres = genreDao.findByProject(project.getId());

            for(Genre currentGenre : currentGenres){
                if(!project.getGenres().contains(currentGenre)){
                    projectHasGenreDao.delete(project.getId(), currentGenre.getId());

                }
            }
            for (Genre genre : project.getGenres()) {
                if (!currentGenres.contains(genre)){
                    projectHasGenreDao.create(project.getId(), genre.getId());
                }
            }

            return projectDao.update(project);
        }
        catch (DaoException e){
            throw new ServiceException(e);
        }

    }

    /**
     * Creates new row in project table
     * @param project project entity
     * @return operation result
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public boolean create(Project project) throws
            ServiceException {
        try {
            ProjectDao projectDao = daoFactory.getProjectDao();
            ManyToManyDao userHasProjectDao = daoFactory.getManyToManyDao
                    ("userHasProject");

            ManyToManyDao projectHasGenreDao = daoFactory.getManyToManyDao
                    ("projectHasGenre");

            if (projectDao.create(project)) {
                userHasProjectDao.create(project.getUsers().get
                        (0).getId(),project.getId());

                for (Genre genre : project.getGenres()) {
                    projectHasGenreDao.create(project.getId(), genre.getId());
                }
                return true;
            }
            return false;
        } catch (DaoException e){
            throw new ServiceException(e);
        }

    }

    /**
     * Delets row in project table by id
     * @param projectId project id
     * @return operation result
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public boolean delete(int projectId) throws ServiceException {

        try{
            ProjectDao projectDao = daoFactory.getProjectDao();
            ManyToManyDao userHasProjectDao = daoFactory.getManyToManyDao
                    ("userHasProjectDao");

            return userHasProjectDao.delete(projectId)
                    && projectDao.delete(projectId);
        }catch (DaoException e){
            throw new ServiceException(e);
        }

    }

    /**
     * Gets whole page amount by genre id
     * @param genreId genre id
     * @return operation result
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public int getPageAmount(int genreId) throws ServiceException {

        try{
            ProjectDao projectDao = daoFactory.getProjectDao();
            if (genreId == 0){
                return projectDao.getPageAmount();
            }
            return projectDao.getPageAmount(genreId);
        } catch (DaoException e){
            throw new ServiceException(e);
        }

    }

    /**
     * Finds list of all project in project table
     * @return list of all projects
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public List<Project> findAll() throws ServiceException {
        try {
            return daoFactory.getProjectDao().findAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds list of project by specific genre and page number
     * @param genreId genre id
     * @param pageNumber discover page number
     * @return list of projects matching conditions
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public List<Project> findAll(int genreId, int pageNumber) throws
            ServiceException {
        try{
            ProjectDao projectDao = daoFactory.getProjectDao();
            if (genreId == 0){
                return projectDao.findAll(pageNumber);
            }
            return projectDao.findByGenre(genreId, pageNumber);
        }
        catch (DaoException e){
            throw new ServiceException(e);
        }
    }

    /**
     * Finds project by id
     * @param id id of a project
     * @return project matching conditions
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public Project findById(int id) throws ServiceException {
        try {
            return daoFactory.getProjectDao().findById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds project by its name
     * @param name name of a project
     * @return project mathching condition
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public Project findByName(String name) throws ServiceException {
        try{
            Project project = daoFactory.getProjectDao().findByName(name);
            project.setUsers(daoFactory.getUserDao().findByProject(project
                    .getId()));
            project.setGenres(daoFactory.getGenreDao().findByProject(project
                    .getId()));
            return project;
        } catch (DaoException e){
            throw new ServiceException(e);
        }

    }


}
