package com.epam.crowdmusic.service;

import com.epam.crowdmusic.dao.ManyToManyDao;
import com.epam.crowdmusic.dto.Project;
import com.epam.crowdmusic.dto.User;
import com.epam.crowdmusic.dao.DaoException;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Provides facade for accessing UserHasProject table.
 */
public class UserHasProjectService extends AbstractService {

    /**
     * Deletes or creates new many to many connections between project and users
     * @param project project entity
     * @param usersId list of users id to be updated
     * @return result of operation
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public boolean updateProjectArtists(Project project, List<Integer>
            usersId) throws ServiceException {
        try {
            ManyToManyDao userHasProjectDao = daoFactory.getManyToManyDao
                    ("userHasProject");

            List<User> currentUsers = project.getUsers();

            boolean result = true;
            for (User user : currentUsers) {
                if (!usersId.contains(user.getId())) {
                    result &= userHasProjectDao.delete(user.getId(), project
                            .getId());
                }
            }
            List<Integer> currentUserIds = currentUsers.stream().map
                    (User::getId)
                    .collect(Collectors.toList());
            for (Integer userId : usersId) {
                if (!currentUserIds.contains(userId)) {
                    result &= userHasProjectDao.create(userId, project.getId());
                }
            }
            return result;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }

    }

}
