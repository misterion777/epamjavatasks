package com.epam.crowdmusic.service;

import com.epam.crowdmusic.dao.ManyToManyDao;
import com.epam.crowdmusic.dao.DaoException;

/**
 * Provides facade for accessing UserHasReward table.
 */
public class UserHasRewardService extends AbstractService {

    /**
     * Creates row in user has reward tabke
     * @param userId user id
     * @param rewardId reward id
     * @return operation result
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public boolean create(int userId, int rewardId) throws ServiceException {
        try{
            ManyToManyDao manyToManyDao = daoFactory.getManyToManyDao
                    ("userHasReward");
            return manyToManyDao.create(userId, rewardId);
        } catch (DaoException e){
            throw new ServiceException(e);
        }


    }

}
