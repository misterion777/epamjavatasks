package com.epam.crowdmusic.service;

import com.epam.crowdmusic.dao.RewardDao;
import com.epam.crowdmusic.dao.UserDao;
import com.epam.crowdmusic.dto.Reward;
import com.epam.crowdmusic.dao.DaoException;

import java.util.List;

/**
 * Provides facade for accessing info about Reward entity.
 */
public class RewardService extends AbstractService {

    /**
     * Find list of rewards by project that contains these rewards
     * @param projectId project id
     * @return list of rewards matching conditions
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public List<Reward> findByProject (int projectId) throws ServiceException {
        try{
            RewardDao rewardDao = daoFactory.getRewardDao();
            UserDao userDao = daoFactory.getUserDao();

            List<Reward> rewards = rewardDao.findByProject(projectId);
            for(Reward reward : rewards){
                reward.setUsers(userDao.findByReward(reward.getId()));
            }
            return rewards;
        }catch (DaoException e){
            throw new ServiceException(e);
        }

    }

    /**
     * Creates new reward
     * @param reward reward entity
     * @return result of operation
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public boolean create(Reward reward) throws ServiceException {
        try {
            return daoFactory.getRewardDao().create(reward);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

}
