package com.epam.crowdmusic.service;

import com.epam.crowdmusic.dao.DaoFactory;
import com.epam.crowdmusic.dao.mysql.MySqlDaoFactory;

/**
 * Abstract service class.
 */
public abstract class AbstractService {

    /**
     * Implemented dao factory object.
     */
    DaoFactory daoFactory = new MySqlDaoFactory();

}
