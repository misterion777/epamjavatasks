package com.epam.crowdmusic.service;

import com.epam.crowdmusic.dao.TrackDao;
import com.epam.crowdmusic.dto.Track;
import com.epam.crowdmusic.dao.DaoException;
import com.epam.crowdmusic.utility.FileUploadUtility;

import java.util.List;


/**
 * Provides facade for accessing info about Track entity.
 */
public class TrackService extends AbstractService {

    /**
     * Delete multiple tracks from db by ids
     * @param ids list of tracks id
     * @return result of operation
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public boolean deleteTracks(List<Integer> ids) throws ServiceException {
        try{
            TrackDao trackDao = daoFactory.getTrackDao();

            boolean result = true;
            for (Integer id: ids){
                Track track = trackDao.findById(id);

                FileUploadUtility.deleteFile(track.getPath());

                result &= trackDao.delete(id);
            }

            return result;
        } catch (DaoException e){
            throw new ServiceException(e);
        }

    }


    /**
     * Finds list of tracks by project that contains that tracks
     * @param projectId project id
     * @return list of tracks matching conditions
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public List<Track> findByProject(int projectId) throws ServiceException {
        try {
            return daoFactory.getTrackDao().findByProject(projectId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Creates new track entity
     * @param track track entity
     * @return result of operation
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public boolean create(Track track) throws ServiceException {
        try {
            return daoFactory.getTrackDao().create(track);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
