package com.epam.crowdmusic.service;

import com.epam.crowdmusic.dao.BannedUserDao;
import com.epam.crowdmusic.dao.ProjectDao;
import com.epam.crowdmusic.dao.RewardDao;
import com.epam.crowdmusic.dao.UserDao;
import com.epam.crowdmusic.dto.BannedUser;
import com.epam.crowdmusic.dto.Reward;
import com.epam.crowdmusic.dto.Role;
import com.epam.crowdmusic.dto.User;
import com.epam.crowdmusic.dao.DaoException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides facade for accessing info about User entity.
 */
public class UserService extends AbstractService {


    /**
     * Finds all users in user table
     * @return list of all users
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public List<User> findAll() throws ServiceException {
        try {
            return daoFactory.getUserDao().findAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Links two user accounts between each other
     * @param currentUserId first user entity id
     * @param anotherUserId second user entity id
     * @return operation result
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public boolean linkUsers(int currentUserId, int anotherUserId) throws
            ServiceException {
        try {
            UserDao userDao = daoFactory.getUserDao();

            return userDao.linkUser(currentUserId, anotherUserId)
                    && userDao.linkUser(anotherUserId, currentUserId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }

    }


    /**
     * Finds all users that have artist role
     * @return list of artists
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public List<User> findArtists() throws ServiceException {
        try {
            List<User> users = daoFactory.getUserDao().findAll();
            List<User> artists = new ArrayList<>();
            for (User user : users) {
                if (user.getRole() == Role.ARTIST) {
                    artists.add(user);
                }
            }
            return artists;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }

    }

    /**
     * Finds user by its name
     * @param name user name
     * @return user that match condition
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public User findUserByName(String name) throws ServiceException {
        try {
            RewardDao rewardDao = daoFactory.getRewardDao();
            ProjectDao projectDao = daoFactory.getProjectDao();
            UserDao userDao = daoFactory.getUserDao();

            User user = userDao.findByName(name);
            if (user.getLinkedUser() != null) {
                user.setLinkedUser(userDao.findById(user.getLinkedUser()
                        .getId()));
            }

            user.setProjects(projectDao.findByUser(user.getId()));

            List<Reward> rewards = rewardDao.findByUser(user.getId());
            for (Reward reward : rewards) {
                reward.setUsers(userDao.findByReward(reward.getId()));
                reward.setProject(projectDao.findById(reward.getProject()
                        .getId()));
            }
            user.setRewards(rewards);
            return user;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }

    }


    /**
     * Finds user by name and password
     * @param name user name
     * @param password hashed user password
     * @return user that matches conditions
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public User findByNameAndPassword(String name, String password) throws
            ServiceException {
        try{
            UserDao userDao = daoFactory.getUserDao();
            BannedUserDao bannedUserDao = daoFactory.getBannedUserDao();
            User user = userDao.findByNameAndPassword(name, password);

            BannedUser bannedUser = bannedUserDao.findByUser(user.getId());


            if (bannedUser != null) {
                if (bannedUser.getExpirationDateTime().isAfter(LocalDateTime.now
                        ())) {
                    bannedUserDao.delete(bannedUser.getId());
                } else {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern
                            ("yyyy-MM-dd HH:mm:ss");

                    throw new DaoException("Sorry, you are banned by " +
                            "administration. " +
                            "Reason: " + bannedUser.getBanReason() +
                            " You will be unbanned on "
                            + bannedUser.getExpirationDateTime().format(formatter));
                }

            }

            if (user.getLinkedUser() != null) {
                user.setLinkedUser(userDao.findById(user.getLinkedUser().getId()));
            }

            return user;
        } catch (DaoException e){
            throw new ServiceException(e);
        }

    }

    /**
     * Updates user entity in table
     * @param user user entity
     * @return operation result
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public boolean update(User user) throws ServiceException {
        try {
            return daoFactory.getUserDao().update(user);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Creates user entity in table
     * @param user user entity
     * @return operation result
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public boolean create(User user) throws ServiceException {
        try {
            return daoFactory.getUserDao().create(user);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds user by id
     * @param id user id
     * @return user entity that matches condition
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public User findById(int id) throws ServiceException {
        try {
            return daoFactory.getUserDao().findById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds list of users by project that contains that users.
     * @param projectId project id
     * @return list of users that matches condition
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public List<User> findByProject(int projectId) throws ServiceException {
        try {
            return daoFactory.getUserDao().findByProject(projectId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }


}
