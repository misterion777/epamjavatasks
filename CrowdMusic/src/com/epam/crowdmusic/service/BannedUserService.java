package com.epam.crowdmusic.service;

import com.epam.crowdmusic.dao.BannedUserDao;
import com.epam.crowdmusic.dto.BannedUser;
import com.epam.crowdmusic.dao.DaoException;


/**
 * Provides facade for accessing info about BannedUser entity.
 */
public class BannedUserService extends AbstractService{

    /**
     * Deletes row in banned user by id
     * @param bannedUserId id of banned user entity
     * @return result of operation
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public boolean delete(int bannedUserId) throws ServiceException {

        try {
            BannedUserDao trackDao = daoFactory.getBannedUserDao();
            return trackDao.delete(bannedUserId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Creates row in banned user table
     * @param bannedUser banned user entity
     * @return result of operation
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public boolean create(BannedUser bannedUser) throws ServiceException {
        try {
            return daoFactory.getBannedUserDao().create(bannedUser);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

}
