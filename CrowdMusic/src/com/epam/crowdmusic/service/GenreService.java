package com.epam.crowdmusic.service;

import com.epam.crowdmusic.dto.Genre;
import com.epam.crowdmusic.dao.DaoException;

import java.util.List;

/**
 * Provides facade for accessing info about Genre entity.
 */
public class GenreService extends AbstractService {

    /**
     * Finds all genres from database
     * @return list of all genres
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public List<Genre> findAll() throws ServiceException {

        try {
            return daoFactory.getGenreDao().findAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }

    }


    /**
     * Finds genres by project id
     * @param projectId project id by which genres is searched
     * @return list of genres contained in project
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public List<Genre> findByProject(int projectId) throws ServiceException {

        try {
            return daoFactory.getGenreDao().findByProject(projectId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }

    }
}
