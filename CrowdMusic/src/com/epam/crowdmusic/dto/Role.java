package com.epam.crowdmusic.dto;

/**
 * Possible roles of users.
 */
public enum Role {
    /**
     * Administration of a web-site.
     */
    ADMIN,

    /**
     * Fan that can donate to projects.
     */
    FAN,

    /**
     * Artist that can create projects.
     */
    ARTIST
}
