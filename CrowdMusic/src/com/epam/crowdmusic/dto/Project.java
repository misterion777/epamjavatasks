package com.epam.crowdmusic.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


/**
 * Project DTO.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Project extends Entity {

    /**
     * Project name.
     */
    private String name;

    /**
     * Project description.
     */
    private String description;

    /**
     * Project creation date.
     */
    private LocalDate creationDate;

    /**
     * Path to project cover image.
     */
    private String picturePath;

    /**
     * List of users that are authors of this project.
     */
    private List<User> users = new ArrayList<>();

    /**
     * List of genres to which this project belong.
     */
    private List<Genre> genres = new ArrayList<>();

}
