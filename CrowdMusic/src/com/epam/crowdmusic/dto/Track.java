package com.epam.crowdmusic.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * Track DTO.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Track extends Entity {
    /**
     * Project to which this track belongs.
     */
    private Project project;

    /**
     * Track name.
     */
    private String name;

    /**
     * Path to an audio file.
     */
    private String path;

}
