package com.epam.crowdmusic.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Genre DTO.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Genre extends Entity {
    /**
     * Genre name.
     */
    private String name;
    /**
     * Parent genre object.
     */
    private Genre parentGenre;
    /**
     * List of projects that belong to this genre.
     */
    private List<Project> projects;
}
