package com.epam.crowdmusic.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Reward DTO.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Reward extends Entity {

    /**
     * Project which this reward belong to.
     */
    private Project project;

    /**
     * Reward name.
     * */
    private String name;

    /**
     * Reward description.
     */
    private String description;

    /**
     * Reward cost.
     */
    private float cost;

    /**
     * List of users that bought that reward.
     */
    private List<User> users;

}
