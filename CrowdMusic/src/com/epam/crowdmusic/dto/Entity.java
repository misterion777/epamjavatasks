package com.epam.crowdmusic.dto;

import lombok.Data;

/**
 * Base DTO.
 */
@Data
public abstract class Entity {
    /**
     * Unique identifier.
     */
    private int id;

}
