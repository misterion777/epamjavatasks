package com.epam.crowdmusic.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;


/**
 * Banned user DTO.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BannedUser extends Entity {
    /**
     * User that have been banned.
     */
    private User user;

    /**
     * DateTime when user will no longer be banned.
     */
    private LocalDateTime expirationDateTime;

    /**
     * Reason for which user was banned.
     */
    private String banReason;

}
