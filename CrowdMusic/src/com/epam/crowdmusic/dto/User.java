package com.epam.crowdmusic.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;


/**
 * User DTO.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class User extends Entity {
    /**
     * User name.
     */
    private String name;

    /**
     * User email.
     */
    private String email;

    /**
     * User password.
     */
    private String password;

    /**
     * User role.
     */
    private Role role;

    /**
     * User credit card number.
     */
    private String creditCardNumber;

    /**
     * User location.
     */
    private String location;

    /**
     * User short biography.
     */
    private String bio;

    /**
     * Path to image file.
     */
    private String picturePath;

    /**
     * Links to social networks separated by space.
     */
    private String social;

    /**
     * User that is linked to this user.
     */
    private User linkedUser;

    /**
     * List of projects that were created by this user.
     */
    private List<Project> projects = new ArrayList<>();

    /**
     * List of rewards that were bought by this user.
     */
    private List<Reward> rewards = new ArrayList<>();
}
