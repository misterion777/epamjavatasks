-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: CrowdMusicDB
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique identificator of the table',
  `username` varchar(80) NOT NULL COMMENT 'name, visible for other users',
  `email` varchar(50) NOT NULL COMMENT 'user email',
  `password` varchar(32) DEFAULT NULL,
  `role` enum('ADMIN','FAN','ARTIST') NOT NULL COMMENT 'role for specifying user activity',
  `credit_card_number` varchar(16) DEFAULT NULL,
  `bio` varchar(1000) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `social` varchar(200) DEFAULT NULL,
  `picture_path` varchar(255) DEFAULT NULL,
  `linked_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `username_2` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='User of the service, can be of the three roles: admin, fan, artist. Each role specifies the activity of the user.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Guthrie Govan','govan@mail.ru','4F56095AAC976EB59A3333FB66A495B0','ARTIST',NULL,'True master of the guitar. Looking for peace and cosmic tranquility in life. Trying to create new album, that will blow everyone\'s mind. Just wait.','London, UK','GuthrieGovanOfficial/   ','/images/Guthrie Govan/govan.jpg',NULL),(2,'Kitsune^2','kitsune@gmail.com','832A4C18FC9C6CAEF9F8CC11ED1AFB6E','ARTIST',NULL,'Nice, catchy and acidy edm composer!','Unknown place, unknown galaxy','   ','/images/Kitsune^2/a3486576328_10.jpg',NULL),(3,'Hammett','kirk@gmail.com','C8C0A99230ACC8D9A430CB43E6E453FD','ARTIST',NULL,'American musician, lead guitarist and a contributing songwriter for the heavy metal bands!','San Fransisco, US','   ','/images/Hammet/Kirk-Hammett.jpg',NULL),(4,'Ulrich','ulrich@tut.by','989459D84D4C610351B4E588DE5E8E70','ARTIST',NULL,'Musician, songwriter, actor, and record producer. Best known as the drummer and co-founder of the American heavy metal band Crowdallica! ','San Fransisco, US','   ','/images/Ulrich/ulrich.jpg',NULL),(5,'AntonyFantano','fantano@bk.ru','56C9D60F2EC775D13130F5115276DD10','FAN',NULL,'The internet\'s busiest music nerd: music critic, video producer, and journalist. My website and YouTube channel review a variety of music genres, including hip hop, rock, metal, indie, electronic music, folk and experimental.','Unknown place, unknown galaxy','   ','/images/AntonyFantano/eMWzSJoOXaKJbQf-800x450-noPad.jpg',NULL),(6,'IlyaL','Ilyalas@gmail.com','F66DD6A0790590E90ABCD291244E94D0','FAN',NULL,'Just a guy from small east-europian country. Loves jazz, rock, all kinds of progressive, some hip-hop. Here to find some new great projects that will inspire me for my own work!','Minsk, Belarus','ilya.lasy   misterion401','/images/IlyaL/SAUUZqUYpOY.jpg',NULL),(7,'CrowdMusician','ilyalas@gmail.com','574BF934C801603F9F128139D9C75C85','ADMIN',NULL,'Rules the Galaxy, the Universe, the Crowd Music','In a Galaxy far, far away.','   ','/images/CrowdMusician/lifeinspacejpeg.jpeg',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-19  1:22:06
