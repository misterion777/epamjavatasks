-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: CrowdMusicDB
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'nique identificator of the table',
  `name` varchar(45) NOT NULL COMMENT 'name of a genre',
  `parent_genre_id` int(11) DEFAULT NULL COMMENT 'id of a wider ancestor genre.',
  PRIMARY KEY (`id`),
  KEY `parent_genre_id_idx` (`parent_genre_id`),
  CONSTRAINT `parent_genre_id` FOREIGN KEY (`parent_genre_id`) REFERENCES `genre` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COMMENT='Musical genre. Tree-like structure to show the variaty of genres.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` VALUES (1,'Electronic',NULL),(2,'Rock',NULL),(3,'Metal',NULL),(4,'Hip-hop/Rap',NULL),(5,'Folk',NULL),(6,'Jazz',NULL),(7,'Pop',NULL),(8,'Funk',NULL),(9,'Punk',NULL),(10,'Classical',NULL),(11,'Blues',NULL),(12,'World',NULL),(13,'Ambient',NULL),(14,'R&B/Soul',NULL),(15,'Reggae',NULL),(16,'Country',NULL),(17,'Rock & Roll',2),(18,'Garage Rock',17),(19,'Rockabilly',17),(20,'Surf Rock',17),(21,'Indie Rock',2),(22,'Progressive Rock',2),(23,'Post-rock',2),(24,'Psychedelic rock',2),(25,'Hard Rock',2),(26,'Instrumental Rock',2),(27,'Math Rock',2),(28,'Black Metal',3),(29,'Death Metal',3),(30,'Thrash Metal',3),(31,'Progressive Metal',3),(32,'Sludge Metal',3),(33,'Heavy Metal',3),(34,'Doom Metal',3),(35,'Hardcore',3),(36,'Grindcore',35),(37,'Post-hardcore',35),(38,'Metalcore',35),(39,'Deathcore',35),(40,'Downtempo',1),(41,'Techno',1),(42,'Electro',1),(43,'Dubstep',1),(44,'IDM',1),(45,'Drum & Bass',1),(46,'Trance',1),(47,'Glitch',1),(48,'Chiptune',1),(49,'EDM',1),(50,'House',1),(51,'Witch house',50),(52,'G-house',50),(53,'Synthwave',1),(54,'Chillwave',53),(55,'Vaporwave',53);
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-19  1:22:06
