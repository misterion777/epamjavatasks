-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: CrowdMusicDB
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_has_reward`
--

DROP TABLE IF EXISTS `user_has_reward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_has_reward` (
  `user_id` int(11) NOT NULL,
  `reward_id` int(11) NOT NULL,
  `reward_review` varchar(1000) DEFAULT NULL COMMENT 'Optional review after acquiring the reward',
  `reward_rating` enum('1','2','3','4','5') DEFAULT NULL COMMENT 'Optional rating to reward.',
  PRIMARY KEY (`user_id`,`reward_id`),
  KEY `fk_user_has_reward_reward1_idx` (`reward_id`),
  KEY `fk_user_has_reward_user1_idx` (`user_id`),
  CONSTRAINT `fk_user_has_reward_reward1` FOREIGN KEY (`reward_id`) REFERENCES `reward` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_reward_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Many-to-many reward user relationship.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_has_reward`
--

LOCK TABLES `user_has_reward` WRITE;
/*!40000 ALTER TABLE `user_has_reward` DISABLE KEYS */;
INSERT INTO `user_has_reward` VALUES (5,1,NULL,NULL),(5,2,NULL,NULL),(5,5,NULL,NULL),(6,1,NULL,NULL),(6,6,NULL,NULL);
/*!40000 ALTER TABLE `user_has_reward` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-19  1:22:06
