-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: CrowdMusicDB
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `track`
--

DROP TABLE IF EXISTS `track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `track` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'nique identificator of the table',
  `project_id` int(11) NOT NULL COMMENT 'id of a project that contains track. ',
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`,`project_id`),
  KEY `fk_track_project1_idx` (`project_id`),
  CONSTRAINT `fk_track_project1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='Musical composition that is a part of any project.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `track`
--

LOCK TABLES `track` WRITE;
/*!40000 ALTER TABLE `track` DISABLE KEYS */;
INSERT INTO `track` VALUES (1,1,'01 - Waves','/audio/Erotic Cakes/01 - Waves.mp3'),(2,1,'02 - Erotic Cakes','/audio/Erotic Cakes/02 - Erotic Cakes.mp3'),(3,1,'03 - Wonderful Slippery Thing','/audio/Erotic Cakes/03 - Wonderful Slippery Thing.mp3'),(4,1,'04 - Ner Ner','/audio/Erotic Cakes/04 - Ner Ner.mp3'),(5,1,'05 - Fives','/audio/Erotic Cakes/05 - Fives.mp3'),(6,1,'06 - Uncle Skunk','/audio/Erotic Cakes/06 - Uncle Skunk.mp3'),(7,1,'07 - Sevens','/audio/Erotic Cakes/07 - Sevens.mp3'),(8,1,'08 - Eric','/audio/Erotic Cakes/08 - Eric.mp3'),(9,1,'09 - Slidey Boy','/audio/Erotic Cakes/09 - Slidey Boy.mp3'),(10,1,'10 - Rhode Island Shred','/audio/Erotic Cakes/10 - Rhode Island Shred.mp3'),(11,2,'01 Intro','/audio/Squaredance/01 Intro.mp3'),(12,2,'02 Squaredance','/audio/Squaredance/02 Squaredance.mp3'),(13,2,'03 Avast Your Ass','/audio/Squaredance/03 Avast Your Ass.mp3'),(14,2,'04 Rock My Emotions','/audio/Squaredance/04 Rock My Emotions.mp3'),(15,2,'05 PKMN','/audio/Squaredance/05 PKMN.mp3'),(16,2,'06 Goodnight','/audio/Squaredance/06 Goodnight.mp3'),(17,2,'07 U R Everything (K2 MAGfest 7 Live Mix)','/audio/Squaredance/07 U R Everything (K2 MAGfest 7 Live Mix).mp3'),(18,2,'08 Taotao Bounce','/audio/Squaredance/08 Taotao Bounce.mp3'),(19,2,'09 Rainbow Tylenol','/audio/Squaredance/09 Rainbow Tylenol.mp3'),(20,2,'10 Dumpster of Chicken','/audio/Squaredance/10 Dumpster of Chicken.mp3'),(21,3,'METALLICA  - METALLICA - 01 - ENTAR SANDMAN','/audio/Rides \'Em Black/METALLICA  - METALLICA - 01 - ENTAR SANDMAN.mp3'),(22,3,'METALLICA  - METALLICA - 02 - SAD BUT TRUE','/audio/Rides \'Em Black/METALLICA  - METALLICA - 02 - SAD BUT TRUE.mp3'),(23,3,'METALLICA  - METALLICA - 03 - HOLIER THAN THOU','/audio/Rides \'Em Black/METALLICA  - METALLICA - 03 - HOLIER THAN THOU.mp3'),(24,3,'METALLICA  - METALLICA - 04 - THE UNFORGIVEN','/audio/Rides \'Em Black/METALLICA  - METALLICA - 04 - THE UNFORGIVEN.mp3'),(25,3,'METALLICA  - METALLICA - 05 - WHEREVER I MAY ROAM','/audio/Rides \'Em Black/METALLICA  - METALLICA - 05 - WHEREVER I MAY ROAM.mp3'),(26,3,'METALLICA  - METALLICA - 06 - DON\'T TREAD ON ME','/audio/Rides \'Em Black/METALLICA  - METALLICA - 06 - DON\'T TREAD ON ME.mp3'),(27,3,'METALLICA  - METALLICA - 07 - THROUGH THE NEVER','/audio/Rides \'Em Black/METALLICA  - METALLICA - 07 - THROUGH THE NEVER.mp3'),(28,3,'METALLICA  - METALLICA - 08 - NOTHING ELSE MATTERS','/audio/Rides \'Em Black/METALLICA  - METALLICA - 08 - NOTHING ELSE MATTERS.mp3'),(29,3,'METALLICA  - METALLICA - 09 - OF WOLFAND MAN','/audio/Rides \'Em Black/METALLICA  - METALLICA - 09 - OF WOLFAND MAN.mp3'),(30,3,'METALLICA  - METALLICA - 10 - THE GOD THAT FAILED','/audio/Rides \'Em Black/METALLICA  - METALLICA - 10 - THE GOD THAT FAILED.mp3');
/*!40000 ALTER TABLE `track` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-19  1:22:06
