<%--
  Created by IntelliJ IDEA.
  User: misterion
  Date: 9/10/18
  Time: 2:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="t" %>
<t:html title="Log In" script="login.js">

    <main role="main" >
        <div class="jumbotron vertical-center">
            <div class="container">
                <h1 class="mb-5">Sign in</h1>
                <form class="form-signin" action="/authenticate">
                    <div class="form-group">
                        <input name="name" type="name" class="form-control" placeholder="Your Name *" value=""/>
                    </div>

                    <div class="form-group">
                        <input name="psw" type="password" class="form-control" placeholder="Password *" value=""/>
                    </div>

                    <div id="error" class="alert alert-danger" role="alert" style="display: none">
                    </div>
                    <button type="submit" class="btn btn-primary">Log in</button>
                </form>

            </div>

        </div>


    </main>

</t:html>


