<%--
  Created by IntelliJ IDEA.
  User: misterion
  Date: 9/10/18
  Time: 2:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="t" %>

<t:html title="${requestScope.user.name}" script="user.js">
    <c:set var="socials" scope="page" value="${fn:split(requestScope.user.social, ' ')}"/>

    <div class="jumbotron">
        <div class="container">
            <div class="row mt-auto">
                <div class="col-md-9"> <!-- Nick and Location -->
                    <h2 style="font-size:38px"><strong>${requestScope.user.name}</strong></h2>
                    <h5 style="color:#3AAA64">
                        <small>${requestScope.user.location}</small>
                    </h5>
                </div>
                <div class="col-md-3">
                    <c:if test="${sessionScope.user.role == 'ADMIN' and sessionScope.user.name != requestScope.user.name}">
                            <button style="float: right;" class="btn btn-danger" data-toggle="modal"
                                    data-target="#banModal">Justice!
                            </button>
                    </c:if>
                    <c:if test="${sessionScope.user.name == requestScope.user.name}">
                        <c:set var="anotherRole" scope="page" value="${sessionScope.user.role == 'FAN' ? 'artist' :'fan'}"/>

                        <div class="button" style="float:right;">
                            <button class="btn btn-outline-success btn-sm" data-toggle="modal"
                                    data-target="#editProfileModal">Edit profile
                            </button>

                            <c:choose>
                                <c:when test="${sessionScope.user.linkedUser == null}">
                                    <button class="btn btn-outline-success btn-sm" data-toggle="modal"
                                            data-target="#addLinkModal">
                                        Link to ${anotherRole} profile
                                    </button>
                                </c:when>
                                <c:otherwise>
                                    <button class="btn btn-outline-success btn-sm" onclick="switchUser()">
                                        Switch to ${sessionScope.user.linkedUser.name}
                                    </button>
                                </c:otherwise>
                            </c:choose>
                        </div>

                    </c:if>
                </div>
            </div>


            <!-- Ban Modal -->
            <div class="modal fade" id="banModal" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Ban user</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="banForm" action="/ban">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="banTime">Select ban time</label>
                                    <select name="banTime" class="form-control" id="banTime" >
                                        <option value="1h" selected>1 hour</option>
                                        <option value="12h">12 hours</option>
                                        <option value="1d">1 day</option>
                                        <option value="2d">3 days</option>
                                        <option value="7d">7 days</option>
                                        <option value="1m">1 month</option>
                                        <option value="6m">6 months</option>
                                        <option value="1y">1 year</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="reason">Write reason</label>
                                    <textarea id="reason" name="reason" class="form-control"
                                              placeholder="Clear reason of ban"></textarea>
                                </div>

                                <input name="userId" value="${requestScope.user.id}" type="hidden">

                                <div class="alert alert-danger" role="alert"
                                     style="display: none">
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button id="submitBanButton" type="submit" class="btn btn-primary">Ban!
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <!-- Link Modal -->
            <div class="modal fade" id="addLinkModal" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Login to another account</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="linkForm" action="/link">
                            <div class="modal-body">
                                <p>If you have any ${anotherRole} profile, you can login to link
                                    your accounts</p>
                                <div class="form-group">
                                    <label for="linkName">Name</label>
                                    <input id="linkName" name="linkName" type="text" class="form-control"
                                           placeholder="${anotherRole} account name" autocomplete="off"/>
                                </div>
                                <div class="form-group">
                                    <label for="linkPsw">Password</label>
                                    <input id="linkPsw" name="linkPsw" type="password" class="form-control"
                                           placeholder="${anotherRole} account password"/>
                                </div>

                                <div class="alert alert-danger" role="alert"
                                     style="display: none">
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button id="submitLinkButton" type="submit" class="btn btn-primary">Link
                                    accounts
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Edit Modal -->
            <div class="modal fade" id="editProfileModal" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Edit profile</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="editForm" action="/edituser" enctype="multipart/form-data">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="imageInput">Profile image</label>
                                    <div class="custom-file">
                                        <label id="imageLabel" class="custom-file-label" for="imageInput">Choose
                                            image</label>
                                        <input class="custom-file-input" id="imageInput" type="file"
                                               name="imageInput" accept="image/*">
                                    </div>
                                    <div id='imagePreview'></div>
                                </div>


                                <div class="form-group">
                                    <label for="email">Your email</label>
                                    <input id="email" name="email" type="email" class="form-control"
                                           placeholder="Your email"
                                           value="${requestScope.user.email}"
                                           autocomplete="off"/>
                                </div>


                                <div class="form-group">
                                    <label for="name">Your name</label>
                                    <input id="name" name="name" type="text" class="form-control"
                                           placeholder="Your name"
                                           value="${requestScope.user.name}"
                                           autocomplete="off"/>
                                </div>
                                <div class="form-group">
                                    <label for="location">Location</label>
                                    <input id="location" name="location" type="text" class="form-control"
                                           placeholder="Location"
                                           value="${requestScope.user.location}"
                                           autocomplete="off"/>
                                </div>

                                <div class="form-group">
                                    <label for="bio">About you</label>
                                    <textarea id="bio" name="bio" class="form-control"
                                              placeholder="Short biography">${requestScope.user.bio}</textarea>
                                </div>


                                <div class="form-group">
                                    <label>Links to your profiles</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">https://www.facebook.com/</div>
                                        </div>
                                        <input name="social" type="text" class="form-control" value="${socials[0]}">
                                    </div>

                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">https://www.spotify.com/</div>
                                        </div>
                                        <input name="social" type="text" class="form-control" value="${socials[1]}">
                                    </div>

                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">https://www.instagram.com/</div>
                                        </div>
                                        <input name="social" type="text" class="form-control" value="${socials[2]}">
                                    </div>

                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">https://vk.com/</div>
                                        </div>
                                        <input name="social" type="text" class="form-control" value="${socials[3]}">
                                    </div>
                                </div>


                                <div id="error" class="alert alert-danger" role="alert"
                                     style="display: none">
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button id="submitButton" type="submit" class="btn btn-primary" disabled>Save changes
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="row" style="margin-top:20px;">
                <div class="col-md-4"> <!-- Image -->
                    <span class="img-wrapper">
                        <img class="rounded-circle img-cover" src="${requestScope.user.picturePath}" onerror="if
                        (this.src != '/resource/img/user_default.jpg') this.src = '/resource/img/user_default.jpg';">
                    </span>
                </div>
                <div class="col-md-5"> <!-- Bio-->
                    <p style="font-size: 19px;" >${requestScope.user.bio}
                    </p>
                </div>
                <div class="col-md-3 text-center"> <!-- Social -->
                    <div class="social-icons" style="padding-top:18px">
                        <c:if test="${not empty socials[0]}">
                            <a href="https://facebook.com/${socials[0]}">
                                <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x" style="color:#3AAA64"></i>
                                <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </c:if>

                        <c:if test="${not empty socials[1]}">
                            <a href="https://instagram.com/${socials[1]}">
                                <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x" style="color:#3AAA64"></i>
                                <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </c:if>
                        <c:if test="${not empty socials[2]}">
                            <a href="https://spotify.com/${socials[2]}">
                                <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x" style="color:#3AAA64"></i>
                                <i class="fa fa-spotify fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </c:if>
                        <c:if test="${not empty socials[3]}">
                            <a href="https://vk.com/${socials[3]}">
                                <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x" style="color:#3AAA64"></i>
                                <i class="fa fa-vk fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </c:if>
                    </div>
                    <div class="button" style="padding-top:18px">
                        <a href="mailto:${requestScope.user.email}" class="btn btn-outline-success btn-block">Send
                            Email</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <c:choose>
            <c:when test="${requestScope.user.role == 'FAN'}">
                <h3>Bought rewards:</h3>
                <div class="row">
                    <c:forEach items="${requestScope.user.rewards}" var="reward">
                        <div class="col-md-4">
                            <div class="card mb-4 shadow-sm">
                                <div class="card-body"><h3 class="card-text">${reward.project.name}: ${reward.name}</h3>
                                    <p class="card-text">${reward.description}</p>
                                    <p>
                                        <small>Crowd: ${reward.users.size()} happy people</small>
                                    </p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group"><a href="/project/${reward.project.name}"
                                                                  class="btn btn-sm btn-outline-secondary">View
                                            project</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>

            </c:when>
            <c:when test="${requestScope.user.role == 'ARTIST'}">
                <h3><strong>Featured projects:</strong></h3>
                <div class="row">
                    <c:if test="${sessionScope.user.name == requestScope.user.name}">
                        <div class="col-md-4 ">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="button">
                                        <a href="/registerproject" class="btn btn-outline-primary">Add new project!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:forEach items="${requestScope.user.projects}" var="project">
                        <div class="col-md-4">
                            <div class="card mb-4 shadow-sm">
                                <img class="card-img-top" style="height: 225px; width: 100%; display: block;"
                                     src="${project.picturePath}"
                                     onerror="if (this.src != '/resource/img/thumbnail.jpg') this.src = '/resource/img/thumbnail.jpg';">
                                <div class="card-body"><h3 class="card-text">${project.name}</h3>
                                    <p class="card-text">${project.description}</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group"><a href="/project/${project.name}"
                                                                  class="btn btn-sm btn-outline-secondary">View</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>

                </div>
            </c:when>
        </c:choose>
    </div>
</t:html>


