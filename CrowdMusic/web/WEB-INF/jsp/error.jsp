<%@page isErrorPage="true" language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="t"%>
<t:html title="Error">

    <main role="main" >
        <div class="jumbotron vertical-center">
            <div class="container">
                <c:choose>
                    <c:when test="${not empty error}">
                        <h2 >${error}</h2>
                    </c:when>
                    <c:when test="${not empty pageContext.errorData.requestURI}">
                        <h2>Requested page ${pageContext.errorData.requestURI} can't be
                            found on server</h2>
                    </c:when>
                    <c:otherwise>Unexpected error</c:otherwise>
                </c:choose>
                <c:url value="/" var="mainUrl"/>
                <a class="btn btn-primary" href="${mainUrl}">Home</a>
            </div>
        </div>
    </main>
</t:html>