<%--
  Created by IntelliJ IDEA.
  User: misterion
  Date: 9/10/18
  Time: 2:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="t" %>
<t:html title="Sign up" script="signup.js">

    <main role="main">
        <div class="jumbotron vertical-center">

            <div class="container register">
                <h1 class="mb-5">Sign up</h1>
                <form action="/register">
                    <div class="register-form">
                        <div class="form-group btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-secondary active">
                                <input type="radio" name="role" value="fan" id="fan" autocomplete="off" checked/>Fan
                            </label>
                            <label class="btn btn-secondary">
                                <input type="radio" name="role" value="artist" id="artist" autocomplete="off"/>Artist
                            </label>
                        </div>

                        <div class="form-group">
                            <input name="username" type="text" class="form-control" placeholder="Nickname *" value=""/>
                        </div>

                        <div class="form-group">
                            <input name="email" type="email" class="form-control" placeholder="Your Email *" value=""/>
                        </div>
                        <div class="form-group">
                            <input name="psw" type="password" class="form-control" placeholder="Password *" value=""/>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Confirm Password *" value=""/>
                        </div>
                        <div id="error" class="alert alert-danger" role="alert" style="display: none">
                        </div>
                        <button type="submit" class="btn btn-primary">Sign up</button>

                    </div>
                </form>
            </div>
        </div>

    </main>


</t:html>


