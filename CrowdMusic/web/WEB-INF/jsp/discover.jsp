<%--
  Created by IntelliJ IDEA.
  User: misterion
  Date: 9/10/18
  Time: 2:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="t" %>

<t:html title="Discover" script="discover.js">

    <main role="main">

        <section class="jumbotron text-center">
            <div class="container-fluid">
                <h1 class="jumbotron-heading">Genres</h1>
                <p class="lead text-muted">Tree-like structure of genres that will help you to find projects</p>
                <p class="text-muted"><i>Click on genre to see its subgenres</i></p>
                <div id="genresContainer">

                </div>

            </div>
        </section>


        <div class="album py-5 bg-light">
            <div class="container">
                <nav aria-label="...">
                    <ul class="pagination justify-content-end">
                        <li id="previous" class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1">Previous</a>
                        </li>

                        <li id="next" class="page-item">
                            <a class="page-link" href="#">Next</a>
                        </li>
                    </ul>
                </nav>
                <div id="projectsContainer" class="row">

                </div>

            </div>

        </div>
    </main>
</t:html>


