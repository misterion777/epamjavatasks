<%--
  Created by IntelliJ IDEA.
  User: misterion
  Date: 9/10/18
  Time: 2:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="t" %>

<t:html title="${requestScope.project.name}" script="project.js">

    <c:set var="userHasProject" scope="page" value="false"/>
    <c:forEach items="${requestScope.project.users}" var="user">
        <c:if test="${user.name == sessionScope.user.name}">
            <c:set var="project" scope="session" value="${requestScope.project}"/>
            <c:set var="userHasProject" scope="page" value="true"/>
        </c:if>
    </c:forEach>


    <main role="main">
        <div class="jumbotron">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2 id="username" style="font-size:38px"><strong>${requestScope.project.name}</strong></h2>
                            <c:if test="${userHasProject eq true}">
                                <div class="col">
                                    <div class="button" style="float:right;">
                                        <button class="btn btn-outline-primary btn-sm" data-toggle="modal"
                                                data-target="#editProjectModal">Edit Project</button>
                                    </div>
                                </div>
                                <!-- Edit Modal -->
                                <div class="modal fade" id="editProjectModal" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Edit project</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form id="editForm" action="/editproject" enctype="multipart/form-data">
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="imageInput">Project image</label>
                                                        <div class="custom-file">
                                                            <label id="imageLabel" class="custom-file-label" for="imageInput">Choose image</label>
                                                            <input class="custom-file-input" id="imageInput" type="file"
                                                                   name="imageInput" accept="image/*">
                                                        </div>
                                                        <div id='imagePreview'></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="editName">Project name</label>
                                                        <input id="editName" name="name" type="text"
                                                               class="form-control"
                                                               placeholder="Project name"
                                                               value="${requestScope.project.name}"
                                                               autocomplete="off"/>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="editDescription">Description</label>
                                                        <textarea id="editDescription" name="description"
                                                                  class="form-control"
                                                                  placeholder="Description">${requestScope.project.description}</textarea>
                                                    </div>

                                                    <label for="tracksInput">Tracks </label>
                                                    <div class="custom-file">
                                                        <label class="custom-file-label" for="tracksInput">Choose audio files
                                                            (up to 10)</label>
                                                        <input class="custom-file-input" id="tracksInput" type="file"
                                                               name="tracksInput" accept="audio/*" multiple>
                                                    </div>
                                                    <div class="mt-2" id='oldTracksDisplay'></div>
                                                    <div class="mt-2" id='newTracksDisplay'></div>

                                                    <div class="form-group">
                                                        <label for="genres">Change genres </label>
                                                        <div>
                                                            <input placeholder="Start typing..." name="editGenres"
                                                                   id="genres"
                                                                   type="text"
                                                                   autocomplete="off"/>
                                                        </div>
                                                    </div>

                                                    <%--<c:if test="${requestScope.project.users.get(0).name == sessionScope.user.name}">--%>
                                                    <div class="form-group">
                                                        <label for="artists">Invite artists </label>
                                                        <div id="artistsWrapper">
                                                            <select style="width:400px;" class="form-control"
                                                                    name="artists"
                                                                     id="artists" autocomplete="off" multiple></select>
                                                        </div>
                                                    </div>
                                                    <%--</c:if>--%>



                                                    <div id="modalError" class="alert alert-danger" role="alert"
                                                         style="display: none">
                                                    </div>


                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button id="submitButton" type="submit" class="btn btn-primary" disabled>Save changes
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </c:if>

                        <h4 id="artistList">By
                            <c:forEach items="${requestScope.project.users}" var="user" varStatus="loop">
                                <a userId="${user.id}" href="/user/${user.name}">${user.name}</a>${!loop.last ? ',' :''}
                            </c:forEach>
                        </h4>
                        <div class="audio-player card">
                            <div class="card">
                                <div class="card-body">
                                    <h3 class="card-title col text-center text-muted">${requestScope.project.name}
                                        playlist</h3>
                                    <div class="row align-items-center mb-3">
                                        <i id="play-button" class="material-icons play-pause text-primary mr-2"
                                           aria-hidden="true">play_circle_outline</i>
                                        <i id="pause-button"
                                           class="material-icons play-pause d-none text-primary mr-2"
                                           aria-hidden="true">pause_circle_outline</i>
                                        <i id="next-button" class="material-icons text-primary ml-2 mr-3"
                                           aria-hidden="true">skip_next</i>
                                    </div>
                                    <div class="p-0 m-0" id="now-playing">
                                        <p class="font-italic mb-0">Now Playing:</p>
                                        <p class="lead" id="title"></p>
                                    </div>
                                    <div class="progress-bar progress">
                                    </div>
                                </div>
                                <ul id="tracksContainer" class="playlist list-group list-group-flush">
                                    <c:forEach items="${requestScope.tracks}" var="track">
                                        <li audio_id="${track.id}" audio_url="${track.path}"
                                            class="list-group-item playlist-item">
                                                ${track.name}
                                        </li>
                                    </c:forEach>
                                </ul>
                            </div>
                            <audio id="audio-player" class="d-none" src="" controls="controls"></audio>
                        </div>

                        <div class="card mt-3">
                            <div class="card-body">
                                <h2>Description:</h2>
                                <p>${requestScope.project.description}
                                </p>
                            </div>
                        </div>
                        <div class="card mt-3">
                            <div id="genresContainer" class="card-body text-center">
                                <c:forEach items="${requestScope.project.genres}" var="genre">
                                    <button id="${genre.id}" parentGenre="${genre.parentGenre.id}"
                                            class="btn btn-primary ml-1 my-2">${genre.name}</button>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <a href="#">
                            <img class="rounded-circle" src="${requestScope.project.picturePath}" onerror="if (this.src != '/resource/img/thumbnail.jpg') this.src = '/resource/img/thumbnail.jpg';" alt="Project cover"
                                 style="width:500px;height:500px">
                        </a>
                        <div class="mt-3">
                            <h3 class="text-center">Support</h3>

                            <c:if test="${userHasProject eq true}">
                                <div class="col">
                                    <div class="card">
                                        <div class="card-body text-center">
                                            <button class="btn btn-outline-primary" data-toggle="modal"
                                                    data-target="#newRewardModal">Add
                                                new
                                                reward!</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- Reward Modal -->
                                <div class="modal fade" id="newRewardModal" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Add reward</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form id="rewardForm" action="/addreward">
                                                    <div class="form-group">
                                                        <label for="name">Reward name *</label>
                                                        <input id="name" name="name" type="text" class="form-control"
                                                               placeholder="Reward name"
                                                               value=""
                                                               autocomplete="off"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="rewardCostInput">Cost *</label>
                                                        <input id="rewardCostInput" name="cost" type="number"
                                                               class="form-control"
                                                               placeholder="Cost of reward"
                                                               value="0.99"
                                                               step="0.01"
                                                               autocomplete="off"/>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="description">Description *</label>
                                                        <textarea id="description" name="description" class="form-control"
                                                                  placeholder="Description of your reward"></textarea>
                                                    </div>

                                                    <div  id="error" class="alert alert-danger" role="alert"
                                                          style="display: none">
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" form="rewardForm" class="btn btn-primary">Add
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:if>

                            <div class="col">
                                <c:forEach items="${requestScope.rewards}" var="reward">
                                    <div class="card">
                                        <div class="card-body">
                                            <form action="/buyreward">
                                                <h5 class="card-title"><strong>${reward.name}
                                                    <span id="rewardCost"> ${reward.cost} $</span></strong>
                                                </h5>
                                                <p id="rewardDescription" class="card-text">${reward.description}</p>
                                                <p>
                                                    <small>Crowd: ${reward.users.size()} happy people</small>
                                                </p>
                                                <input type="hidden" name="rewardId" value="${reward.id}">

                                                <div  class="alert alert-danger mt-1 mb-3" role="alert"
                                                      style="display: none">
                                                </div>

                                                <c:if test="${userHasProject eq false}">
                                                    <button class="btn btn-primary">Join the crowd!</button>
                                                </c:if>

                                            </form>
                                        </div>
                                    </div>
                                </c:forEach>

                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </main>


</t:html>




