<%--
  Created by IntelliJ IDEA.
  User: misterion
  Date: 9/10/18
  Time: 2:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="t" %>
<t:html title="Create project" script="registerproject.js">


    <main role="main">
        <div class="jumbotron vertical-center">

            <div class="container-fluid register">
                <h1 class="mb-5">Create project</h1>
                <form action="/createproject" enctype="multipart/form-data" method="post">
                    <div class="register-form">
                        <div class="row">

                            <div class="col">
                                <div class="form-group">
                                    <label for="name">Name *</label>
                                    <input id="name" name="name" type="text" class="form-control"
                                           placeholder="Project name"
                                           value=""
                                           autocomplete="off"/>
                                </div>

                                <div class="form-group">
                                    <label for="description">Description *</label>
                                    <textarea id="description" name="description" class="form-control"
                                               placeholder="Short description of your project"></textarea>
                                </div>


                                <div class="form-group">
                                    <label for="genres">Genres (up to 5) *</label>
                                    <div>
                                        <input placeholder="Start typing..." name="genres" id="genres"
                                               type="text"
                                               autocomplete="off"/>
                                    </div>

                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label for="imageInput">Project image *</label>
                                    <div class="custom-file">
                                        <label id="imageLabel" class="custom-file-label" for="imageInput">Choose image</label>
                                        <input class="custom-file-input" id="imageInput" required="required" type="file"

                                               name="imageInput" accept="image/*">
                                    </div>
                                    <div id='imagePreview'></div>
                                </div>

                                <div class="form-group">
                                    <label for="tracksInput">Tracks *</label>
                                    <div class="custom-file">
                                        <label class="custom-file-label" for="tracksInput">Choose audio files
                                            (up to 10)</label>
                                        <input class="custom-file-input" id="tracksInput" required="required" type="file"
                                               name="tracksInput" accept="audio/*" multiple>
                                    </div>
                                    <div id='fileListDisplay'></div>
                                </div>

                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label for="rewardPolicySelect">Select reward policy</label>
                                    <select class="form-control" id="rewardPolicySelect" >
                                        <option value="free">All tracks are available without donation</option>
                                        <option value="partial">First 30 seconds of each track are available
                                            without donation</option>
                                        <option value="first">Only first track is available without
                                            donation</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div id="free" class="options">
                                        <em>
                                            As default your tracks will be available for listening without any donation.
                                            You'll be able to add more donate options on your project page.
                                        </em>
                                    </div>
                                    <div id="partial" class="options" style="display: none;">
                                        <label for="rewardCostInput">Cost of your first reward</label>
                                        <input id="rewardCostInput" name="cost" type="number" class="form-control"
                                               placeholder="Cost of basic reward"
                                               value="0.99"
                                               step="0.01"
                                               autocomplete="off"/>

                                        <p class="mt-3" style="margin-bottom: -1px;">
                                            <em>First reward sample:</em></p>
                                        <%--REWARD TEMPLATE--%>
                                        <div class="card">
                                            <div class="card-body">
                                                <h5 class="card-title">
                                                    <strong>Buy digital version
                                                        <span id="rewardCost">0.99 $</span></strong>
                                                </h5>
                                                <p id="rewardDescription" class="card-text">Join our crowd! You will
                                                    get full access to all
                                                    tracks on this project page (online and download)!</p>
                                                <p><small>Crowd: 10 happy people</small></p>
                                                <button class="btn btn-primary" disabled>Join the crowd!</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div  class="alert alert-danger mt-1 mb-3" role="alert"
                                      style="display: none">
                                </div>
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>

    </main>
</t:html>


