<%--
  Created by IntelliJ IDEA.
  User: misterion
  Date: 9/10/18
  Time: 1:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="t"%>

<t:html title="Home">

    <main role="main">
        <div class="container">
            <div class="jumbotron mt-5">
                <h2 class="jumbotron-heading"> What is CrowdMusic?</h2>
                <div class="lead text-muted">
                    <p> CrowdMusic is a small raising star in the world of musical crowdfunding.
                        This project is created to make newcomer musicians from all
                        over the world share their work, projects, ideas.  </p>
                    <p>
                        Feel free to share every your little musical idea, even if you think it's total garbage.
                        Even in the most awful composition there are plenty of interesting ideas
                        that can be improved with the help of other's advice.
                        People may like your work, support you with some kind words and maybe even money!
                    </p>
                    <p>
                        The most special thing about CrowdMusic is that there are musicians
                        in our team who can give some professional advice to their less experienced colleagues.
                    </p>
                </div>
            </div>
        </div>
            <%--<p>--%>
                <%--Have some musical idea, but don't know how to start?--%>
            <%--</p>--%>
            <%--<p>--%>
                <%--Check out our brand new guide on how to create nice music projects!--%>
            <%--</p>--%>
            <%--<br>--%>
            <%--<a href="tutorial.html" class="button">--%>
                <%--Watch tutorial--%>
            <%--</a>--%>

        <%----%>
        <%----%>
            <%--<p>--%>
                <%--Composed some interesting piece, but don't know wether people like it or not?--%>
            <%--</p>--%>
            <%--<p>--%>
                <%--Don't be afraid and keep your publishing. Share it!--%>
            <%--</p>--%>
            <%--<br>--%>
            <%--<a href="register.html" class="button">--%>
                <%--Start a project--%>
            <%--</a>--%>
        <%----%>
    </main>
</t:html>





