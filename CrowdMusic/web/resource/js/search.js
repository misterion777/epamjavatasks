let searchInput = $("#searchInput");
let searchData;
let currentSearchValue = {};

$("#search").submit(function () {

    let $form = $(this);

    $.post($form.attr("action"), currentSearchValue, function (response) {

        if (response.forward) {
            window.location = response.forward;
        }
        else {

            if (response.error) {
                searchInput.addClass("is-invalid");
                searchInput.tooltip({
                    title: response.error,
                    trigger: 'manual'
                });
                searchInput.tooltip('show');

            }
        }
    });

    event.preventDefault();
});


$(searchInput).focus(function () {

    searchInput.tooltip('hide');
    searchInput.removeClass("is-invalid");
    if (!searchData) {
        let engine = new Bloodhound({

            prefetch: {
                url: "/getsearchdata",
                filter: function (response) {
                    return response.searchData;
                }
            },

            identify: function (obj) {
                return obj.name + obj.id
            },
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,

        });

        $('#search #searchInput').typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            },
            {
                name: 'searchData',
                displayKey: "name",
                source: engine
            }
        ).on('typeahead:selected', function (event, data) {
            currentSearchValue = {id: data.id, type: getObjectType(data)};
        });
        searchData = true;
    }

});


function getObjectType(object) {
    if (object.email) {
        return "user"
    }
    else {
        return "project"
    }
}