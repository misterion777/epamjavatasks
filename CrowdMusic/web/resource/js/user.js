let imageInput = document.getElementById('imageInput');
let imagePreview = $("#imagePreview");
let submitButton = $("#submitButton");
let $inputs = $("#editForm").find("input, textarea");

$(document).on("change", "#imageInput", function (event) {

    let img = `<img id="preview" class="img-responsive" src="${URL.createObjectURL(imageInput.files[0])}" height="125px">`;
    $("#imageLabel").text(imageInput.files[0].name);
    imagePreview.html(img);

});

$(document).on('keyup, change','input, textarea', function(){
    for(let i = 0; i<$inputs.length;i++){
        if ($inputs[i].defaultValue !== $inputs[i].value){
            submitButton.prop('disabled', false);
            return;
        }
    }
    submitButton.prop('disabled', true);
});


function switchUser(){
    $.get("/switch", function (response) {

        if (response.forward) {
            window.location = response.forward;
        }
        else {
            let $error = $form.find(".alert");
            if (response.error) {
                $error.show();
                $error.text(response.error);
            } else {
                $error.text("Unknown error!");
            }
        }
    });

}

function getFormData($form){

    let unindexed_array = $form.serializeArray();
    let indexed_array = {};

    $.map(unindexed_array, function(n, i){
        if (n['name'] === "social" && typeof indexed_array[n['name']] !== "undefined"){
            indexed_array[n['name']] += " " + n['value'];
        }
        else {
            indexed_array[n['name']] = n['value'];
        }
    });

    return indexed_array;
}

$("form[id!='editForm']").submit(function(){
    let $form = $(this);

    event.preventDefault();


    $.post($form.attr("action"), $form.serialize(), function (response) {

        if (response.forward) {
            window.location = response.forward;
        }
        else {
            let $error = $form.find(".alert");
            if (response.error) {
                $error.show();
                $error.text(response.error);
            } else {
                $error.text("Unknown error!");
            }
        }
    });

});


$(document).on("submit", "#editForm", function (event) {
    let $form = $(this);

    let formData = new FormData();

    if (imageInput.files.length !== 0){
        formData.append(imageInput.files[0].name, imageInput.files[0]);
    }

    let data = getFormData($form);

    formData.append("data", JSON.stringify(data));

    $.ajax({
        type: "POST",
        url: $form.attr("action"),
        contentType: false,
        processData: false,
        data: formData,
        success: function (response) {
            console.log("got response!");
            if (response.forward) {
                window.location = response.forward;
            }
            else {
                if (response.error) {
                    let $error = $form.find(".alert");
                    $error.show();
                    $error.text(response.error);
                }
            }
        }
    });

    event.preventDefault();
});