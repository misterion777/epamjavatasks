$(document).on("submit", "form", function (event) {
    let $form = $(this);

    event.preventDefault();

    $.post($form.attr("action"), $form.serialize(), function (response) {

        if (response.forward) {
            window.location = response.forward;
        }
        else {
            let $error = $("#error");
            if (response.error) {
                $error.show();
                $error.text(response.error);
            } else {
                $error.text("Unknown error!");
            }
        }
    });

});