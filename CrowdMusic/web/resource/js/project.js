let alreadyLoaded = false;
let fileList = [];
let fileInput = document.getElementById('tracksInput');
let imageInput = document.getElementById('imageInput');
let oldTracksDisplay = $("#oldTracksDisplay");
let newTracksDisplay = $("#newTracksDisplay");
let imagePreview = $("#imagePreview");
let $inputs = $("#editForm").find("input, textarea, select");
let submitButton = $("#submitButton");

let genresInput = $('#genres');
let genres = {};
let artists = {};
let deletedTracks = [];
let currentArtists = [];
let artistsSelect = $("#artists")
let artistsDefault;




$("#editProjectModal").on("show.bs.modal", function (e) {
    if (!alreadyLoaded) {
        genres = new Bloodhound({
            prefetch: {
                url: "/getgenres",
                filter: function (response) {
                    return response.genres;
                }
            },
            identify: function (obj) {
                return obj.id
            },
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,

        });
        genres.initialize();

        $.get("/getartists", function (response) {

            artists = $.map(response.artists, function(obj) {
                return { id: obj.id, text: obj.name };
            });

            artistsSelect.select2({

                width: 'resolve',
                placeholder: 'Select an artist',
                data: artists,
                allowClear: true
            });

            artistsSelect.on("select2:select", function (e) { console.log(e); });
            let attributes = [];
            $("#artistList").children().each(function(){
                let elem = $(this);
                attributes.push(elem.attr("userId"));
            });
            artistsSelect.val(attributes).trigger('change');

            artistsDefault = artistsSelect.val();
            submitButton.prop('disabled', true);
        });

        genresInput = $('#genres');
        genresInput.tagsinput({
            maxTags: 5,
            freeInput: false,
            allowDuplicates: false,
            onTagExists: function (item, $tag) {
                $tag.hide().fadeIn();
            },
            itemValue: 'id',
            itemText: 'name',
            typeaheadjs: {
                name: 'engine',
                displayKey: 'name',
                valueKey: 'id',
                source: genres.ttAdapter()
            }
        });

        alreadyLoaded = true;
    }
    else {
        genresInput.tagsinput('removeAll');
        genresInput.tagsinput({
            maxTags: 5,
            freeInput: false,
            allowDuplicates: false,
            onTagExists: function (item, $tag) {
                $tag.hide().fadeIn();
            },
            itemValue: 'id',
            itemText: 'name',
            typeaheadjs: {
                name: 'engine',
                displayKey: 'name',
                valueKey: 'id',
                source: genres.ttAdapter()
            }
        });

        artistsSelect.val(null).trigger('change');
        let attributes = [];
        $("#artistList").children().each(function(){
            let elem = $(this);
            attributes.push(elem.attr("userId"));
        });
        artistsSelect.val(attributes).trigger('change');
    }

    $("#genresContainer").children().each(function () {
        let $button = $(this);
        genresInput.tagsinput('add', {
            name: $button.text(),
            parentGenre: {id: parseInt($button.attr('parentGenre'))},
            id: parseInt($button.attr('id'))
        });
    });
    $(genresInput).attr("value", genresInput.val());

    oldTracksDisplay.empty();
    $("#tracksContainer").children().each(function () {
        let p = `<p track_id ="${$(this).attr("audio_id")}">${$(this).text()}
        <button style="float:right" onclick="deleteTrack(event)" class="btn btn-primary btn-sm">x</button></p>`;
        oldTracksDisplay.append(p);
    });

    submitButton.prop('disabled', true);
});

$(document).on('keyup, change', 'input, textarea, select', function () {
    for (let i = 0; i < $inputs.length; i++) {
        if (typeof $inputs[i].defaultValue === 'undefined'){
            if (JSON.stringify(artistsDefault) !== JSON.stringify(artistsSelect.val())){
                submitButton.prop('disabled', false);
                return;
            }
        } else if ($inputs[i].defaultValue !== $inputs[i].value) {
            submitButton.prop('disabled', false);
            return;
        }
    }
    submitButton.prop('disabled', true);
});


$(document).on("change", "#imageInput", function (event) {
    let img = `<img id="preview" class="img-responsive" src="${URL.createObjectURL(imageInput.files[0])}" height="125px">`;
    $("#imageLabel").text(imageInput.files[0].name);
    imagePreview.html(img);
});


$(document).on("change", "#tracksInput", function (event) {

    newTracksDisplay.empty();
    fileList = [];
    for (let i = 0; i < fileInput.files.length; i++) {
        let currentFile = fileInput.files[i];
        fileList.push(currentFile);
        let p = `<p>${currentFile.name}</p>`;
        newTracksDisplay.append(p);

    }
});


function deleteTrack(event) {
    let $p = $(event.target).parent();
    deletedTracks.push($($p).attr("track_id"));
    $p.remove();
    submitButton.prop('disabled', false);
}


$(document).on("submit", '#editForm', function (event) {
    let $form = $(this);

    let formData = new FormData();

    formData.append("name", $form.find("#editName").val());
    formData.append("description", $form.find("#editDescription").val());
    formData.append("genres", JSON.stringify({genres: genresInput.tagsinput('items')}));
    formData.append("deletedTracks", JSON.stringify(deletedTracks));
    formData.append("artists", JSON.stringify(artistsSelect.val()));

    if (imageInput.files.length !== 0) {
        formData.append(imageInput.files[0].name, imageInput.files[0]);
    }

    fileList.forEach(function (file) {
        formData.append(file.name, file);
    });

    $.ajax({
        type: "POST",
        url: $form.attr("action"),
        contentType: false,
        processData: false,
        data: formData,
        success: function (response) {
            console.log("got response!");
            if (response.forward) {
                window.location = response.forward;
            }
            else {
                if (response.error) {
                    let $error = $form.find(".alert");
                    $error.show();
                    $error.text(response.error);
                }
            }
        }
    });

    event.preventDefault();
});


$(document).on("submit", 'form[id!="editForm"]', function (event) {
    let $form = $(this);

    $.post($form.attr("action"), $form.serialize(), function (response) {

        if (response.forward) {

            window.location = response.forward;
        }
        else {
            if (response.error) {
                let $error = $form.find(".alert");
                $error.show();
                $error.text(response.error);
            }
        }
    });

    event.preventDefault();
});

$(document).ready(function () {
    // __________VARIABLES_____________
    var playPause = $('.play-pause');
    var playButton = $('#play-button');
    var pauseButton = $('#pause-button');
    var nextButton = $('#next-button');
    var audioPlayer = $('#audio-player');
    var thumbnail = $('#thumbnail');
    var slider = $('.progress-bar').slider({
        step: 0.01
    });
    var playlistItems = $('.playlist-item');
    playlistItems.first().addClass("active");
    var thisSong = $('.active.playlist-item');

    var title = $('#title');
    var nowPlaying = $('#now-playing');


    // __________FUNCTIONS_____________
    // Toggle UI buttons
    function togglePlayPause() {
        playButton.toggleClass("d-none");
        pauseButton.toggleClass('d-none');
    };

    // Loads the title information into the #title element
    function trackName() {
        title.html(thisSong.html());
    }

    // Load next track and updates the UI.
    // Use dest = 'this' as call(this, 'this') to go the clicked items
    // Use nextTrack() to go to the next playlist item. If the current
    // playlist item is the last, it will go to the first.
    function nextTrack(dest) {
        // Toggle Play/Pause button if paused
        if (playButton.is(':visible')) {
            togglePlayPause();
        }
        // Update UI
        thisSong.toggleClass('active');
        // Set default value for dest
        if (dest !== 'this') {
            dest = 'next';
        }
        // Update active playlist item NEXT
        if (dest === 'next') {
            if (thisSong.is(':last-child')) {
                thisSong = $('.playlist-item').first();
            } else {
                thisSong = thisSong.next();
            }
        }
        // Update active playlist item THIS
        else if (dest === 'this') {
            thisSong = $(this);
        }
        // Update src and UI
        audioPlayer['0'].pause();
        thisSong.toggleClass('active');
        trackName();
        audioPlayer.attr('src', thisSong.attr('audio_url'));
        thumbnail.attr('src', thisSong.attr('img_url'));
        // Reset time and play
        audioPlayer['0'].currentTime = 0;
        audioPlayer['0'].play();
    }

    // Load the Play Pause Button functionality
    playPause.click(function () {
        if (audioPlayer['0'].paused === true) {
            audioPlayer['0'].play();
            togglePlayPause();
            trackName();
            nowPlaying.slideDown();
            //Change icon to Pause
        }
        else {
            audioPlayer['0'].pause();
            togglePlayPause();
        }
    });

    //  SLIDER
    $('.progress-bar').slider();
    // moves the slider on progress
    audioPlayer.on('timeupdate', function () {
        // $('.progress').attr("aria-value-now", this.currentTime / this.duration * 100);
        // $('.progress-bar').css("width", this.currentTime / this.duration * 100);

        slider.slider("option", "value", this.currentTime / this.duration * 100);
    });
    //  changes playback time on slider input
    slider.on('slidestop', function (event, ui) {
        var newTime = ui.value / 100 * audioPlayer['0'].duration;
        audioPlayer['0'].currentTime = newTime;
    })

    //   PLAYLIST
    audioPlayer.attr("src", thisSong.attr('audio_url'));
    // NEXT button functionality
    nextButton.click(function () {
        audioPlayer['0'].pause();
        nextTrack();
    });

    // Click on playlist items functionality
    playlistItems.click(function () {
        nowPlaying.slideDown();
        if (audioPlayer['0'].paused) {
            togglePlayPause();
        }
        audioPlayer['0'].pause();
        // call the global nextTrack('this') function from within the current function scope
        nextTrack.call(this, 'this');
    });

    // Go to next track when track has ended

    audioPlayer.bind('ended', function () {
        // done playing
        nextTrack();
    });
});
