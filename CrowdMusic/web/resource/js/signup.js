
$(document).on("submit", "form", function (event) {

    let $form = $(this);
    let $error = $("#error");
    let email = $("input[name='email']", $form).val();

    if (validateEmail(email)) {
        console.log("valid email!");
        $.post($form.attr("action"), $form.serialize(), function (response) {

            if (response.forward) {
                window.location = response.forward;
            }
            else {
                $error.show();
                if (response.error) {
                    $error.text(response.error);
                } else {
                    $error.text("Unknown error!");
                }
            }
        });
    }
    else {
        $(".alert").show();
        $error.text("Invalid email!");
    }
    event.preventDefault(); // Important! Prevents submitting the form.
});


function validateEmail($email) {
    let emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
}