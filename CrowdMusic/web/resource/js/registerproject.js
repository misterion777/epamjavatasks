let jsonGenres;
let genresInput;
let fileList = [];
let fileInput = document.getElementById('tracksInput');
let fileListDisplay = $("#fileListDisplay");

let imageInput = document.getElementById('imageInput');
let imagePreview = $("#imagePreview");

let rewardPolicySelect = $("#rewardPolicySelect");

$(document).on("change", "#tracksInput", function (event) {

    fileListDisplay.empty();
    fileList = [];
    for (let i = 0; i < fileInput.files.length; i++) {
        let currentFile = fileInput.files[i];
        fileList.push(currentFile);
        let p = '<p>' + (i + 1) + ': ' + currentFile.name + '</p>';
        fileListDisplay.append(p);

    }
});


$(document).on("change", "#imageInput", function (event) {
    let img = `<img id="preview" class="img-responsive" src="${URL.createObjectURL(imageInput.files[0])}" height="125px">`;
    $("#imageLabel").text(imageInput.files[0].name);
    imagePreview.html(img);
});

$(document).on("change", "#rewardCostInput", function (event) {

    $("#rewardCost").text($(this).val() + " $");
});

$(document).on("change", "#rewardPolicySelect", function (event) {
    let value = $(this).val();
    $('.options').hide();

    if (value == "free") {
        $("#free").show();
    }
    else {
        $("#partial").show();
    }

});


$(document).ready(function () {

    let engine = new Bloodhound({
        prefetch:{
          url: "/getgenres",
          filter: function(response) {
              return response.genres;
          }
        },
        identify: function (obj) {
            return obj.id
        },
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,

    });
    engine.initialize();

    genresInput = $('#genres');
    genresInput.tagsinput({
        maxTags: 5,
        freeInput: false,
        allowDuplicates: false,
        onTagExists: function (item, $tag) {
            $tag.hide().fadeIn();
        },
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'engine',
            displayKey: 'name',
            valueKey: 'id',
            source: engine.ttAdapter()
        }
    });
});


$(document).on("submit", "form", function (event) {
    let $form = $(this);

    let formData = new FormData();

    let projectData = {
        name: $("#name").val(),
        description:  $("#description").val(),
        genres: genresInput.tagsinput('items')
    };
    formData.append("projectData", JSON.stringify(projectData));
    formData.append(imageInput.files[0].name, imageInput.files[0]);
    fileList.forEach(function (file) {
        formData.append(file.name, file);
    });

    if (rewardPolicySelect.val() !== "free"){
        let rewardData = {
          name: "Buy digital version",
          description: $("#rewardDescription").text(),
          cost: $("#rewardCostInput").val()
        };
        formData.append("rewardData", JSON.stringify(rewardData));
    }

    $.ajax({
        type: "POST",
        url: $form.attr("action"),
        contentType: false,
        processData: false,
        data: formData,
        success: function (response) {
            console.log("got response!");
            if (response.forward) {
                window.location = response.forward;
            }
            else {
                if (response.error) {
                    let $error = $form.find(".alert");
                    $error.show();
                    $error.text(response.error);
                }
            }
        }
    });


    event.preventDefault(); // Important! Prevents submitting the form.
});