let genresContainer = $("#genresContainer");
let currentGenre = 0;
let genres;
$(document).ready(function () {


    $.get("/builddiscover", function (response) {
        if (response.forward) {
            window.location = response.forward;
        }
        else {
            genres = response.genres;
            if (genres) {
                genresContainer.append(`<button class="btn btn-warning ml-1 my-2" onclick="changeGenre(event, 0)">All</button>`);
                for (let i = 0; i < genres.length; i++) {
                    if (genres[i].parentGenre.id === 0) {
                        let genre = `<button class="btn btn-warning ml-1 my-2" onclick="changeGenre(event, ${genres[i].id})">${genres[i].name}</button>`;
                        genresContainer.append(genre);
                    }
                }
            }
            buildProjects(response);
            if (response.error) {
                var $error = $("#error");
                $error.hidden = false;
                $error.text(response.error);

            }
        }
    });
});

function changeGenre(event, genreId) {
    $(event.target).siblings().removeClass("active");
    $(event.target).addClass("active");

    let currentContainer = $(event.target).parent();
    currentContainer.find("div").remove();
    if (genreId !== 0){

        let newContainer = $("<div></div>");
        currentContainer.append(newContainer);

        for (let i = 0; i < genres.length; i++) {
            if (genres[i].parentGenre.id === genreId) {
                let genre = `<button class="btn btn-warning ml-1 my-2" onclick="changeGenre(event, ${genres[i].id})">${genres[i].name}</button>`;
                newContainer.append(genre);
            }
        }
    }

    let stateObj = {genreId: currentGenre};
    window.history.pushState(stateObj, "", "/discover?genreId="+currentGenre);

    currentGenre = genreId;
    getProjects(event, genreId, 1, true);
}

window.onpopstate = function(e){
    if(e.state){
        changeGenre(event, e.state.genreId);
    }
};


function getProjects(event, genreId, page, updatePageAmount) {
    $("li").removeClass("active");
    let button = event.target;

    $(button).parent().addClass("active");

    let data = {
        genreId: genreId,
        page: page,
        updatePageAmount: updatePageAmount
    };
    $.get("/getprojects", data, buildProjects);
}

function buildProjects(response) {
    let projects = response.projects;
    let pageAmount = response.pageAmount;

    if (response.forward){
        window.location = response.forward;
    }

    if (pageAmount) {
        let next = $("#next");
        next.prevUntil("#previous").remove();
        for (let i = 0; i < pageAmount; i++) {
            let li = `<li class="page-item">
                        <button class="page-link" onclick="getProjects(event, ${currentGenre},${i + 1}, false)">${i + 1}</button>
                      </li>`;
            next.before(li);
        }
    }

    if (projects) {
        let projectsContainer = $("#projectsContainer");
        projectsContainer.empty();
        for (let i = 0; i < projects.length; i++) {
            let projectCard = `<div class="col-md-4">                        <div class="card mb-4 shadow-sm">                            <img class="card-img-top" style="height: 225px; width: 100%; display: block;"
                              src="${projects[i].picturePath}" onerror="if (this.src != '/resource/img/thumbnail.jpg') this.src = '/resource/img/thumbnail.jpg';">                          <div class="card-body">                                <h3 class="card-text">${projects[i].name}</h3>                                <p class="card-text">${projects[i].description}</p>                                <div class="d-flex justify-content-between align-items-center">                                    <div class="btn-group">                                        <a href="/project/${projects[i].name}" class="btn btn-sm btn-outline-secondary">View</a>                                    </div>                                </div>                            </div>                        </div>                    </div>                    `;
            projectsContainer.append(projectCard);
        }
    }
}

