import com.epam.kickstart_b.validators.CubeValidator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Contains test for cube input data validator.
 */
public class CubeValidatorTest {
    /**
     * Validator object that is being tested.
     */
    private CubeValidator validator;

    /**
     * Initializes validator object.
     */
    @BeforeClass
    public void initialize() {
        validator = new CubeValidator();
    }

    /**
     * Provides data for getValidatedDoubles method.
     *
     * @return array of input data for testing getValidatedDoubles.
     */
    @DataProvider(name = "validatorDataProvider")
    public Object[][] validatorDataProvider() {

        HashMap<Integer, String> inputHashMap = new HashMap<>();
        inputHashMap.put(1, "name1 +25.5 0 0 0");
        inputHashMap.put(5, "hello 1.2 0.567 12 56");
        inputHashMap.put(6, "world -25.5 120 0.23 0.1");
        inputHashMap.put(7, "tsok 0.000 3445 43.2 123");
        inputHashMap.put(9, "cube 567 -4.2 0 23");

        ArrayList<Object[]> arrayList = new ArrayList<>();
        arrayList.add(new Object[]{"name1", new double[]{25.5, 0, 0, 0}});
        arrayList.add(new Object[]{"hello", new double[]{1.2, 0.567, 12, 56}});
        arrayList.add(new Object[]{"cube", new double[]{567, -4.2, 0, 23}});

        return new Object[][]{
                {inputHashMap, arrayList}
        };
    }

    /**
     * Test for creating doubles from already parsed correct lines.
     *
     * @param inputHashMap   hash map with correct lines
     * @param expectedResult expected result of validator.getValidatedDoubles
     *                      method
     */
    @Test(dataProvider = "validatorDataProvider")
    public void testGetValidatedDoubles(HashMap<Integer, String>
            inputHashMap, ArrayList<Object[]> expectedResult) {
        ArrayList<Object[]> result = validator.getValidatedDoubles
                (inputHashMap);
        Assert.assertEquals(expectedResult, result);

    }
}
