import com.epam.kickstart_b.actions.CubePointsAction;
import com.epam.kickstart_b.creator.CubeCreator;
import com.epam.kickstart_b.entities.CubeEntity;
import com.epam.kickstart_b.entities.Point3DEntity;
import com.epam.kickstart_b.exceptions.InvalidEdgeLengthException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Contain test for cube points action logic
 */
@Test
public class CubePointsActionTest {

    /**
     * CubePointsAction object that is being tested.
     */
    private CubePointsAction cubePointsAction;
    /**
     * Cube creator object that is used to create objects for testing.
     */
    private CubeCreator cubeCreator;

    /**
     * Initialize objects needed for test.
     */
    @BeforeClass
    public void initialize() {

        cubeCreator = new CubeCreator();
    }

    /**
     * Test for calculating points of the cube.
     */
    @Test
    public void testCalculatePoints() {

        Object[] inputData = new Object[]{"name1", new double[]{10, 5, 6, 7}};

        Point3DEntity[] expectedResult = new Point3DEntity[]{
                new Point3DEntity(5, 6, 7),
                new Point3DEntity(5, 16, 7),
                new Point3DEntity(15, 16, 7),
                new Point3DEntity(15, 6, 7),
                new Point3DEntity(5, 6, 17),
                new Point3DEntity(5, 16, 17),
                new Point3DEntity(15, 16, 17),
                new Point3DEntity(15, 6, 17),
        };

        CubeEntity entity = null;
        try {
            entity = cubeCreator.createCubeEntity(inputData);
        } catch (InvalidEdgeLengthException e) {
            Assert.fail(e.getMessage());
        }
        cubePointsAction = new CubePointsAction(entity);
        Point3DEntity[] result = cubePointsAction.calculatePoints();

        Assert.assertEquals(expectedResult, result);
    }


}
