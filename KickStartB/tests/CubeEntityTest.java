import com.epam.kickstart_b.entities.CubeEntity;
import com.epam.kickstart_b.entities.Point3DEntity;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Contain tests for cube entity equals() and hashcode().
 */
public class CubeEntityTest {


    /**
     * Provides input data for equals and hashcode tests.
     *
     * @return array of input data for testing equals and hashcode.
     */
    @DataProvider(name = "inputDataProvider")
    public Object[][] inputDataProvider() {
        return new Object[][]{
                {
                        new double[][]{
                                {1.5, 23, 153},
                                {1.5, 23, 153},
                                {10}
                        },
                        true
                },
                {
                        new double[][]{
                                {1.5, -23, 153},
                                {1.5, 23, 153},
                                {10}
                        },
                        false
                }
        };
    }


    /**
     * Testing equals method of a cube entity
     *
     * @param input          input values for creating cube entity object
     * @param expectedResult expected result of cube.equals method
     */
    @Test(dataProvider = "inputDataProvider")
    public void testEquals(double[][] input, boolean expectedResult) {
        CubeEntity cube1 = new CubeEntity("name1", input[2][0], new
                Point3DEntity(input[0][0], input[0][1], input[0][2]));
        CubeEntity cube2 = new CubeEntity("name1", input[2][0], new
                Point3DEntity(input[1][0], input[1][1], input[1][2]));

        Assert.assertEquals(cube1.equals(cube2), expectedResult);
    }
}

