import com.epam.kickstart_b.creator.CubeCreator;
import com.epam.kickstart_b.entities.CubeEntity;
import com.epam.kickstart_b.entities.Point3DEntity;
import com.epam.kickstart_b.exceptions.InvalidEdgeLengthException;
import org.testng.Assert;
import org.testng.annotations.*;


/**
 * Contain tests for cube creator logic.
 */
@Test
public class CubeCreatorTest {

    /**
     * Cube creator object that is used to create objects for testing.
     */
    private CubeCreator creator;

    /**
     * Initialize objects needed for test.
     */
    @BeforeClass
    public void initialize() {
        creator = new CubeCreator();
    }

    /**
     * Data provider for testing createCubeEntity method.
     *
     * @return array of input data for testing getVolume.
     */
    @DataProvider(name = "cubeCreationProvider")
    public Object[][] cubeCreationData() {
        return new Object[][]{
                {new Object[]{"name1", new double[]{15.3, 10, 5, 12.4}},
                        new CubeEntity("name1", 15.3, new Point3DEntity(10,
                                5, 12.4))},
                {new Object[]{"name2", new double[]{1, 0, 0, 0}},
                        new CubeEntity("name2", 1, new Point3DEntity(0, 0, 0))},
                {new Object[]{"name3", new double[]{123453.3235423, 12340,
                        2345.234, -1312.4}},
                        new CubeEntity("name3", 123453.3235423,
                                new Point3DEntity(12340, 2345.234, -1312.4))},
        };
    }

    /**
     * Test whether createCubeEntity throws an exception
     * when passing negative number as an edge length
     *
     * @throws InvalidEdgeLengthException negative number as an edge length
     */
    @Test(expectedExceptions = InvalidEdgeLengthException.class)
    public void testCreateCubeEntityException()
            throws InvalidEdgeLengthException {
        creator.createCubeEntity(new Object[]{"name1", new double[]{-3, 23,
                1, -3}});
    }


    /**
     * Test for creating cube entity.
     *
     * @param inputData      input data for creating cube entity object
     * @param expectedResult expected result of creator.createCubeEntity method
     */
    @Test(dataProvider = "cubeCreationProvider")
    public void testCreateCubeEntity(Object[] inputData, CubeEntity
            expectedResult) {

        CubeEntity cubeEntity = null;
        try {
            cubeEntity = creator.createCubeEntity(inputData);
        } catch (InvalidEdgeLengthException e) {
            Assert.fail(e.getMessage());
        }

        Assert.assertEquals(expectedResult, cubeEntity, expectedResult
                .toString());
    }
}
