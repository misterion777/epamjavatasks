import com.epam.kickstart_b.actions.CubeAction;
import com.epam.kickstart_b.creator.CubeCreator;
import com.epam.kickstart_b.entities.CubeEntity;
import com.epam.kickstart_b.exceptions.InvalidEdgeLengthException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Contain tests for cube action logic.
 */
@Test
public class CubeActionTest {
    /**
     * Cube action object which is being tested.
     */
    private CubeAction cubeAction;
    /**
     * Cube creator object that is used to create objects for testing.
     */
    private CubeCreator cubeCreator;

    /**
     * Constant that is used for accuracy of tests.
     */
    private static final double DELTA = 0.00001;


    /**
     * Initialize objects needed for test.
     */
    @BeforeClass
    public void initialize() {
        cubeAction = new CubeAction();
        cubeCreator = new CubeCreator();
    }

    /**
     * Data provider for testing getVolume method.
     *
     * @return array of input data for testing getVolume.
     */
    @DataProvider(name = "volumeProvider")
    public Object[][] volumeData() {
        return new Object[][]{
                {new Object[]{"name1", new double[]{15.3, 10, 5, 12.4}},
                        3581.577},
                {new Object[]{"name1", new double[]{1, 0, 0, 0}}, 1},
                {new Object[]{"name1", new double[]{123453.3235423, 12340,
                        2345.234, -1312.4}},
                        1881517919097718.0309329625372166}
        };
    }

    /**
     * Data provider for testing getAreaSurface method.
     *
     * @return array of input data for testing getAreaSurface.
     */
    @DataProvider(name = "surfaceProvider")
    public Object[][] surfaceData() {
        return new Object[][]{
                {new Object[]{"name1", new double[]{15.3, 10, 5, 12.4}},
                        1404.54},
                {new Object[]{"name2", new double[]{1, 0, 0, 0}}, 6},
                {new Object[]{"name3", new double[]{123453.3235423, 12340,
                        2345.234, -1312.4}},
                        91444338561.83882051933574}
        };
    }

    /**
     * Data provider for testing isOnBase method.
     *
     * @return array of input data for testing isOnBase.
     */
    @DataProvider(name = "baseProvider")
    public Object[][] baseData() {
        return new Object[][]{
                {new Object[]{"name1", new double[]{15.3, 10, 5, 12.4}}, false},
                {new Object[]{"name2", new double[]{1, 0, 0, 0}}, true},
                {new Object[]{"name3", new double[]{123453.3235423, 12340,
                        2345.234, -1312.4}},
                        false}
        };
    }

    /**
     * Data provider for testing getVolumeRatio method.
     *
     * @return array of input data for testing getVolumeRatio.
     */
    @DataProvider(name = "volumeRatioProvider")
    public Object[][] volumeRatioData() {
        return new Object[][]{
                {new Object[]{"name1", new double[]{5, -3, 5, 1}}, 0.66666},
                {new Object[]{"name2", new double[]{123, 135, 11, 213}}, 0},
                {new Object[]{"name3", new double[]{123453.3235423, 12340,
                        2435.234, -13332.4}}, 8.25964744099}
        };
    }


    /**
     * Test for cubeAction.getVolume method.
     *
     * @param inputData      input data for creating cube entity object
     * @param expectedResult expected result of cubeAction.getVolume method
     */
    @Test(dataProvider = "volumeProvider")
    public void testGetVolume(Object[] inputData, double expectedResult) {

        CubeEntity cubeEntity = null;
        try {
            cubeEntity = cubeCreator.createCubeEntity(inputData);
        } catch (InvalidEdgeLengthException e) {
            Assert.fail(e.getMessage());
        }

        cubeAction.setOperatedCube(cubeEntity);

        double volume = cubeAction.getVolume();

        Assert.assertEquals(expectedResult, volume, DELTA, Double.toString
                (expectedResult));
    }


    /**
     * Test for cubeAction.getAreaSurface method.
     *
     * @param inputData      input data for creating cube entity object
     * @param expectedResult expected result of cubeAction.getAreaSurface method
     */
    @Test(dataProvider = "surfaceProvider")
    public void testGetSurface(Object[] inputData, double expectedResult) {

        CubeEntity cubeEntity = null;
        try {
            cubeEntity = cubeCreator.createCubeEntity(inputData);
        } catch (InvalidEdgeLengthException e) {
            Assert.fail(e.getMessage());
        }

        cubeAction.setOperatedCube(cubeEntity);

        double surfaceArea = cubeAction.getSurfaceArea();

        Assert.assertEquals(expectedResult, surfaceArea, DELTA, Double
                .toString(expectedResult));
    }


    /**
     * Test for cubeAction.isBaseOnPlane method.
     *
     * @param inputData      input data for creating cube entity object
     * @param expectedResult expected result of cubeAction.isBaseOnPlane method
     */
    @Test(dataProvider = "baseProvider")
    public void testIsBaseOnPlane(Object[] inputData, boolean expectedResult) {
        CubeEntity cubeEntity = null;
        try {
            cubeEntity = cubeCreator.createCubeEntity(inputData);
        } catch (InvalidEdgeLengthException e) {
            Assert.fail(e.getMessage());
        }

        cubeAction.setOperatedCube(cubeEntity);

        boolean value = cubeAction.isBaseOnPlane();

        Assert.assertEquals(expectedResult, value, Boolean.toString
                (expectedResult));
    }

    /**
     * Test for cubeAction.getVolumeRatio
     *
     * @param inputData      input data for creating cube entity object
     * @param expectedResult expected result of cubeAction.getVolumeRatio method
     */
    @Test(dataProvider = "volumeRatioProvider")
    public void testGetVolumeRatio(Object[] inputData, double expectedResult) {
        CubeEntity cubeEntity = null;
        try {
            cubeEntity = cubeCreator.createCubeEntity(inputData);
        } catch (InvalidEdgeLengthException e) {
            Assert.fail(e.getMessage());
        }

        cubeAction.setOperatedCube(cubeEntity);

        double value = cubeAction.getVolumeRatio();

        Assert.assertEquals(expectedResult, value, DELTA, Double.toString
                (expectedResult));
    }
}
