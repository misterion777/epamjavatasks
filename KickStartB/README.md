**KickStart Chapter B**

Add additional functionality to chapter A:

1. Add id and name to objects (change parser a little bit)
2. Create registrator class.
3. Add observer to existing objects.
4. Create repository class (use singleton) with methods of adding, removing, editing objects.
5. Create specifications for sorting and finding objects in repository.

Sun checkstyle
Java 1.8