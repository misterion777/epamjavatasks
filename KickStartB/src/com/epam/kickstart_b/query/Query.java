package com.epam.kickstart_b.query;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Interface for query to repository.
 *
 * @param <Entity>      type of objects which are stored in repository
 * @param <Registrator> type of registrators which are stored in repository
 */
public interface Query<Entity, Registrator> {
    /**
     * Specific query to repository.
     *
     * @param storage map of elements of repository
     * @return list of objects that matches the result of a specific query
     */
    ArrayList<Entity> query(HashMap<Entity, Registrator> storage);
}
