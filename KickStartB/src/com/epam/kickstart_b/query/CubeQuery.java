package com.epam.kickstart_b.query;

import com.epam.kickstart_b.entities.CubeEntity;
import com.epam.kickstart_b.registrator.CubeRegistrator;

/**
 * Query for repository that contains cube objects and registrators.
 */
public interface CubeQuery extends Query<CubeEntity, CubeRegistrator> {
}
