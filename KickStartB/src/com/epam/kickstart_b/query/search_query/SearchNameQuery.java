package com.epam.kickstart_b.query.search_query;

import com.epam.kickstart_b.entities.CubeEntity;
import com.epam.kickstart_b.query.CubeQuery;
import com.epam.kickstart_b.registrator.CubeRegistrator;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Query for searching cube objects by name.
 */
public class SearchNameQuery implements CubeQuery {

    /**
     * Name to search for.
     */
    private String name;

    /**
     * Constructor which assigns name to search for.
     * @param nameToSearch name to search for
     */
    public SearchNameQuery(final String nameToSearch) {
        this.name = nameToSearch;
    }

    /**
     * Implementation of searching cube object by name.
     * @param storage map of elements of repository
     * @return list of object with specific name
     */
    public ArrayList<CubeEntity> query(
            final HashMap<CubeEntity, CubeRegistrator> storage) {
        ArrayList<CubeEntity> result = new ArrayList<>();
        for (CubeEntity cube: storage.keySet()) {
            if (cube.getName().equals(this.name)) {
                result.add(cube);
            }
        }
        return result;
    }

}
