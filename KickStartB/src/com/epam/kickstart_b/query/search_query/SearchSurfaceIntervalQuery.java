package com.epam.kickstart_b.query.search_query;

import com.epam.kickstart_b.entities.CubeEntity;
import com.epam.kickstart_b.query.CubeQuery;
import com.epam.kickstart_b.registrator.CubeRegistrator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Query for searching cube objects whose area surface is in specific interval.
 */
public class SearchSurfaceIntervalQuery implements CubeQuery {
    /**
     * Minimal value of an area surface interval.
     */
    private double minSurface;
    /**
     * Maximal value of an area surface interval.
     */
    private double maxSurface;

    /**
     * Constructor which assigns area surface interval to search in.
     *
     * @param minimalSurface minimal value of an area surface interval
     * @param maximal        maximal value of an area surface interval
     */
    public SearchSurfaceIntervalQuery(final double minimalSurface, final double
            maximal) {
        this.minSurface = minimalSurface;
        this.maxSurface = maximal;
    }

    /**
     * Implementation of searching cube object
     * whose area surface is in specific interval.
     *
     * @param storage map of elements of repository
     * @return list of objects whose area surface is in specific interval.
     */
    public ArrayList<CubeEntity> query(
            final HashMap<CubeEntity, CubeRegistrator> storage) {
        ArrayList<CubeEntity> result = new ArrayList<>();
        for (Map.Entry<CubeEntity, CubeRegistrator> entry : storage.entrySet
                ()) {
            double areaSurface = entry.getValue().getAreaSurface();
            if (areaSurface >= minSurface && areaSurface <= maxSurface) {
                result.add(entry.getKey());
            }
        }
        return result;
    }

}
