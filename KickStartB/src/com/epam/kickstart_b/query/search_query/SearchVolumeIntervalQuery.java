package com.epam.kickstart_b.query.search_query;

import com.epam.kickstart_b.entities.CubeEntity;
import com.epam.kickstart_b.query.CubeQuery;
import com.epam.kickstart_b.registrator.CubeRegistrator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Query for searching cube objects whose volume is in specific interval.
 */
public class SearchVolumeIntervalQuery implements CubeQuery {

    /**
     * Minimal value of an volume interval.
     */
    private double minVolume;
    /**
     * Maximal value of an volume interval.
     */
    private double maxVolume;

    /**
     * Constructor which assigns volume interval to search in.
     *
     * @param minimalVolume minimal value of a volume interval
     * @param maximalVolume maximal value of a volume interval
     */
    public SearchVolumeIntervalQuery(final double minimalVolume, final double
            maximalVolume) {
        this.minVolume = minimalVolume;
        this.maxVolume = maximalVolume;
    }

    /**
     * Implementation of searching cube object
     * whose volume is in specific interval.
     *
     * @param storage map of elements of repository
     * @return list of objects whose volume is in specific interval.
     */
    public ArrayList<CubeEntity> query(
            final HashMap<CubeEntity, CubeRegistrator> storage) {
        ArrayList<CubeEntity> result = new ArrayList<>();
        for (Map.Entry<CubeEntity, CubeRegistrator> entry : storage.entrySet
                ()) {
            double volume = entry.getValue().getVolume();
            if (volume >= minVolume && volume <= maxVolume) {
                result.add(entry.getKey());
            }
        }
        return result;
    }
}
