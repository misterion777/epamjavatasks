package com.epam.kickstart_b.query.search_query;

import com.epam.kickstart_b.actions.CubeAction;
import com.epam.kickstart_b.entities.CubeEntity;
import com.epam.kickstart_b.query.CubeQuery;
import com.epam.kickstart_b.registrator.CubeRegistrator;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Query for searching cube objects whose
 * bases are on coordinate plane.
 */
public class SearchBaseOnPlaneQuery implements CubeQuery {

    /**
     * Implementation of searching cube objects whose
     * bases are on coordinate plane.
     *
     * @param storage map of elements of repository
     * @return list of objects whose
     * bases are on coordinate plane
     */
    @Override
    public ArrayList<CubeEntity> query(
            final HashMap<CubeEntity, CubeRegistrator> storage) {
        ArrayList<CubeEntity> result = new ArrayList<>();
        for (CubeEntity cube : storage.keySet()) {
            CubeAction cubeAction = new CubeAction(cube);
            if (cubeAction.isBaseOnPlane()) {
                result.add(cube);
            }
        }
        return result;
    }
}
