package com.epam.kickstart_b.query.search_query;

import com.epam.kickstart_b.actions.CubeAction;
import com.epam.kickstart_b.entities.CubeEntity;
import com.epam.kickstart_b.query.CubeQuery;
import com.epam.kickstart_b.registrator.CubeRegistrator;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Query for searching cube objects whose
 * volumes ratio after crossing by a coordinate plane is in specific interval.
 */
public class SearchVolumeRatioIntervalQuery implements CubeQuery {

    /**
     * Minimal value of an volume ratio interval.
     */
    private double minRatio;
    /**
     * Maximal value of an volume ratio interval.
     */
    private double maxRatio;

    /**
     * Constructor which assigns volume ratio interval to search in.
     *
     * @param minimalRatio minimal value of a volume ratio interval
     * @param maximalRatio maximal value of a volume ratio interval
     */
    public SearchVolumeRatioIntervalQuery(final double minimalRatio, final
    double maximalRatio) {
        this.minRatio = minimalRatio;
        this.maxRatio = maximalRatio;
    }

    /**
     * Implementation of searching cube object
     * whose volumes ratio after crossing by a coordinate plane
     * is in specific interval.
     *
     * @param storage map of elements of repository
     * @return list of objects whose volume ratio is in specific interval.
     */
    @Override
    public ArrayList<CubeEntity> query(
            final HashMap<CubeEntity, CubeRegistrator> storage) {
        ArrayList<CubeEntity> result = new ArrayList<>();
        for (CubeEntity cube : storage.keySet()) {
            CubeAction cubeAction = new CubeAction(cube);
            double ratio = cubeAction.getVolumeRatio();
            if (ratio >= minRatio && ratio <= maxRatio) {
                result.add(cube);
            }
        }
        return result;
    }
}
