package com.epam.kickstart_b.query.search_query;

import com.epam.kickstart_b.entities.CubeEntity;
import com.epam.kickstart_b.query.CubeQuery;
import com.epam.kickstart_b.registrator.CubeRegistrator;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Query for searching cube objects by id.
 */
public class SearchIdQuery implements CubeQuery {

    /**
     * Id to search for.
     */
    private long id;

    /**
     * Constructor which assigns id to search for.
     *
     * @param identify id to search for
     */
    public SearchIdQuery(final long identify) {
        this.id = identify;
    }


    /**
     * Implementation of searching cube object by id.
     *
     * @param storage map of elements of repository
     * @return list with one element - object with specific id.
     */
    public ArrayList<CubeEntity> query(
            final HashMap<CubeEntity, CubeRegistrator> storage) {
        ArrayList<CubeEntity> result = new ArrayList<>();
        for (CubeEntity cube : storage.keySet()) {
            if (cube.getId() == this.id) {
                result.add(cube);
            }
        }
        return result;
    }

}
