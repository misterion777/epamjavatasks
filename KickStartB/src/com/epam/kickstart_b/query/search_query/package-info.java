/**
 * Contains specifications for searching objects in repository.
 */
package com.epam.kickstart_b.query.search_query;
