package com.epam.kickstart_b.query.sort_query;

import com.epam.kickstart_b.entities.CubeEntity;
import com.epam.kickstart_b.registrator.CubeRegistrator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Query for sorting repository cube objects by name.
 */
public class SortNameQuery extends AbstractSortQuery {

    /**
     * Implementation of sorting repository cube objects by name.
     *
     * @param storage map of elements of repository
     * @return list of objects sorted by name
     */
    @Override
    public ArrayList<CubeEntity> query(
            final HashMap<CubeEntity, CubeRegistrator> storage) {
        ArrayList<CubeEntity> cubesList = new ArrayList<>(storage.keySet());
        Comparator<CubeEntity> comparator =
                Comparator.comparing(CubeEntity::getName);
        if (isDescending()) {
            comparator = comparator.reversed();
        }
        cubesList.sort(comparator);
        return cubesList;
    }

}
