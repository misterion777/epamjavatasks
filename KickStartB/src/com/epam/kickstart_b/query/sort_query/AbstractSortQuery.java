package com.epam.kickstart_b.query.sort_query;

import com.epam.kickstart_b.query.CubeQuery;

/**
 * Contains specification for sorting query.
 */
public abstract class AbstractSortQuery implements CubeQuery {

    /**
     * Determines whether sorting occurs in descending or ascending order.
     */
    private final boolean descending;

    /**
     * Default constructor with ascending order for sorting.
     */
    public AbstractSortQuery() {
        this.descending = false;
    }

    /**
     * Constructor for creating settings for sorting.
     *
     * @param isDescending false - ascending sorting, true - descending
     */
    public AbstractSortQuery(final boolean isDescending) {
        this.descending = isDescending;
    }


    /**
     * Getter for descending field.
     *
     * @return boolean that determines whether sorting occurs
     * in descending or ascending order.
     */
    public boolean isDescending() {
        return descending;
    }
}
