package com.epam.kickstart_b.generator;

/**
 * Generates id for cube object.
 */
public final class IdGenerator {
    /**
     * Id value
     */
    private static long id = 0;

    /**
     * Generates new id for object.
     *
     * @return new id
     */
    public static long generateId() {
        return ++id;
    }

    /**
     * Private default constructor
     */
    private IdGenerator() {
    }

}
