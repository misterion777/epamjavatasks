package com.epam.kickstart_b.actions;

import com.epam.kickstart_b.entities.CubeEntity;
import com.epam.kickstart_b.entities.Point3DEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class containing implementation logic for such methods as
 * calculating volume, surface area of cube etc.
 */
public class CubeAction {
    /**
     * Logger object.
     */
    private static final Logger LOGGER = LogManager.getLogger();
    /**
     * Cube entity for which all data is calculated.
     */
    private CubeEntity operatedCube;

    /**
     * Array of points of operated cube.
     */
    private Point3DEntity[] points;

    /**
     * Default constructor.
     */
    public CubeAction() {

    }

    /**
     * Constructor for cube action object.
     *
     * @param newOperatedCube cube entity object on which is necessary to
     *                     calculate data.
     */
    public CubeAction(final CubeEntity newOperatedCube) {
        setOperatedCube(newOperatedCube);
    }

    /**
     * Cube entity object getter.
     *
     * @return cube entity object
     */
    public CubeEntity getOperatedCube() {
        return operatedCube;
    }

    /**
     * Cube entity object setter.
     *
     * @param newOperatedCube cube entity object
     */
    public void setOperatedCube(final CubeEntity newOperatedCube) {
        this.operatedCube = newOperatedCube;

    }

    /**
     * Sets an array of all points of an operated cube.
     */
    private void setPoints() {
        CubePointsAction pointsAction = new CubePointsAction(getOperatedCube());
        points = pointsAction.calculatePoints();
        LOGGER.info(points);
    }

    /**
     * Get surface area of a cube.
     *
     * @return cube serface area
     */
    public double getSurfaceArea() {
        final int sidesAmount = 6;
        return sidesAmount * Math.pow(operatedCube.getEdgeLength(), 2);
    }

    /**
     * Get volume of a cube.
     *
     * @return volume of a cube
     */
    public double getVolume() {
        final int dimensionsAmount = 3;
        return Math.pow(operatedCube.getEdgeLength(), dimensionsAmount);
    }

    /**
     * Check whether one of the cubes sides is on coordinate plane.
     *
     * @return boolean value
     */
    public boolean isBaseOnPlane() {
        setPoints();
        final int pointsOnSide = 4;
        int pointsOnZeroXPlane = 0;
        int pointsOnZeroYPlane = 0;
        int pointsOnZeroZPlane = 0;
        for (Point3DEntity point : this.points) {
            if (point.getX() == 0) {
                pointsOnZeroXPlane++;
            }
            if (point.getY() == 0) {
                pointsOnZeroYPlane++;
            }
            if (point.getZ() == 0) {
                pointsOnZeroZPlane++;
            }
        }

        return (pointsOnZeroXPlane == pointsOnSide)
                || (pointsOnZeroYPlane == pointsOnSide)
                || (pointsOnZeroZPlane == pointsOnSide);
    }

    /**
     * Count ratio of volumes of figures that are created after cross section
     * of a cube by one of the coordinate plane.
     *
     * @return ratio of volumes (zero if there is no cross section)
     */
    public double getVolumeRatio() {
        setPoints();
        final int xBiggestValueIndex = 2;
        final int yBiggestValueIndex = 1;
        final int zBiggestValueIndex = 4;

        // X
        if (points[0].getX() < 0) {
            return points[xBiggestValueIndex].getX()
                    / Math.abs(points[0].getX());
        }
        // Y
        if (points[0].getY() < 0) {
            return points[yBiggestValueIndex].getY()
                    / Math.abs(points[0].getY());
        }
        // Z
        if (points[0].getZ() < 0) {
            return points[zBiggestValueIndex].getZ()
                    / Math.abs(points[0].getZ());
        }

        return 0;
    }

}

