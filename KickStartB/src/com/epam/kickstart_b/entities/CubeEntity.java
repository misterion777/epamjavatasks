package com.epam.kickstart_b.entities;

import java.util.Objects;

import com.epam.kickstart_b.generator.IdGenerator;
import com.epam.kickstart_b.observer.CubeEvent;
import com.epam.kickstart_b.observer.Observer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class containing description of geometrical figure - cube.
 */
public class CubeEntity {
    /**
     * Logger object.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Name of cube object.
     */
    private String name;

    /**
     * Unique id for cube object.
     */
    private long id;

    /**
     * Length of cube edge.
     */
    private double edgeLength;
    /**
     * Left corner point of lower base of a cube.
     * Serves as initial point for calculating another corners of cube.
     */
    private Point3DEntity lowerLeftPoint;

    /**
     * Observer of that object.
     */
    private Observer observer;

    /**
     * Constructor for cube entity class.
     *
     * @param cubeName           name of the cube object
     * @param cubeEdgeLength     length of cube edge
     * @param cubeLowerLeftPoint initial left corner point of lower base of a
     *                           cube
     */
    public CubeEntity(final String cubeName, final double cubeEdgeLength,
            final Point3DEntity cubeLowerLeftPoint) {
        setId(IdGenerator.generateId());
        setName(cubeName);
        setLowerLeftPoint(cubeLowerLeftPoint);
        setEdgeLength(cubeEdgeLength);
        LOGGER.info("Created cube entity: " + this.toString());
    }

    /**
     * Default constructor for a cube object.
     */
    public CubeEntity() {
        setId(IdGenerator.generateId());
    }

    /**
     * Edge length getter.
     *
     * @return edge length of cube
     */
    public double getEdgeLength() {
        return edgeLength;
    }

    /**
     * Edge length setter.
     *
     * @param newEdgeLength edge length of cube
     */
    public void setEdgeLength(final double newEdgeLength) {
        this.edgeLength = newEdgeLength;
        notifyObserver();
    }

    /**
     * Initial point getter.
     *
     * @return initial left corner point of lower base of a cube
     */
    public Point3DEntity getLowerLeftPoint() {
        return lowerLeftPoint;
    }

    /**
     * Initial point setter.
     *
     * @param newLowerLeftPoint initial left corner point of lower base of a
     *                          cube
     */
    public void setLowerLeftPoint(final Point3DEntity newLowerLeftPoint) {
        this.lowerLeftPoint = newLowerLeftPoint;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format("Id: %s, Name: %s, "
                        + "Init point: %s, edge length: %f",
                id, name, lowerLeftPoint, edgeLength);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CubeEntity that = (CubeEntity) o;
        return Double.compare(that.edgeLength, edgeLength) == 0
                && name.equals(that.name)
                && Objects.equals(lowerLeftPoint, that.lowerLeftPoint);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int magicNum1 = 8;
        final int magicNum2 = 32;
        int result = ((int) id ^ (int) edgeLength
                ^ ((int) edgeLength >>> magicNum1));
        result = magicNum2 * result + lowerLeftPoint.hashCode();
        result = magicNum2 * result + name.hashCode();
        return result;
    }

    /**
     * Name getter.
     *
     * @return name of cube object
     */
    public String getName() {
        return name;
    }

    /**
     * Name setter.
     *
     * @param newName new cube object name
     */
    public void setName(final String newName) {
        this.name = newName;
    }

    /**
     * Id getter.
     *
     * @return id of cube object
     */
    public long getId() {
        return id;
    }

    /**
     * Id setter.
     *
     * @param identity new id
     */
    private void setId(final long identity) {
        this.id = identity;
    }

    /**
     * Adds an observer of this object.
     *
     * @param newObserver observer of this cube object
     */
    public void addObserver(final Observer newObserver) {
        this.observer = newObserver;
    }

    /**
     * Removes observer of this object.
     */
    public void removeObserver() {
        observer = null;
    }

    /**
     * Notifies an observer when necessary.
     */
    public void notifyObserver() {
        if (observer != null) {
            observer.handleEvent(new CubeEvent(this));
        }

    }

}
