package com.epam.kickstart_b.entities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class containing description of point in three dimensional space.
 */
public class Point3DEntity {

    /**
     * Logger object.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * X-coordinate in 3d space.
     */
    private double x;
    /**
     * Y-coordinate in 3d space.
     */
    private double y;
    /**
     * Z-coordinate in 3d space.
     */
    private double z;

    /**
     * Default constructor of point entity.
     */
    public Point3DEntity() {
        LOGGER.info("Created empty 3d point entity.");
    }

    /**
     * Constructor of 3d point with x, y, z coordinates parameters.
     *
     * @param x X-coordinate in 3d space
     * @param y Y-coordinate in 3d space
     * @param z Z-coordinate in 3d space
     */
    public Point3DEntity(final double x, final double y, final double z) {
        setX(x);
        setY(y);
        setZ(z);
        LOGGER.info("Created 3d point entity: " + this.toString());
    }

    /**
     * X getter.
     *
     * @return X-coordinate in 3d space
     */
    public double getX() {
        return x;
    }

    /**
     * X setter.
     *
     * @param x X-coordinate in 3d space
     */
    public void setX(final double x) {
        this.x = x;
    }

    /**
     * Y getter.
     *
     * @return Y-coordinate in 3d space
     */
    public double getY() {
        return y;
    }

    /**
     * Y setter.
     *
     * @param y Y-coordinate in 3d space
     */
    public void setY(final double y) {
        this.y = y;
    }

    /**
     * Z getter.
     *
     * @return Z-coordinate in 3d space
     */
    public double getZ() {
        return z;
    }

    /**
     * Z setter.
     *
     * @param z Z-coordinate in 3d space
     */
    public void setZ(final double z) {
        this.z = z;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Point3DEntity that = (Point3DEntity) o;
        return Double.compare(that.x, x) == 0
                && Double.compare(that.y, y) == 0
                && Double.compare(that.z, z) == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int magicNum1 = 8;
        final int magicNum2 = 32;
        int result = ((int) x ^ ((int) y >>> magicNum1));
        result = magicNum2 * result + (int) z;
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format("(%f, %f, %f)", x, y, z);
    }
}
