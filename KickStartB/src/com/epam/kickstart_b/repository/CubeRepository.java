package com.epam.kickstart_b.repository;

import com.epam.kickstart_b.entities.CubeEntity;
import com.epam.kickstart_b.query.Query;
import com.epam.kickstart_b.registrator.CubeRegistrator;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Implementation of repository that contains key-values,
 * where key is a cube object, value - its registrator.
 */
public final class CubeRepository implements
        Repository<CubeEntity, CubeRegistrator> {

    /**
     * Map with all elements of a repository.
     */
    private HashMap<CubeEntity, CubeRegistrator> storage = new HashMap<>();

    /**
     * Static repository object.
     */
    private static CubeRepository repository;

    /**
     * Implementation of singleton pattern.
     *
     * @return repository object
     */
    public static CubeRepository getRepository() {
        if (repository == null) {
            repository = new CubeRepository();
        }
        return repository;
    }

    /**
     * Private default constructor.
     */
    private CubeRepository() {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(final CubeEntity key, final CubeRegistrator value) {
        storage.put(key, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(final CubeEntity key) {
        storage.remove(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArrayList<CubeEntity> query(
            final Query<CubeEntity, CubeRegistrator> currentQuery) {
        return currentQuery.query(storage);
    }

    /**
     * Get copy of all elements in repository.
     *
     * @return copy of a map that contains all elements of repository.
     */
    public HashMap<CubeEntity, CubeRegistrator> getAll() {
        return new HashMap<>(storage);
    }
}
