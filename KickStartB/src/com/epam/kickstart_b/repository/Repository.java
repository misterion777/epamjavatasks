package com.epam.kickstart_b.repository;

import com.epam.kickstart_b.query.Query;

import java.util.ArrayList;

/**
 * Describes generic repository methods.
 *
 * @param <Entity>      Type of objects which are stored in repository
 * @param <Registrator> Type of registrators which are stored in repository
 */
public interface Repository<Entity, Registrator> {

    /**
     * Adds pair key-value to repository.
     *
     * @param key   object
     * @param value registrator
     */
    void add(Entity key, Registrator value);

    /**
     * Removes pair by key from repository.
     *
     * @param key object to be removed.
     */
    void remove(Entity key);

    /**
     * Any possible query to repository.
     *
     * @param currentQuery object with implementation of specific query
     * @return list of objects that matches the result of a query
     */
    ArrayList<Entity> query(Query<Entity, Registrator> currentQuery);
}
