package com.epam.kickstart_b.validators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Class that validates allready parsed input data.
 */
public class CubeValidator {

    /**
     * Object for error logs.
     */

    private static final Logger ERROR_LOGGER = LogManager.getLogger();

    /**
     * Validates input strings for cube creation and create a double array.
     *
     * @param lines parsed input string lines
     * @return list of double arrays containing valid data
     * for cube entity object creation
     */
    public ArrayList<Object[]> getValidatedDoubles(
            final HashMap<Integer, String> lines) {
        ArrayList<Object[]> validatedLines = new ArrayList<>();
        lines.forEach((lineNumber, line) -> {
            Object[] newArray = new Object[2];

            ArrayList<String> stringValues = new ArrayList<>(Arrays.asList(
                    line.split(" ")));
            newArray[0] = stringValues.get(0);
            stringValues.remove(0);

            double[] values = stringValues.stream()
                    .mapToDouble(Double::parseDouble)
                    .toArray();
            newArray[1] = values;
            if (values[0] > 0) {
                validatedLines.add(newArray);
            } else {
                ERROR_LOGGER.error(
                        String.format("Line %d: Edge length "
                                        + "must be bigger than zero!",
                                lineNumber));
            }
        });
        return validatedLines;
    }
}
