package com.epam.kickstart_b.creator;

import com.epam.kickstart_b.entities.CubeEntity;
import com.epam.kickstart_b.entities.Point3DEntity;
import com.epam.kickstart_b.exceptions.InvalidEdgeLengthException;
import com.epam.kickstart_b.registrator.CubeRegistrator;
import com.epam.kickstart_b.repository.CubeRepository;

import java.util.ArrayList;

/**
 * Class that creates new cube objects from parsed input data
 * and pushes objects to repository.
 */
public class CubeCreator {

    /**
     * Creates single cube entity based on input data
     * and then pushes it to repository.
     *
     * @param data array containing edge length
     *             and xyz-coordinates of initial point
     * @throws InvalidEdgeLengthException occurs while passing edge length
     * less or equal to zero
     */
    public CubeEntity createCubeEntity(final Object[] data)
            throws InvalidEdgeLengthException {
        double[] values = (double[]) data[1];

        if (values[0] <= 0) {
            throw new InvalidEdgeLengthException(
                    "Edge length of cube must be more than zero!");
        }
        final int xIndex = 1;
        final int yIndex = 2;
        final int zIndex = 3;

        CubeEntity cube = new CubeEntity();
        CubeRegistrator registrator = new CubeRegistrator();

        cube.addObserver(registrator);

        cube.setName((String) data[0]);
        cube.setEdgeLength(values[0]);
        cube.setLowerLeftPoint(new Point3DEntity(values[xIndex],
                values[yIndex],
                values[zIndex]));

        CubeRepository.getRepository().add(cube, registrator);
        return cube;
    }

    /**
     * Creates multiple cube entity based on input data
     * and pushes them to repository.
     *
     * @param cubesData list containing input data for each cube object
     * @throws InvalidEdgeLengthException occurs while passing edge length
     * less or equal to zero
     */
    public void createCubeEntities(
            final ArrayList<Object[]> cubesData) throws
            InvalidEdgeLengthException {
        for (Object[] cubeData : cubesData) {
            createCubeEntity(cubeData);
        }

    }


}
