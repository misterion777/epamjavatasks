package com.epam.kickstart_b.parsers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * Parser for input data for creating cube entity object.
 */
public class CubeDataParser {
    /**
     * Object for info logs.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Regular expression for valid data line in input text file.
     */
    private static final String DATA_REGEX = "^\\w+ ([-+]?\\d*[.,]?\\d+( |$))"
            + "{4}";

    /**
     * Parse input file and separate valid lines.
     *
     * @param filePath path to input files
     * @return hashmap with correct data lines
     * where key is number of line in initial input file
     * and value - correct string
     */
    public HashMap<Integer, String> getCorrectLines(final String filePath) {

        HashMap<Integer, String> correctLines = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {

            String line;
            int i = 1;
            while ((line = br.readLine()) != null) {
                if (line.matches(DATA_REGEX)) {

                    LOGGER.info(String.format("Line %d: OK", i));
                    line = line.replaceAll(",", ".");
                    correctLines.put(i, line);
                } else {
                    LOGGER.error(
                            String.format("Line %d: Wrong input", i));
                }
                i++;
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

        return correctLines;
    }

}
