package com.epam.kickstart_b.registrator;

import com.epam.kickstart_b.actions.CubeAction;
import com.epam.kickstart_b.observer.CubeEvent;
import com.epam.kickstart_b.observer.Observer;

/**
 * Contains volume and area surface of
 * a cube object that is being observed by object of this class.
 */
public class CubeRegistrator implements Observer {
    /**
     * Area surface of an observed cube object.
     */
    private double areaSurface;
    /**
     * Volume of an observed cube object.
     */
    private double volume;

    /**
     * Getter for an area surface.
     *
     * @return area surface of cube object.
     */
    public double getAreaSurface() {
        return areaSurface;
    }

    /**
     * Setter for an area surface.
     *
     * @param newAreaSurface area surface of a cube object
     */
    public void setAreaSurface(final double newAreaSurface) {
        this.areaSurface = newAreaSurface;
    }

    /**
     * Getter for volume.
     *
     * @return volume of a cube object
     */
    public double getVolume() {
        return volume;
    }

    /**
     * Setter for volume.
     *
     * @param newVolume volume of a cube object
     */
    public void setVolume(final double newVolume) {
        this.volume = newVolume;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void handleEvent(final CubeEvent event) {
        CubeAction action = new CubeAction(event.getSource());
        this.volume = action.getVolume();
        this.areaSurface = action.getSurfaceArea();
        System.out.println(String.format("%s has changed!", event.getSource()));
    }

}
