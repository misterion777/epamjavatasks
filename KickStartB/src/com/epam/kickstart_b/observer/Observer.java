package com.epam.kickstart_b.observer;

/**
 * Observer interface.
 */
public interface Observer {
    /**
     * Handles event that is passed by a observable object.
     *
     * @param event event that is created by a observable object
     */
    void handleEvent(CubeEvent event);
}
