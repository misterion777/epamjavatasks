import com.epam.kickstart_a.entities.Point3DEntity;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


/**
 * Contain tests for point entity equals() and hashcode().
 */
public class Point3DEntityTest {

    /**
     * Provides input data for equals and hashcode tests.
     * @return array of input data for testing equals and hashcode.
     */
    @DataProvider(name = "inputDataProvider")
    public Object[][] inputDataProvider(){
        return new Object[][]{
                {
                    new double[][]{
                            {1.5, -23, 153},
                            {1.5, -23, 153}
                    },
                    true
                },
                {
                    new double[][]{
                            {1.5, -23, 153},
                            {1.5, 23, 153}
                    },
                    false
                }
        };
    }

    /**
     * Testing hashcode method of a point entity
     * @param input input values for creating point entity object
     * @param expectedResult expected result of point.hashcode method
     */
    @Test(dataProvider = "inputDataProvider")
    public void testHashCode(double[][] input, boolean expectedResult){
        Point3DEntity point1 = new Point3DEntity(input[0][0], input[0][1], input[0][2]);
        Point3DEntity point2 = new Point3DEntity(input[1][0], input[1][1], input[1][2]);

        Assert.assertEquals(expectedResult, point1.hashCode() == point2.hashCode());
    }

    /**
     * Testing equals method of a point entity
     * @param input input values for creating point entity object
     * @param expectedResult expected result of point.equals method
     */
    @Test(dataProvider = "inputDataProvider")
    public void testEquals(double[][] input, boolean expectedResult){
        Point3DEntity point1 = new Point3DEntity(input[0][0], input[0][1], input[0][2]);
        Point3DEntity point2 = new Point3DEntity(input[1][0], input[1][1], input[1][2]);

        Assert.assertEquals(expectedResult, point1.equals(point2));
    }

}
