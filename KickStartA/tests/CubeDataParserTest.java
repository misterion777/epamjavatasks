import com.epam.kickstart_a.parsers.CubeDataParser;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.util.HashMap;

/**
 * Contain tests for cube data parser.
 */
public class CubeDataParserTest {

    /**
     * Parser object that is being tested.
     */
    private CubeDataParser parser;

    /**
     * Initializes parser object.
     */
    @BeforeClass
    public void initialize(){
        parser = new CubeDataParser();
    }

    /**
     * Provides data for getCorrectLines method.
     * @return array of input data for testing getCorrectLines.
     */
    @DataProvider(name = "parserDataProvider")
    public Object[][] parserDataProvider(){

        String filePath = "./data/1.txt";

        HashMap<Integer, String> expectedResult = new HashMap<>();
        expectedResult.put(1, "+25.5 0 0 0");
        expectedResult.put(5, "1.2 0.567 12 56");
        expectedResult.put(6, "-25.5 120 0.23 0.1");
        expectedResult.put(7, "0.000 3445 43.2 123");
        expectedResult.put(9, "567 -4.2 0 23");

        return new Object[][]{
                {filePath,expectedResult}
        };
    }

    /**
     * Test for getting correct lines from input files
     * @param inputFilePath path for an input file
     * @param expectedResult expected result of parser.getCorrectLines method
     */
    @Test(dataProvider = "parserDataProvider")
    public void testGetCorrectLines(String inputFilePath, HashMap<Integer, String> expectedResult){
        HashMap<Integer, String> result = parser.getCorrectLines(inputFilePath);
        Assert.assertEquals(expectedResult, result);

    }
}
