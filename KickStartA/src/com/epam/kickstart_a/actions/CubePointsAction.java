package com.epam.kickstart_a.actions;

import com.epam.kickstart_a.entities.CubeEntity;
import com.epam.kickstart_a.entities.Point3DEntity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class containing all logic for calculating point list of a operated cube.
 */
public class CubePointsAction {
    /**
     * Logger object.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Cube entity for which all points are calculated.
     */
    private CubeEntity operatedCube;

    /**
     * Constructor which takes a cube entity to operate as a parameter.
     * @param operatedCube cube entity to operate
     */
    public CubePointsAction(CubeEntity operatedCube) {
        setOperatedCube(operatedCube);
    }

    /**
     * Cube entity getter.
     * @return operated cube entity
     */
    public CubeEntity getOperatedCube() {
        return operatedCube;
    }

    /**
     * Cube entity setter.
     * @param operatedCube operated cube entity
     */
    private void setOperatedCube(CubeEntity operatedCube) {
        this.operatedCube = operatedCube;
    }
    /**
     * Method for calculating all points of an operated cube.
     * @return array of calculated points
     */
    public Point3DEntity[] calculatePoints() {
        final int rightAngle = 90;
        final int pointsAmount = 8;

        Point3DEntity[] points = new Point3DEntity[pointsAmount];
        points[0] = getOperatedCube().getLowerLeftPoint();
        points[pointsAmount / 2] = calculateUpperPoint(points[0]);
        double angle = 0;
        for (int i = 1; i < pointsAmount / 2; i++) {
            points[i] = calculateNextPoint(angle, points[i - 1]);
            points[i + pointsAmount / 2 ] = calculateNextPoint(angle,
                    points[i + pointsAmount / 2 - 1]);
            angle += rightAngle;
        }


        LOGGER.trace("Points successfully calculated.");

        return points;
    }

    /**
     * Calculates left corner point of upper base of a cube.
     * @param firstPoint left corner point of lower base of a cube
     * @return left corner point of upper base of a cube
     */
    private Point3DEntity calculateUpperPoint(Point3DEntity firstPoint) {
        return new Point3DEntity(firstPoint.getX(), firstPoint.getY(),
                firstPoint.getZ() + getOperatedCube().getEdgeLength());
    }

    /**
     * Calculates point of a cube based on previously calculated point
     * and angle between previous point and current point.
     * @param angle angle between previous point and current point
     * @param prevPoint previously calculated point of a cube
     * @return calculated point of a cube
     */
    private Point3DEntity calculateNextPoint(double angle,
                                             Point3DEntity prevPoint) {
        final double accuracy = 10000d;
        double[] cathetusses = getCathetusses(angle,
                getOperatedCube().getEdgeLength());

        return new Point3DEntity(roundWithAccuracy(
                prevPoint.getX() + cathetusses[0], accuracy),
                roundWithAccuracy(prevPoint.getY() + cathetusses[1], accuracy),
                roundWithAccuracy(prevPoint.getZ(), accuracy));

    }

    /**
     * Round() implementation with specific accuracy.
     * @param value value to be rounded
     * @param accuracy needed accuracy
     * @return already rounded value
     */
    private double roundWithAccuracy(double value, double accuracy) {
        return (double) Math.round(value * accuracy) / accuracy;
    }

    /**
     * Get cathetusses of right angled triangle by hypotenuse and angle.
     * @param angle of the angles in right angled triangle
     * @param hypotenuse hypotenuse of a triangle
     * @return array with cathetusses lengths where zero element is
     * cathetus opposite the angle, first element - the other cathetus
     */
    private double[] getCathetusses(double angle, double hypotenuse) {
        return new double[]{
                // x
                Math.sin(Math.toRadians(angle)) * hypotenuse,
                // y
                Math.cos(Math.toRadians(angle)) * hypotenuse
        };
    }

}
