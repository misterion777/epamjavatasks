package com.epam.kickstart_a.entities;

import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class containing description of geometrical figure - cube.
 */
public class CubeEntity {
    /**
     * Logger object.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Length of cube edge.
     */
    private double edgeLength;
    /**
     * Left corner point of lower base of a cube.
     * Serves as initial point for calculating another corners of cube.
     *
     */
    private Point3DEntity lowerLeftPoint;

    /**
     * Constructor for cube entity class.
     * @param edgeLength length of cube edge
     * @param lowerLeftPoint initial left corner point of lower base of a cube
     */
    public CubeEntity(double edgeLength, Point3DEntity lowerLeftPoint) {
        setLowerLeftPoint(lowerLeftPoint);
        setEdgeLength(edgeLength);
        LOGGER.trace("Created cube entity: " + this.toString());
    }

    /**
     * Edge length getter.
     * @return edge length of cube
     */
    public double getEdgeLength() {
        return edgeLength;
    }

    /**
     * Edge length setter.
     * @param edgeLength edge length of cube
     */
    public void setEdgeLength(double edgeLength) {
        this.edgeLength = edgeLength;
    }

    /**
     * Initial point getter.
     * @return initial left corner point of lower base of a cube
     */
    public Point3DEntity getLowerLeftPoint() {
        return lowerLeftPoint;
    }

    /**
     * Initial point setter.
     * @param lowerLeftPoint initial left corner point of lower base of a cube
     */
    public void setLowerLeftPoint(Point3DEntity lowerLeftPoint) {
        this.lowerLeftPoint = lowerLeftPoint;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format("Init point: %s, edge length: %f",
                lowerLeftPoint, edgeLength);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CubeEntity that = (CubeEntity) o;
        return Double.compare(that.edgeLength, edgeLength) == 0
                && Objects.equals(lowerLeftPoint, that.lowerLeftPoint);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int magicNum1 = 8;
        final int magicNum2 = 32;
        int result = ((int) edgeLength ^ ((int) edgeLength >>> magicNum1));
        result = magicNum2 * result + lowerLeftPoint.hashCode();
        return result;
    }
}
