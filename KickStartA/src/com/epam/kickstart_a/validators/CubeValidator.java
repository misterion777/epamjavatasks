package com.epam.kickstart_a.validators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Class that validates allready parsed input data.
 */
public class CubeValidator {

    /**
     * Object for error logs.
     */

    private static final Logger ERROR_LOGGER = LogManager.getLogger();

    /**
     * Validates input strings for cube creation and create a double array.
     * @param lines parsed input string lines
     * @return list of double arrays containing valid data
     * for cube entity object creation
     */
    public ArrayList<double[]> getValidatedDoubles(
            HashMap<Integer, String> lines) {
        ArrayList<double[]> validatedLines = new ArrayList<>();
        lines.forEach((lineNumber, line) -> {
            double[] values = Arrays.stream(line.split(" "))
                    .mapToDouble(Double::parseDouble)
                    .toArray();
            if (values[0] > 0) {
                validatedLines.add(values);
            } else {
                ERROR_LOGGER.error(
                        String.format("Line %d: Edge length "
                                        + "must be bigger than zero!",
                        lineNumber));
            }
        });
        return validatedLines;
    }
}
