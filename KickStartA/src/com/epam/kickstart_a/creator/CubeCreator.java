package com.epam.kickstart_a.creator;

import com.epam.kickstart_a.entities.CubeEntity;
import com.epam.kickstart_a.entities.Point3DEntity;
import com.epam.kickstart_a.exceptions.InvalidEdgeLengthException;

import java.util.ArrayList;

/**
 * Class that creates new cube objects from parsed input data.
 */
public class CubeCreator {

    /**
     * Creates single cube entity based on input data.
     * @param data array containing edge length
     *             and xyz-coordinates of initial point
     * @return cube entity object
     * @throws InvalidEdgeLengthException
     * occurs while passing edge length less or equal to zero
     */
    public CubeEntity createCubeEntity(double[] data)
            throws InvalidEdgeLengthException {
        if (data[0] <= 0) {
            throw new InvalidEdgeLengthException(
                    "Edge length of cube must be more than zero!");
        }
        final int xIndex = 1;
        final int yIndex = 2;
        final int zIndex = 3;
        return new CubeEntity(data[0], new Point3DEntity(data[xIndex],
                data[yIndex],
                data[zIndex]));
    }

    /**
     * Creates multiple cube entity based on input data.
     * @param cubesData list containing input data for each cube object
     * @return list of created cube objects
     * @throws InvalidEdgeLengthException
     * occurs while passing edge length less or equal to zero
     */
    public ArrayList<CubeEntity> createCubeEntities(
            ArrayList<double[]> cubesData) throws InvalidEdgeLengthException {
        ArrayList<CubeEntity> cubes = new ArrayList<>();
        for (double[] cubeData: cubesData) {
            cubes.add(createCubeEntity(cubeData));
        }
        return cubes;
    }


}
