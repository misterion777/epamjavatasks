package com.epam.kickstart_a.parsers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * Parser for input data for creating cube entity object.
 */
public class CubeDataParser {
    /**
     * Object for trace logs.
     */
    private static final Logger TRACER = LogManager.getLogger();

    /**
     * Object for error logs.
     */

    private static final Logger ERROR_LOGGER = LogManager.getLogger();

    /**
     * Regular expression for valid data line in input text file.
     */
    private static final String DATA_REGEX = "^([-+]?\\d*[.,]?\\d+( |$)){4}";

    /**
     * Parse input file and separate valid lines.
     * @param filePath path to input files
     * @return hashmap with correct data lines
     * where key is number of line in initial input file
     * and value - correct string
     */
    public HashMap<Integer, String> getCorrectLines(String filePath) {

        HashMap<Integer, String> correctLines = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {

            String line;
            int i = 1;
            while ((line = br.readLine()) != null) {
                    if (line.matches(DATA_REGEX)) {

                        TRACER.trace(String.format("Line %d: OK", i));
                        line = line.replaceAll(",", ".");
                        correctLines.put(i, line);
                    } else {
                        ERROR_LOGGER.error(
                                String.format("Line %d: Wrong input", i));
                    }
                    i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return correctLines;
    }

}
