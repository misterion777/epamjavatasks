package com.epam.kickstart_a.exceptions;

/**
 * Exception that occurs while trying to create
 * cube entity object with edge length less or equal to zero.
 */
public class InvalidEdgeLengthException extends Exception {

    /**
     * Constructor for an exception class.
     * @param message message to be shown after exception occur
     */
    public InvalidEdgeLengthException(String message) {
        super(message);
    }

}
