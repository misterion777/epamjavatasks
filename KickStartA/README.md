**KickStart Chapter A**

Implement classes CUBE and POINT. Implement methods and unit-tests:

1. cube surface area calculation; 
2. ratio between volumes of figures that are created after crossing cube with one of the coordinate plane;
3. is one of the bases of cube is on one of the coordinate plane;
4. Use such classes as:
action (with business logic), entity (with objects), parsers, validators, creators. 