package parser;

import com.epam.xmlparsing.model.Device;
import com.epam.xmlparsing.model.InputDevice;
import com.epam.xmlparsing.model.OutputDevice;
import com.epam.xmlparsing.model.PeripheralDevice;
import org.testng.annotations.BeforeSuite;

import java.util.HashSet;
import java.util.Set;

public class DevicesDataProvider {

    private static Set<Device> devices;

    public static Set<Device> getDevices(){
        if (devices == null){
            devicesDataInit();
        }
        return devices;
    }

    @BeforeSuite
    private static void devicesDataInit() {
        devices = new HashSet<>();

        PeripheralDevice ioDevice = new InputDevice("COM","keyboard");
        Device device = new Device("LogitechG102",ioDevice,"gb",
                20.5f,"2015-09-24Z",false,0);

        devices.add(device);

        ioDevice = new OutputDevice("LPT","videocard");
        device = new Device("GigabyteZ370P",ioDevice,"cn",
                41.2f,"2018-09-24Z",true,45);

        devices.add(device);

        ioDevice = new InputDevice("USB","mouse");
        device = new Device("CanyonCNE-CMSW1G",ioDevice,"cn",
                10f,"2016-09-24Z",false,0);

        devices.add(device);

        ioDevice = new OutputDevice("LPT","videocard");
        device = new Device("NVIDIA-RTX2080Ti",ioDevice,"de",
                1000f,"2019-09-24Z",true,120);

        devices.add(device);

        ioDevice = new InputDevice("USB","biometrics");
        device = new Device("HyperXPulseFireSurge",ioDevice,"by",
                100f,"2010-09-24Z",false,0);

        devices.add(device);

        ioDevice = new InputDevice("COM","midi");
        device = new Device("YamahaV8M",ioDevice,"id",
                54.75f,"2013-09-24Z",false,0);

        devices.add(device);

        ioDevice = new OutputDevice("COM","gps");
        device = new Device("GPS10V2",ioDevice,"it",
                25f,"2015-09-24Z",true,0);

        devices.add(device);

        ioDevice = new OutputDevice("USB","soundcard");
        device = new Device("Scarlet435J",ioDevice,"be",
                199f,"2014-09-24Z",true,50);

        devices.add(device);

        ioDevice = new InputDevice("USB","gamepad");
        device = new Device("DUALSHOCK4",ioDevice,"cn",
                55.55f,"2018-09-24Z",false,0);

        devices.add(device);

        ioDevice = new OutputDevice("LPT","headphones");
        device = new Device("BeyerDynamic124v",ioDevice,"fr",
                99.98f,"2016-09-24Z",true,120);

        devices.add(device);

        ioDevice = new InputDevice("USB","scanner");
        device = new Device("XeroxG102",ioDevice,"cn",
                5.99f,"2007-09-24Z",false,0);

        devices.add(device);

        ioDevice = new OutputDevice("COM","plotter");
        device = new Device("PLT4c02",ioDevice,"jp",
                21.2f,"2014-09-24Z",false,0);

        devices.add(device);

        ioDevice = new InputDevice("USB","audioCapture");
        device = new Device("ROODMic12",ioDevice,"de",
                26.77f,"2015-04-02Z",true,12);

        devices.add(device);

        ioDevice = new OutputDevice("COM","monitor");
        device = new Device("LG-FXW192",ioDevice,"id",
                344.57f,"2018-01-16Z",false,120);

        devices.add(device);

        ioDevice = new InputDevice("USB","mouse");
        device = new Device("LogitechM170",ioDevice,"cn",
                19.99f,"2016-06-13Z",false,0);

        devices.add(device);

        ioDevice = new OutputDevice("LPT","videocard");
        device = new Device("MSI-GTX1060",ioDevice,"us",
                400f,"2016-10-24Z",true,240);

        devices.add(device);

    }
}
