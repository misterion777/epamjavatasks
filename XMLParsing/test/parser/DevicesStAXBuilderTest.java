package parser;

import com.epam.xmlparsing.model.Device;
import com.epam.xmlparsing.parser.DevicesBuilder;
import com.epam.xmlparsing.parser.stax.DevicesStAXBuilder;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Set;

public class DevicesStAXBuilderTest {
    private InputStream stream;

    @BeforeClass
    public void init() {

        String path = "./xml/devices.xml";
        try {
            stream = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void devicesSetTest() {
        DevicesBuilder devicesBuilder = new DevicesStAXBuilder();
        devicesBuilder.buildDevicesSet(stream);

        Set<Device> actualDevices = devicesBuilder.getDevices();

        Assert.assertEquals(actualDevices, DevicesDataProvider.getDevices());
    }
}
