import com.epam.xmlparsing.parser.DevicesEnum;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DevicesEnumTest {

    @Test
    public void findByKeyPositiveTest() {
        String key = "energyUsage";
        DevicesEnum devicesEnum = DevicesEnum.findByKey(key);
        Assert.assertEquals(devicesEnum, DevicesEnum.ENERGY_USAGE);
    }

    @Test
    public void findByKeyNegativeTest() {
        String key = "fuelUsage";
        DevicesEnum device = DevicesEnum.findByKey(key);
        Assert.assertNull(device);
    }
}
