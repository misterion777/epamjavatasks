package com.epam.xmlparsing.parser;

import java.util.HashMap;
import java.util.Map;

/**
 * The enum Devices enum.
 */
public enum DevicesEnum {
    /**
     * Devices devices enum.
     */
    DEVICES("devices"),
    /**
     * Device devices enum.
     */
    DEVICE("device"),
    /**
     * Peripheral devices enum.
     */
    PERIPHERAL("peripheral"),

    /**
     * Critical devices enum.
     */
    CRITICAL("critical"),
    /**
     * Energy usage devices enum.
     */
    ENERGY_USAGE("energyUsage"),

    /**
     * Name devices enum.
     */
    NAME("name"),
    /**
     * Origin devices enum.
     */
    ORIGIN("origin"),
    /**
     * Price devices enum.
     */
    PRICE("price"),
    /**
     * Manufacture date devices enum.
     */
    MANUFACTURE_DATE("manufactureDate"),
    /**
     * Input spec devices enum.
     */
    INPUT_SPEC("inputSpec"),
    /**
     * Output spec devices enum.
     */
    OUTPUT_SPEC("outputSpec"),
    /**
     * Port type devices enum.
     */
    PORT_TYPE("portType"),
    /**
     * Input type devices enum.
     */
    INPUT_TYPE("inputType"),
    /**
     * Output type devices enum.
     */
    OUTPUT_TYPE("outputType");

    /**
     * String value of enum object.
     */
    private String value;

    /**
     * Gets value.
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Devices MAP.
     */
    private static final Map<String, DevicesEnum> MAP;

    static {
        MAP = new HashMap<>();
        for (DevicesEnum device : DevicesEnum.values()) {
            MAP.put(device.value, device);
        }
    }

    /**
     * Find by key devices enum.
     *
     * @param i the
     * @return the devices enum
     */
    public static DevicesEnum findByKey(final String i) {
        return MAP.get(i);
    }

    /**
     * Enum constructor.
     *
     * @param value string value
     */
    DevicesEnum(final String value) {
        this.value = value;
    }

}
