package com.epam.xmlparsing.parser.sax;

import com.epam.xmlparsing.model.Device;
import com.epam.xmlparsing.parser.DevicesBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

/**
 * The type Devices sax builder.
 */
public class DevicesSAXBuilder implements DevicesBuilder {
    /**
     * Logger object.
     */
    private static final Logger LOGGER = LogManager.getLogger(
            DevicesSAXBuilder.class);

    /**
     * Devices set.
     */
    private Set<Device> devices;
    /**
     * Event handler for parsing devices.
     */
    private DeviceHandler deviceHandler;
    /**
     * Xml reader object.
     */
    private XMLReader xmlReader;

    /**
     * Instantiates a new Devices sax builder.
     */
    public DevicesSAXBuilder() {
        deviceHandler = new DeviceHandler();
        try {
            xmlReader = XMLReaderFactory.createXMLReader();
            xmlReader.setContentHandler(deviceHandler);
        } catch (SAXException e) {
            LOGGER.fatal("SAX parser exception: " + e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buildDevicesSet(final InputStream fileInputStream) {
        try {
            xmlReader.parse(new InputSource(fileInputStream));
        } catch (SAXException e) {
            LOGGER.error("SAX parser exception: " + e);
        } catch (IOException e) {
            LOGGER.error("I/О exception: " + e);
        }
        devices = deviceHandler.getDevices();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Device> getDevices() {
        return devices;
    }
}
