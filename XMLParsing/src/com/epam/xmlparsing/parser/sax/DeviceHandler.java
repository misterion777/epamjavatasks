package com.epam.xmlparsing.parser.sax;

import com.epam.xmlparsing.model.Device;
import com.epam.xmlparsing.model.InputDevice;
import com.epam.xmlparsing.model.OutputDevice;
import com.epam.xmlparsing.parser.DevicesEnum;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

/**
 * The type Device handler.
 */
public class DeviceHandler extends DefaultHandler {

    /**
     * Devices set.
     */
    private Set<Device> devices;
    /**
     * Current device that is being parsed.
     */
    private Device current;
    /**
     * Current enum that is being checked.
     */
    private DevicesEnum currentEnum;
    /**
     * Enums with text.
     */
    private EnumSet<DevicesEnum> withText;

    /**
     * Instantiates a new Device handler.
     */
    DeviceHandler() {
        devices = new HashSet<>();
        withText = EnumSet.range(DevicesEnum.NAME, DevicesEnum.OUTPUT_TYPE);
    }

    /**
     * {@inheritDoc}
     */
    public void startElement(final String uri, final String localName, final
    String qName, final Attributes attrs) {
        if (DevicesEnum.DEVICE.getValue().equals(localName)) {
            current = new Device();
            for (int i = 0; i < attrs.getLength(); ++i) {
                DevicesEnum temp = DevicesEnum.findByKey(attrs.getQName(i));
                switch (temp) {
                    case CRITICAL:
                        current.setCritical(Boolean.parseBoolean(attrs
                                .getValue(i)));
                        break;
                    case ENERGY_USAGE:
                        current.setEnergyUsage(Integer.parseInt(attrs
                                .getValue(i)));
                        break;

                }
            }
        } else {
            DevicesEnum temp = DevicesEnum.findByKey(localName);
            if (withText.contains(temp)) {
                currentEnum = temp;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public void endElement(final String uri, final String localName, final
    String qName) {
        boolean localNameIsTariff = DevicesEnum.DEVICE.getValue().equals(
                localName);
        if (localNameIsTariff) {
            devices.add(current);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void characters(final char[] ch, final int start, final int length) {
        String s = new String(ch, start, length).trim();
        if (currentEnum != null) {
            switch (currentEnum) {
                case NAME:
                    current.setName(s);
                    break;
                case ORIGIN:
                    current.setOrigin(s);
                    break;
                case PRICE:
                    current.setPrice(Float.parseFloat(s));
                    break;
                case MANUFACTURE_DATE:
                    current.setManufactureDate(s);
                    break;
                case INPUT_SPEC:
                    InputDevice inputDevice = new InputDevice();
                    current.setDeviceType(inputDevice);
                    break;
                case OUTPUT_SPEC:
                    OutputDevice outputDevice = new OutputDevice();
                    current.setDeviceType(outputDevice);
                    break;
                case PORT_TYPE:
                    current.getDeviceType().setPortType(s);
                    break;
                case INPUT_TYPE:
                    ((InputDevice) current.getDeviceType()).setInputType(s);
                    break;
                case OUTPUT_TYPE:
                    ((OutputDevice) current.getDeviceType()).setOutputType(s);
                    break;
                default:
                    throw new EnumConstantNotPresentException(currentEnum
                            .getDeclaringClass(), currentEnum.name());
            }
        }
        currentEnum = null;
    }


    /**
     * Gets devices.
     *
     * @return the devices
     */
    public Set<Device> getDevices() {
        return devices;
    }

    /**
     * Sets devices.
     *
     * @param devices the devices
     */
    public void setDevices(final Set<Device> devices) {
        this.devices = devices;
    }
}
