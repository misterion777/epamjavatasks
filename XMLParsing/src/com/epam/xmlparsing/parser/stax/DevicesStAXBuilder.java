package com.epam.xmlparsing.parser.stax;

import com.epam.xmlparsing.model.Device;
import com.epam.xmlparsing.model.InputDevice;
import com.epam.xmlparsing.model.OutputDevice;
import com.epam.xmlparsing.parser.DevicesBuilder;
import com.epam.xmlparsing.parser.DevicesEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * The type Devices st ax builder.
 */
public class DevicesStAXBuilder implements DevicesBuilder {
    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(
            DevicesStAXBuilder.class);
    /**
     * The Devices set.
     */
    private Set<Device> devices;
    /**
     * XML Input factory.
     */
    private XMLInputFactory inputFactory;


    /**
     * Instantiates a new Devices StAX builder.
     */
    public DevicesStAXBuilder() {
        devices = new HashSet<>();
        inputFactory = XMLInputFactory.newInstance();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buildDevicesSet(final InputStream inputStream) {
        XMLStreamReader reader;
        String name;
        try {
            reader = inputFactory.createXMLStreamReader(inputStream);
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = reader.getLocalName();
                    if (DevicesEnum.findByKey(name) == DevicesEnum.DEVICE) {
                        Device device = buildDevice(reader);
                        devices.add(device);
                    }
                }
            }
        } catch (XMLStreamException ex) {
            LOGGER.error("StAX parsing error: " + ex.getMessage());
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                LOGGER.error("Unable to close file: " + e);
            }
        }
    }

    /**
     * Build device device.
     *
     * @param reader the reader
     * @return the device
     * @throws XMLStreamException the xml stream exception
     */
    private Device buildDevice(final XMLStreamReader reader) throws
            XMLStreamException {
        Device device = new Device();
        buildBaseDevice(device, reader);
        return device;
    }

    /**
     * Build base device.
     *
     * @param device the device
     * @param reader the reader
     * @throws XMLStreamException the xml stream exception
     */
    private void buildBaseDevice(final Device device, final XMLStreamReader
            reader) throws XMLStreamException {

        device.setCritical(Boolean.parseBoolean(reader.getAttributeValue(
                null, DevicesEnum.CRITICAL.getValue())));
        String energyUsageString = reader.getAttributeValue(null, DevicesEnum
                .ENERGY_USAGE.getValue());
        if (energyUsageString != null) {
            device.setEnergyUsage(Integer.parseInt(energyUsageString));
        }
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (DevicesEnum.findByKey(name)) {
                        case NAME:
                            device.setName(getXMLText(reader));
                            break;
                        case ORIGIN:
                            device.setOrigin(getXMLText(reader));
                            break;
                        case PRICE:
                            device.setPrice(Float.parseFloat(getXMLText(
                                    reader)));
                            break;
                        case MANUFACTURE_DATE:
                            device.setManufactureDate(getXMLText(reader));
                            break;
                        case INPUT_SPEC:
                            device.setDeviceType(new InputDevice());
                            buildInputSpec((InputDevice) device.getDeviceType(),
                                    reader);
                            break;
                        case OUTPUT_SPEC:
                            device.setDeviceType(new OutputDevice());
                            buildOutputSpec((OutputDevice) device
                                    .getDeviceType(), reader);
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    DevicesEnum temp = DevicesEnum.findByKey(name);
                    if (temp == DevicesEnum.DEVICE) {
                        return;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Device");
    }

    /**
     * Build input spec.
     *
     * @param inputDevice the input device
     * @param reader      the reader
     * @throws XMLStreamException the xml stream exception
     */
    private void buildInputSpec(final InputDevice inputDevice, final
    XMLStreamReader reader) throws XMLStreamException {
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (DevicesEnum.findByKey(name)) {
                        case PORT_TYPE:
                            inputDevice.setPortType(getXMLText(reader));
                            break;
                        case INPUT_TYPE:
                            inputDevice.setInputType(getXMLText(reader));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (DevicesEnum.findByKey(name) == DevicesEnum.INPUT_SPEC) {
                        return;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag InputSpec");
    }


    /**
     * Build output spec.
     *
     * @param outputDevice the output device
     * @param reader       the reader
     * @throws XMLStreamException the xml stream exception
     */
    private void buildOutputSpec(final OutputDevice outputDevice, final
    XMLStreamReader reader) throws XMLStreamException {
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (DevicesEnum.findByKey(name)) {
                        case PORT_TYPE:
                            outputDevice.setPortType(getXMLText(reader));
                            break;
                        case OUTPUT_TYPE:
                            outputDevice.setOutputType(getXMLText(reader));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (DevicesEnum.findByKey(name) == DevicesEnum
                            .OUTPUT_SPEC) {
                        return;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag OutputSpec");
    }

    /**
     * Gets xml text.
     *
     * @param reader the reader
     * @return the xml text
     * @throws XMLStreamException the xml stream exception
     */
    private String getXMLText(final XMLStreamReader reader) throws
            XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Device> getDevices() {
        return devices;
    }
}
