package com.epam.xmlparsing.parser.dom;

import com.epam.xmlparsing.model.Device;
import com.epam.xmlparsing.model.InputDevice;
import com.epam.xmlparsing.model.OutputDevice;
import com.epam.xmlparsing.parser.DevicesBuilder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;


import com.epam.xmlparsing.parser.DevicesEnum;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The type Devices DOM builder.
 */
public class DevicesDOMBuilder implements DevicesBuilder {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(
            DevicesDOMBuilder.class);

    /**
     * The Devices.
     */
    private Set<Device> devices;
    /**
     * The Doc builder.
     */
    private DocumentBuilder docBuilder;

    /**
     * Instantiates a new Devices DOM builder.
     */
    public DevicesDOMBuilder() {
        devices = new HashSet<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOGGER.error(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buildDevicesSet(final InputStream fileInputStream) {
        LOGGER.info("Starting DOM parser!");
        try {
            Document doc = docBuilder.parse(fileInputStream);
            Element root = doc.getDocumentElement();
            NodeList devicesList = root.getElementsByTagName(DevicesEnum
                    .DEVICE.getValue());
            for (int i = 0; i < devicesList.getLength(); i++) {
                Element deviceElement = (Element) devicesList.item(i);
                Device device = buildDevice(deviceElement);
                devices.add(device);
            }
        } catch (IOException e) {
            LOGGER.error("I/O exception: " + e);
        } catch (SAXException e) {
            LOGGER.error("Parsing failure: " + e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Device> getDevices() {
        return devices;
    }

    /**
     * Build device.
     *
     * @param deviceElement the device element
     * @return the device
     */
    private Device buildDevice(final Element deviceElement) {
        Device device = new Device();
        buildBaseDevice(deviceElement, device);
        return device;
    }

    /**
     * Build base device.
     *
     * @param deviceElement the device element
     * @param device        the device
     */
    private void buildBaseDevice(final Element deviceElement, final Device
            device) {
        boolean isCritical = Boolean.parseBoolean(deviceElement.getAttribute(
                DevicesEnum.CRITICAL.getValue()));
        device.setCritical(isCritical);
        String energyUsageString = deviceElement.getAttribute(DevicesEnum
                .ENERGY_USAGE.getValue());
        int energyUsage = 0;
        if (!energyUsageString.equals("")) {
            energyUsage = Integer.parseInt(energyUsageString);
        }
        device.setEnergyUsage(energyUsage);

        device.setName(getElementText(deviceElement, DevicesEnum.NAME
                .getValue()));
        device.setOrigin(getElementText(deviceElement, DevicesEnum.ORIGIN
                .getValue()));
        device.setManufactureDate(getElementText(deviceElement, DevicesEnum
                .MANUFACTURE_DATE.getValue()));
        device.setPrice(Float.parseFloat(getElementText(deviceElement,
                DevicesEnum.PRICE.getValue())));


        Element inputElement = (Element) deviceElement.getElementsByTagName(
                DevicesEnum.INPUT_SPEC.getValue()).item(0);
        if (inputElement == null) {
            Element outputElement = (Element) deviceElement
                    .getElementsByTagName(DevicesEnum.OUTPUT_SPEC.getValue())
                    .item(0);
            device.setDeviceType(new OutputDevice());
            buildOutput(outputElement, (OutputDevice) device.getDeviceType());
        } else {
            device.setDeviceType(new InputDevice());
            buildInput(inputElement, (InputDevice) device.getDeviceType());
        }

    }

    /**
     * Gets element text.
     *
     * @param element     the element
     * @param elementName the element name
     * @return the element text
     */
    private static String getElementText(final Element element, final String
            elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        return node.getTextContent();
    }


    /**
     * Build input.
     *
     * @param inputElement the input element
     * @param inputDevice  the input device
     */
    private void buildInput(final Element inputElement, final InputDevice
            inputDevice) {
        inputDevice.setPortType(getElementText(inputElement, DevicesEnum
                .PORT_TYPE.getValue()));
        inputDevice.setInputType(getElementText(inputElement, DevicesEnum
                .INPUT_TYPE.getValue()));
    }

    /**
     * Build output.
     *
     * @param outputElement the output element
     * @param outputDevice  the output device
     */
    private void buildOutput(final Element outputElement, final OutputDevice
            outputDevice) {
        outputDevice.setPortType(getElementText(outputElement, DevicesEnum
                .PORT_TYPE.getValue()));
        outputDevice.setOutputType(getElementText(outputElement, DevicesEnum
                .OUTPUT_TYPE.getValue()));
    }


}
