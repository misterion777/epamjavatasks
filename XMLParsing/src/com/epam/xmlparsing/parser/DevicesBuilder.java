package com.epam.xmlparsing.parser;

import com.epam.xmlparsing.model.Device;

import java.io.InputStream;
import java.util.Set;

/**
 * The interface Devices builder.
 */
public interface DevicesBuilder {
    /**
     * Build devices set from xml file input stream.
     *
     * @param fileInputStream xml file input stream
     */
    void buildDevicesSet(InputStream fileInputStream);

    /**
     * Gets devices set.
     *
     * @return the devices
     */
    Set<Device> getDevices();

}
