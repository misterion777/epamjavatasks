package com.epam.xmlparsing.servlet;

import com.epam.xmlparsing.model.Device;
import com.epam.xmlparsing.parser.DevicesBuilder;
import com.epam.xmlparsing.parser.dom.DevicesDOMBuilder;
import com.epam.xmlparsing.parser.sax.DevicesSAXBuilder;
import com.epam.xmlparsing.parser.stax.DevicesStAXBuilder;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

/**
 * The type Xml servlet.
 */
@WebServlet(name = "XMLServlet", urlPatterns = {"/XMLServlet"})
public class XMLServlet extends HttpServlet {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(XMLServlet.class);

        /**
         * Max threshold size for disk file item.
         */
        private static final int MAX_MEMORY_SIZE = 1024 * 1024 * 2;
        /**
         * Max upload file size.
         */
        private static final int MAX_REQUEST_SIZE = 1024 * 1024;

    /**
     * Instantiates a new servlet.
     */
    public XMLServlet() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    protected void doPost(final HttpServletRequest request, final
    HttpServletResponse response)
            throws ServletException {
        processRequest(request, response);
    }

    /**
     * Gets xml file and outputs table based on parsed data.
     *
     * @param request  http request to server, containing uploaded file and
     *                 parser type.
     * @param response response to client.
     * @throws ServletException servlet exception.
     */
    private void processRequest(final HttpServletRequest request, final
    HttpServletResponse response)
            throws ServletException{

        if (!ServletFileUpload.isMultipartContent(request)) {
            return;
        }

        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(MAX_MEMORY_SIZE);
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setSizeMax(MAX_REQUEST_SIZE);

        try {
            List fileList = upload.parseRequest(request);
            FileItem fileItem = (FileItem) fileList.get(0);
            FileItem selectItem = (FileItem) fileList.get(1);
            InputStream inputStream = fileItem.getInputStream();
            String parserType = selectItem.getString();

            DevicesBuilder devicesBuilder = getDeviceBuilder(parserType);
            devicesBuilder.buildDevicesSet(inputStream);

            Set<Device> devices = devicesBuilder.getDevices();
            request.setAttribute("devices", devices);
            request.getRequestDispatcher("/result.jsp").forward(request,
                    response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }


    /**
     * Gets device builder by string.
     *
     * @param parserType input parser type
     * @return device builder object.
     * @throws Exception exception.
     */
    private DevicesBuilder getDeviceBuilder(final String parserType) throws
            Exception {
        switch (parserType) {
            case "dom":
                LOGGER.info("DOM parser chosen!");
                return new DevicesDOMBuilder();
            case "sax":
                LOGGER.info("SAX parser chosen!");
                return new DevicesSAXBuilder();
            case "stax":
                LOGGER.info("StAX parser chosen!");
                return new DevicesStAXBuilder();
            default:
                throw new Exception("Something went wrong!");
        }
    }

    /**
     * {@inheritDoc}
     */
    public void destroy() {
        super.destroy();
    }

}
