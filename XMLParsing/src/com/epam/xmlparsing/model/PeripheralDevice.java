package com.epam.xmlparsing.model;

import java.util.Objects;

/**
 * The type Peripheral device.
 */
public abstract class PeripheralDevice {
    /**
     * Port type of the device.
     */
    private String portType;

    /**
     * Instantiates a new Peripheral device.
     */
    PeripheralDevice() {
    }

    /**
     * Instantiates a new Peripheral device.
     *
     * @param portType the port type
     */
    PeripheralDevice(final String portType) {
        this.portType = portType;
    }

    /**
     * Gets port type.
     *
     * @return the port type
     */
    public String getPortType() {
        return portType;
    }

    /**
     * Sets port type.
     *
     * @param portType the port type
     */
    public void setPortType(final String portType) {
        this.portType = portType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PeripheralDevice that = (PeripheralDevice) o;
        return Objects.equals(portType, that.portType);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {

        return Objects.hash(portType);
    }
}
