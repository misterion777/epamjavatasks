package com.epam.xmlparsing.model;


import java.util.Objects;

/**
 * Device type that is parsed from xml.
 */
public class Device {

    /**
     * The Name.
     */
    private String name;
    /**
     * The Device type.
     */
    private PeripheralDevice deviceType;
    /**
     * The Origin.
     */
    private String origin;
    /**
     * The Price.
     */
    private float price;
    /**
     * The Manufacture date.
     */
    private String manufactureDate;
    /**
     * The Critical.
     */
    private boolean critical;

    /**
     * The Energy usage.
     */
    private int energyUsage = 0;


    /**
     * Instantiates a new Device.
     */
    public Device() {

    }

    /**
     * Instantiates a new Device.
     *
     * @param name            name of device
     * @param deviceType      device type
     * @param origin          origin of device
     * @param price           price of device
     * @param manufactureDate manufacture date of device
     * @param critical        is critical for pc
     * @param energyUsage     energy usage of device
     */
    public Device(final String name,
                  final PeripheralDevice deviceType,
                  final String origin,
                  final float price,
                  final String manufactureDate,
                  final boolean critical,
                  final int energyUsage) {
        this.name = name;
        this.deviceType = deviceType;
        this.origin = origin;
        this.price = price;
        this.manufactureDate = manufactureDate;
        this.critical = critical;
        this.energyUsage = energyUsage;
    }

    /**
     * Gets origin.
     *
     * @return the origin
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Sets origin.
     *
     * @param origin the origin
     */
    public void setOrigin(final String origin) {
        this.origin = origin;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public float getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(final float price) {
        this.price = price;
    }

    /**
     * Gets manufacture date.
     *
     * @return the manufacture date
     */
    public String getManufactureDate() {
        return manufactureDate;
    }

    /**
     * Sets manufacture date.
     *
     * @param manufactureDate the manufacture date
     */
    public void setManufactureDate(final String manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    /**
     * Is critical boolean.
     *
     * @return the boolean
     */
    public boolean isCritical() {
        return critical;
    }

    /**
     * Sets critical.
     *
     * @param critical the critical
     */
    public void setCritical(final boolean critical) {
        this.critical = critical;
    }

    /**
     * Gets energy usage.
     *
     * @return the energy usage
     */
    public Integer getEnergyUsage() {
        return energyUsage;
    }

    /**
     * Sets energy usage.
     *
     * @param energyUsage the energy usage
     */
    public void setEnergyUsage(final Integer energyUsage) {
        this.energyUsage = energyUsage;
    }

    /**
     * Gets device type.
     *
     * @return the device type
     */
    public PeripheralDevice getDeviceType() {

        return deviceType;
    }

    /**
     * Sets device type.
     *
     * @param deviceType the device type
     */
    public void setDeviceType(final PeripheralDevice deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {

        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Device device = (Device) o;
        return Float.compare(device.price, price) == 0
                && critical == device.critical
                && energyUsage == device.energyUsage
                && Objects.equals(name, device.name)
                && Objects.equals(deviceType, device.deviceType)
                && Objects.equals(origin, device.origin)
                && Objects.equals(manufactureDate, device.manufactureDate);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {

        return Objects.hash(name, deviceType, origin, price,
                manufactureDate, critical, energyUsage);
    }
}
