package com.epam.xmlparsing.model;

import java.util.Objects;

/**
 * The type Input device.
 */
public class InputDevice extends PeripheralDevice {

    /**
     * String value for input device type.
     */
    private String inputType;

    /**
     * Instantiates a new Input device.
     */
    public InputDevice() {
    }

    /**
     * Instantiates a new Input device.
     *
     * @param portType  the port type
     * @param inputType the input type
     */
    public InputDevice(final String portType, final String inputType) {
        super(portType);
        this.inputType = inputType;
    }

    /**
     * Gets input type.
     *
     * @return the input type
     */
    public String getInputType() {
        return inputType;
    }

    /**
     * Sets input type.
     *
     * @param inputType the input type
     */
    public void setInputType(final String inputType) {
        this.inputType = inputType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        InputDevice that = (InputDevice) o;
        return Objects.equals(inputType, that.inputType);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), inputType);
    }
}
