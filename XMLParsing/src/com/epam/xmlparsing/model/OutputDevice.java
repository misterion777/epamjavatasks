package com.epam.xmlparsing.model;

import java.util.Objects;

/**
 * The type Output device.
 */
public class OutputDevice extends PeripheralDevice {
    /**
     * Output device type.
     */
    private String outputType;

    /**
     * Instantiates a new Output device.
     */
    public OutputDevice() {
    }

    /**
     * Instantiates a new Output device.
     *
     * @param portType   the port type
     * @param outputType the output type
     */
    public OutputDevice(final String portType, final String outputType) {
        super(portType);
        this.outputType = outputType;
    }

    /**
     * Gets output type.
     *
     * @return the output type
     */
    public String getOutputType() {
        return outputType;
    }

    /**
     * Sets output type.
     *
     * @param outputType the output type
     */
    public void setOutputType(final String outputType) {
        this.outputType = outputType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        OutputDevice that = (OutputDevice) o;
        return Objects.equals(outputType, that.outputType);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), outputType);
    }
}
