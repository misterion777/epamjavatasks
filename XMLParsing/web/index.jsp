<%--
  Created by IntelliJ IDEA.
  User: misterion
  Date: 8/21/18
  Time: 12:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>XMLParsing</title>
    <link rel="stylesheet" href="stylesheet.css">
</head>
<body>
<h3>Welcome to XML parser for devices info!</h3>
<form action="XMLServlet" method="post" enctype="multipart/form-data">
    <input required="required" type="file" name="inputFile">
    <select name="parserType" title="Parser type">
        <option selected="selected" value="dom">DOM</option>
        <option value="sax">SAX</option>
        <option value="stax">StAX</option>
    </select>
    <input type="submit" value="Parse!">
</form>
</body>
</html>
