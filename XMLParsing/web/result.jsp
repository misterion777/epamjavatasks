<%--
  Created by IntelliJ IDEA.
  User: misterion
  Date: 9/4/18
  Time: 4:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>XMLParsing</title>
    <link rel="stylesheet" href="stylesheet.css">
</head>
<body>
<jsp:useBean id="devices" scope="request" type="java.util.Set"/>
<table>
    <tr>
        <th>Name</th>
        <th>Origin</th>
        <th>Manufacture date</th>
        <th>Is critical</th>
        <th>Port type</th>
        <th>Device type</th>
        <th>Energy usage</th>
        <th>Price</th>
    </tr>
    <c:forEach items="${devices}" var="device">
        <tr>
            <td><c:out value="${device.name}"/></td>
            <td><c:out value="${device.origin}"/></td>
            <td><c:out value="${device.manufactureDate}"/></td>
            <td><c:out value="${device.critical}"/></td>
            <td><c:out value="${device.deviceType.portType}"/></td>
            <td>
                <c:catch var="exception">${device.deviceType.inputType}</c:catch>
                <c:if test="${not empty exception}">${device.deviceType.outputType}</c:if>
            </td>
            <td>${device.energyUsage}</td>
            <td>${device.price}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
